import time
import numpy as np
from collections import deque

import gymnasium as gym
from gymnasium import logger, spaces

import pybullet as p
import pybullet_data

import apterros.python.rewards as rewards
from apterros.propeller.propeller import TakeOffPropeller

Z_QUE_LEN = 40  # length of the queue on Z

class MiniAPTERROS(gym.Env):
    ''' 
    This class defines the Gym Environment for the miniapterros simulations. 
    For the '1D' MiniAPTERROS only the thrust power is controlled, not the thrust orientation.
    '''
    #
    # metada are by the script ai/src/run/[train|test]_model.py
    #
    # metadata = {'render_modes': ['human', 'rgb_array'],
    #             'render_fps': 
    #             'version': 
    #             'urdf': 
    #             'seed': 
    #             'total_mass': 
    #             'tau': 
    #             'z_target': 
    #             'reward_f': 
    #             'max_step': 
    #             'static_frict': 
    #             'stage': 
    #             }    

    def __init__(self, 
                 headless:bool=True,
                 clip_action:bool=True,
                 verbose:bool=1,
                 initProp_input:float=0):
        '''
        Parameters of the constructor:
            headless    : wether to display 3D graphical rendering or not.
                          For computing only take headless=True (default value: True).
            clip_action : wether to clip 'action' given by the neural network to keep its 
                          value in the 'action space' interval. Default is True.
            verbose     : verbosity (displaying info), possible values: 0,1,2. 
                          Default value : 1.
        '''
        # run the constructor of the base class:
        super().__init__()

        # attributes to be set using metadata mechanism:
        self.version      = None    # the version of the vehicule: '1D' or '3D'
        self.urdf_file    = None    # The relative pathname of the URDF file
        self.seed_value   = None    # seed for ranfom generators
        self.total_mass   = None    # kg, to be confirmed
        self.tau          = None    # time step in seconds
        self.z_target     = None    # target altitude in meter for taking off
        self.stage        = None    # the stage ('takeoff', 'station', 'landing')
        self.max_step     = None    # max number of steps to run
        self.static_frict = None    # static friction force in Newton when 1D constrained

        # self.action_space & self.observation_space will be defined by the 
        # setAttributesFromMetadata method:
        self.action_space      = None
        self.observation_space = None
        
        # The four propellers:
        TakeOffPropeller.loadCalibrationData("apterros/propeller/calib_4turbines.npz")
        self.NE_turbine    = TakeOffPropeller(1, mass=0.4) # the North-Est electric porpeller turbine
        self.NW_turbine    = TakeOffPropeller(2, mass=0.4) # the North-West electric porpeller turbine
        self.SE_turbine    = TakeOffPropeller(3, mass=0.4) # the South-Est electric porpeller turbine
        self.SW_turbine    = TakeOffPropeller(4, mass=0.4) # the South-West electric porpeller turbine
        self.turbines      = (self.NE_turbine, self.NW_turbine, 
                              self.SE_turbine, self.SW_turbine)
        
        # Other attributes related to pybullet :
        self.gravity      = 9.81
        self.pc_id        = None  # the id of the connection with PyBullet
        self.plane_id     = None  # the id of the ground plane
        self.apterros_id  = None  # the id of the vehicule in PyBullet
        
        self.NE_turbine_id = 12
        self.NW_turbine_id = 14
        self.SE_turbine_id = 17
        self.SW_turbine_id = 19
        self.turbines_id   = (self.NE_turbine_id, self.NW_turbine_id, 
                              self.SE_turbine_id, self.SW_turbine_id)
        
        # Other attributes:
        self.state        = None  # state of the vehicule
        self.prev_state   = None  # previous state 
        self.min_action   = -1.0  # 'action' comming from PPO is in range [-1, 1] which is
        self.max_action   =  1.0  # scaled later to [0, 1] by taking (action+1)/2 if needed
        self.headless     = headless
        self.prev_action  = initProp_input
        self.verbose      = verbose
        self.clip_action  = clip_action
        self.cur_step     = 0
        self.cur_time     = 0     # current physical time, secondes        
        self.z_que        = deque([], Z_QUE_LEN)  # moving window of 20 successive values of z
        self.viewer       = None
        self.screen       = None
        self.steps_beyond_terminated = None
        self.initialized  = False      
                
        # Initialize the attributes from metadata:
        self.setAttributesFromMetadata()
        self._max_episode_steps = self.max_step
        if self.verbose >= 1 :
            print(f'[MiniAPTERROS-{self.version}] initialized, _max_episode_steps:{self._max_episode_steps}')

        # Initialize the pybullet simulation process:
        mode = p.DIRECT if self.headless else p.GUI 
        self.pc_id = p.connect(mode)   # 
        p.setAdditionalSearchPath(pybullet_data.getDataPath())
        
        p.configureDebugVisualizer(p.COV_ENABLE_GUI, 0)
        p.resetSimulation()
        # set some important parameters:
        p.setGravity(0,0,-9.81)       # m/s^2
        p.setTimeStep(self.tau)       # the simulation time step in secondes
        

    def setAttributesFromMetadata(self): 
        '''
        To set the actual attributes with values coming from the metadata dict.
        '''
        self.version      = MiniAPTERROS.metadata['version']
        self.urdf_file    = MiniAPTERROS.metadata['urdf']
        self.seed_value   = MiniAPTERROS.metadata['seed']
        self.total_mass   = MiniAPTERROS.metadata['total_mass']
        self.z_target     = MiniAPTERROS.metadata['z_target']
        self.tau          = MiniAPTERROS.metadata['tau']
        self.stage        = MiniAPTERROS.metadata['stage']
        self.max_step     = MiniAPTERROS.metadata['max_step']
        self.static_frict = MiniAPTERROS.metadata['static_frict']
        self.reward_f     = MiniAPTERROS.metadata['reward_f']
        
        print(f"version     : {self.version}")
        print(f"urdf_file   : {self.urdf_file}")
        print(f"seed        : {self.seed_value}")
        print(f"total_mass  : {self.total_mass}")
        print(f"z_target    : {self.z_target}")
        print(f"tau         : {self.tau}")
        print(f"stage       : {self.stage}")
        print(f"max_step    : {self.max_step}")
        print(f"static_frict: {self.static_frict}")
        print(f"reward_f    : {self.reward_f}")

        if self.version == '1D':
            # Action space: 
            # - action in [-1,1], transfomed in PWM signal in [0, 100%] to drive the 4
            #   electrical propeller power.
            self.action_space = spaces.Box(low=self.min_action,
                                           high=self.max_action,
                                           shape=(1,))
            
            # Observation space:
            # - z in [0, +inf[ 
            # - z_dot in ]-inf, +inf[
            inf = np.finfo(np.float32).max
            self.observation_space = spaces.Box(low=np.array([0, -inf], dtype=np.float32),  
                                                high=np.array([inf, inf], dtype=np.float32), 
                                                dtype=np.float32)
                    
        self.initialized = True        
    
    def compute_thrust_from_PWM(self, pwm) :
        """
        To converts the input PWM into a force in Newton unit
        """
        thrust_Newton = 0
        
        if isinstance(pwm, list): pwm = pwm[0]
        
        for T in  self.turbines:
            thrust_Newton += T.ComputeThrust(pwm)
        assert(thrust_Newton >= 0)            
        
        return thrust_Newton
        
        ''' previously was:
        
        res = 4*self.gravity*3.71*prop_input #caracterisation 2021

        #res = 4*9.81*(3.09*prop_input) # D'après JLC
        #res = 4*9.81*(3.25e-2*prop_input*100 - 3.09e-1) # D'après étalonnage  de novembre 2019
        #res = 4*9.81*(0.0355*prop_input*100 - 0.52) # version précédente
        if res < 0: res = 0
        return res'''
    
    def updateState(self, do_step=False):
        '''
        Get the robot state from the simulation, and update the 'state' attribute.  
        If 'do_step' is True, request PyBullet simulation to make a time step.
        '''
        
        if do_step:
            p.stepSimulation(physicsClientId=self.pc_id)
            self.curr_step += 1
        
        match self.version:
            case '1D':
                state = self.updateState_1D()
            case '3D':
                state = self.updateState_3D()
            
        self.state = state
        
        
    def updateState_1D(self):
        '''
        Get the robot 1D state from the simulation.
        '''        
        pos_orient = p.getBasePositionAndOrientation(self.apterros_id)
        z = pos_orient[0][2]
        
        lin_rot_veloc = p.getBaseVelocity(self.apterros_id)
        z_dot = lin_rot_veloc[0][2]
        
        state = [z, z_dot]
        return state
    
    def updateState_3D(self):
        '''
        Get the robot 1D state from the simulation.
        '''        
        
        #state = [x, y, z, x_dot, y_dot, z_dot, q]
        
        state = None
        return state
    
    def step(self, action):
        """
        Apply the action to the vehicule and run one step of simulation.
        """
            
        try :
            assert self.action_space.contains(action), \
                "%r (%s) invalid" % (action, type(action))
        except :
            if self.clip_action: np.clip(action, self.min_action, self.max_action)
        
        # transform 'action' from range [-1,1] to range[0,1]:
        action = (float(action[0])+1)/2
        pwm    = 100*action        
        # Apply the thrust to the 4 pturbines:
        vehicule   = self.apterros_id
        tot_thrust = 0
        for turb, turb_id in zip(self.turbines, self.turbines_id):
            thrust = turb.ComputeThrust_N(pwm)
            tot_thrust += thrust
            p.applyExternalForce(vehicule, turb_id, [0,0,thrust], [0,0,0], p.LINK_FRAME, 
                                 physicsClientId=self.pc_id)
        print(f'tot_thrust: {tot_thrust:8.2f} N, {tot_thrust/self.gravity:8.2f} kg')

        # computes the new state of the vehicule:        
        p.stepSimulation(physicsClientId=self.pc_id)
        
        if self.cur_step == 0 : print(f" step: {self.cur_step}", end="")
        self.cur_step  += 1        
        self.cur_time  += self.tau
        
        self.updateState() # update the attribute self.state)
        
        #
        # Now, computes terminated, truncated and the reward:
        #
        terminated, truncated = False, False
        
        z, z_dot = self.state
        self.z_que.append(z)
        
        if z > 1.2*self.z_target: 
            terminated = True
            reward = 0
            print(f" step: {self.cur_step} done by z_target")
            
        elif self.max_step is not None and self.cur_step == self.max_step:
            terminated = True
            truncated  = True
            reward     = 0
            print(f" step: {self.cur_step} done by max_step\n")
            
        elif len(self.z_que) == Z_QUE_LEN:
            z_que_mean = np.array(self.z_que).mean()
            if self.stage == "takeoff":
                if self.cur_step > 100 and z_que_mean <= 0.005:
                    terminated = True
                    reward = -10
                    print(f" step: {self.cur_step} done by z_deque too small\n")
                elif abs(self.z_target - z_que_mean) <= 0.05*self.z_target:
                    terminated = True
                    reward = 1
                    print(f" step: {self.cur_step} done by z_deque close to target\n")
                    
            elif self.stage == "landing":
                pass
        
        if not terminated:
            reward_instr = f"rewards.{self.reward_f}(self, action, self.state, self.cur_time)"
            reward = eval(reward_instr) # eval() generates Python code
            
        elif self.steps_beyond_terminated is None:
            # Apterros terminated for the first time, but the exprerience is still going on
            self.steps_beyond_terminated = 0
            
        else:
            if self.steps_beyond_terminated == 0:
                logger.warn(
                "You are calling 'step()' even though this environment has already\n"
                "returned terminated = True. You should always call 'reset()' once\n"
                "you receive 'terminated = True' -- any further steps are undefined behavior.")
            self.steps_beyond_terminated += 1
            reward = 0.0       
            
        self.prev_action = action
        self.prev_state  = self.state

        return np.array(self.state, dtype=np.float32), reward, terminated, False, {'t':self.cur_time, 'z':z, 'action':action}

    def reset(self, seed=None, options=None):
        """
        Resets simulation and spawn every object in its initial position.
        """
        
        assert(self.initialized)      
        print(f"MiniAPTERROS reset")
        
        if seed == None: seed = self.seed_value
        super().reset(seed=seed)
        
        self.state = self.np_random.uniform(low=0.005, high=0.01, size=(2,))
        
        if self.stage == "landing":
            # z is z_target and z_dot is null:
            self.state = (self.z_target, 0.) 
        elif self.stage == "takeoff":
            # z and z_target are null:
            self.state = (0., 0.)
        
        self.z_que.clear()
        
        self.steps_beyond_terminated = None
        self.prev_state = self.state
        self.cur_step   = 0
        self.cur_time   = 0        
        
        if options != None:
            # Possibly  from the 'option' dict argument:
            dt = options.get('dt', None)
            if dt is not None : self.tau = dt
        
        # After reset every parameter has to be set
        p.resetSimulation(physicsClientId=self.pc_id)
        p.setTimeStep(self.tau, physicsClientId=self.pc_id)
        p.setGravity(0, 0, -9.81, physicsClientId=self.pc_id)
        p.resetDebugVisualizerCamera(cameraDistance=1., cameraYaw=45, 
                                     cameraPitch=10, 
                                     cameraTargetPosition=[0, 0, 0.5], physicsClientId=self.pc_id)
        
        # Load the ground:
        self.plane_id = p.loadURDF("plane.urdf", physicsClientId=self.pc_id)
        
        # Position & load the vehicule:
        self.apterros_id  = p.loadURDF(self.urdf_file, 
                                       [0, 0, 0.5],
                                       p.getQuaternionFromEuler([0.0, 0, 0]),
                                       physicsClientId=self.pc_id)
        
        pos = p.getBasePositionAndOrientation(self.apterros_id)
        print(f"Intial z-pos of apterros plate0: {pos[0][2]}")

        return np.array(self.state, dtype=np.float32), {}
    

    def close(self):
        '''
        Staff to done when closing the environment
        '''
        if self.screen is not None:
            import pygame
            pygame.display.quit()
            pygame.quit()
            self.isopen = False
        
        # close the Pybullet process:
        p.disconnect(physicsClientId=self.pc_id)

if __name__ == '__main__':
    
    # scene path relative to the 'project root' directory:
    urdf = "./apterros/pybullet/urdf/MiniAPTERROS-2.1.urdf"
    
    
    MiniAPTERROS.metadata = {'render.modes': ['human'],
                             'video.frames_per_second': 10,
                             'version': '1D',
                             'urdf': './apterros/pybullet/MiniAPTERROS-1.urdf',
                             'seed': 1234,
                             'total_mass': 8,
                             'tau': 10e-3,
                             'z_target': 1,
                             'reward_f': 'reward_takeoff_1',
                             'max_step': 5000,
                             'static_frict': 1,
                             'stage': 'takeoff'
                             }
    
    def convert_pwm_to_action(pwm):
        # convert pwm in [0,100] to action in [-1,1]
        return [2*pwm/100-1]
    
    env   = MiniAPTERROS(headless=False)
    env.reset()
        
    for i in range(100):
        action = convert_pwm_to_action(54)
        env.step(action)
        time.sleep(env.tau)
        #time.sleep(1)

    env.close()
