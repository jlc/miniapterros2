import time
import numpy as np
from numpy import pi, radians, degrees
from collections import deque

import gymnasium as gym
from gymnasium import logger, spaces

import pybullet as p
import pybullet_data

import apterros.python.rewards as rewards
from apterros.propeller.propeller import AntiRollPropeller

#  URDF frame {O,(x,y,z)} and GNC frame  {G,(X,Y,Z)} (Guidance Navigation Control) 
#            __________
#           |           |
#           |           |
#           |           |
# anti-roll |           |
#  electric |===========|
#   turbine |           |
#    |-> ██ |===========| ██         
#           |    z,X    |           
#           |     ^     |               
#           |     |     |                         
#           |     |     |             G : gravity center (~6 cm obove battery_plate)
#           |   ▓▓|▓▓   |            
#     battery-> ▓▓|▓▓   |             {G,(X,Y,Z)} : reference frame of the Mini-APTERROS
#           |   ▓▓G▓▓   |                           for the GNC processing.
#           |   ▓▓|▓▓...|...> Z       - 'roll' is the rotation angle around the X axis
#   battery ===/==O======......> y    - 'pitch' is the rotation angle around the Y axis
#   plate     / / |                   - 'yaw' is the rotation angle around the Z axis             
#            / /  @ <- gymbal joint   
#           / /   |                   {O,(x,y,z)} : reference frame of the URDF link
#          / / ███=███ <- turbine                   "battery_plate" (a.k.a the "base_link")
#         v  v  █   █       plate     - 'roll' is the rotation around the x axis
#        Y  x                         - 'pitch' is the rotation around the y axis
#                                     - 'yaw' is the rotation around the z axis
#                        
# the 'roll' of the rocket in the {G, (X,Y,Z)} frame is equal to the 'yaw' angle in 
# the frame {O, (x,y,z)})
#

class MiniAPTERROS_AntiRoll(gym.Env):
    ''' 
    This class defines the Gym Environment for the simulations where the 2 x 2 anti-roll
    electric turbines are driven in order to nullify the Mini-APTERROS roll around its
    longitudinal axis (GX, or Ox), du to the imperfect symmetry of the 4 main electric turbines 
    used for the take-off of the Mini-APTERROS.
    
    The whole mini-APTERROS is described via its URDF file.
    
    For the present the torque due to the take-off turbines is given arbitrarly as a 
    torque applied to the URDF link "central_hub", corresponding to the hub to which the
    4 take-off turbines are fixed.
    '''
    #
    # metada are set by the script ai/src/run/[train|test]_model.py
    #
    # metadata = {'render_modes': ['human', 'rgb_array'],
    #             'render_fps': 
    #             'version': 
    #             'urdf': 
    #             'total_mass': 
    #             'roll_target_deg':
    #             'roll_err_psi_deg':
    #             'roll_torque_Nm':
    #             'z_target_m': 
    #             'tau':     
    #             'max_step': 
    #             'reward_f': 
    #             'forced_Zveloc':
    #             }    

    ROLL_QUEUE_LEN = 20  # length of the queue on roll values
    PWM_QUEUE_LEN  = 10  # length of the queue on PWM values


    def __init__(self, 
                 CW_turbine_id,
                 CCW_turbine_id,
                 headless:bool=True,
                 clip_action:bool=True,
                 verbose:bool=1,
                 initProp_input:float=0):
        '''
        Parameters of the constructor:
            CW_turbine_id   : tuple (id1, id2) of the 2 CW electric turbines
            CCW_turbine_id  : tuple (id1, id2) of the 2 CW electric turbines
            headless        : wether to display 3D graphical rendering or not.
                              For computing only take headless=True (default value: True).
            clip_action     : wether to clip 'action' given by the neural network to keep its 
                              value in the 'action space' interval. Default is True.
            verbose         : verbosity (displaying info), possible values: 0,1,2. 
                              Default value : 1.
            initProp_input  : initial propeller input
        '''
        # run the constructor of the base class:
        super().__init__()

        # attributes to be set using metadata mechanism:
        self.version        = None    # the version of the vehicule: '1D' or '3D'
        self.urdf_file      = None    # The relative pathname of the URDF file
        self.seed_value     = None    # seed for ranfom generators
        self.total_mass     = None    # kg, to be confirmed
        self.tau            = None    # time step in seconds         
        self.roll_target    = None    # target angle [rd]  in {}
        self.roll_err_epsi  = None    # the threshold of the roll error 
        self.roll_torque    = None    # the roll torque applied to the vehicule 
        self.roll_abs_error = None    # abs(roll_target - effective roll)
        self.z_target       = None    # the actual height of the Mini-APTERROS
        self.max_step       = None    # max number of steps to run
        self.locked         = False

        # moving window of successive values of the absolute value of the difference
        # between the roll_target and the effective roll of the mini-APTERROS:
        self.roll_abs_error_que = deque([], MiniAPTERROS_AntiRoll.ROLL_QUEUE_LEN)  
        self.prev_PWM_que       = deque([], MiniAPTERROS_AntiRoll.PWM_QUEUE_LEN)

        # self.action_space & self.observation_space will be defined later by the 
        # setAttributesFromMetadata method:
        self.action_space      = None
        self.observation_space = None
        
        # The four propellers:
        self.CW_turbine1    = AntiRollPropeller(1, mass=0.05) # the North-Est electric porpeller turbine
        self.CW_turbine2    = AntiRollPropeller(2, mass=0.05) # the North-West electric porpeller turbine
        self.CCW_turbine1   = AntiRollPropeller(3, mass=0.05) # the South-Est electric porpeller turbine
        self.CCW_turbine2   = AntiRollPropeller(4, mass=0.05) # the South-West electric porpeller turbine
        self.antiroll_turbines = (self.CW_turbine1, self.CW_turbine1, self.CCW_turbine1, self.CCW_turbine2)
        
        # Other attributes related to pybullet :
        self.gravity      = 9.81
        self.pc_id        = None  # the id of the connection with PyBullet
        self.plane_id     = None  # the id of the ground plane
        self.apterros_id  = None  # the id of the vehicule in PyBullet
        
        self.CW_turbine1_id  = 14
        self.CW_turbine2_id  = 15
        self.CCW_turbine1_id = 12
        self.CCW_turbine2_id = 13
        self.antiroll_turbines_id = (self.CW_turbine1_id, self.CW_turbine2_id, 
                                     self.CCW_turbine1_id, self.CCW_turbine2_id)
        
        # Other attributes:
        self.state        = None  # state of the vehicule [roll, dot_roll, roll_torque]
        self.prev_state   = None  # previous state 
        self.min_action   = -1.0  # 'action' comming from PPO is in range [-1, 1] which is
        self.max_action   =  1.0  # scaled later to [0, 1] by taking (action+1)/2 if needed
        self.headless     = headless
        self.action       = initProp_input
        self.prev_action  = initProp_input
        self.verbose      = verbose
        self.clip_action  = clip_action
        self.cur_step     = 0
        self.cur_time     = 0     # current physical time, secondes        
        self.viewer       = None
        self.screen       = None
        self.steps_beyond_terminated = None
        self.initialized  = False      
        self.forced_Zveloc = 0  # used to force a Z velocity for the simulation
                
        # Initialize the attributes from metadata:
        self.setAttributesFromMetadata()
        self._max_episode_steps = self.max_step
        if self.verbose >= 1 :
            print(f'[MiniAPTERROS_AntiRoll-{self.version}] initialized, _max_episode_steps:{self._max_episode_steps}')

        # Initialize the pybullet simulation process:
        mode = p.DIRECT if self.headless else p.GUI 
        self.pc_id = p.connect(mode)   # 
        p.setAdditionalSearchPath(pybullet_data.getDataPath())
        
        p.configureDebugVisualizer(p.COV_ENABLE_GUI, 0)
        p.resetSimulation()
        p.setRealTimeSimulation(0)
        # set some important parameters:
        p.setGravity(0,0,-9.81)       # m/s^2
        p.setTimeStep(self.tau)       # the simulation time step in secondes
        p.resetDebugVisualizerCamera(cameraDistance=1.2, cameraYaw=45, cameraPitch=0, 
                                     cameraTargetPosition=[0, 0, 1.], physicsClientId=self.pc_id)
        

    def setAttributesFromMetadata(self): 
        '''
        To set the actual attributes with values coming from the metadata dict.
        '''
        self.version        = MiniAPTERROS_AntiRoll.metadata['version']
        self.urdf_file      = MiniAPTERROS_AntiRoll.metadata['urdf']
        self.seed_value     = MiniAPTERROS_AntiRoll.metadata['seed']
        self.total_mass     = MiniAPTERROS_AntiRoll.metadata['total_mass']
        self.roll_target    = radians(MiniAPTERROS_AntiRoll.metadata['roll_target_deg'])
        self.roll_err_epsi  = radians(MiniAPTERROS_AntiRoll.metadata['roll_err_epsi_deg'])
        self.roll_torque    = MiniAPTERROS_AntiRoll.metadata['roll_torque_Nm']
        self.torque_percent = MiniAPTERROS_AntiRoll.metadata['torque_percent']
        self.z_target       = MiniAPTERROS_AntiRoll.metadata['z_target_m']
        self.tau            = MiniAPTERROS_AntiRoll.metadata['tau']
        self.max_step       = MiniAPTERROS_AntiRoll.metadata['max_step']
        self.reward_f       = MiniAPTERROS_AntiRoll.metadata['reward_f']
        self.forced_Zveloc  = MiniAPTERROS_AntiRoll.metadata['forced_Zveloc']
        
        print(f"version       : {self.version}")
        print(f"urdf_file     : {self.urdf_file}")
        print(f"seed          : {self.seed_value}")
        print(f"total_mass    : {self.total_mass} [kg]")
        print(f"roll_target   : {degrees(self.roll_target)} [°] {self.roll_target:6.4f} [rd]")
        print(f"roll_err_epsi : {degrees(self.roll_err_epsi)} [°], {self.roll_err_epsi:6.4f} [rd]")
        print(f"roll_torque   : {self.roll_torque} [Nm]")
        print(f"torque_percent: {self.torque_percent} %")
        print(f"z_target      : {self.z_target} [m]")
        print(f"tau           : {self.tau} [s]")
        print(f"max_step      : {self.max_step}")
        print(f"reward_f      : {self.reward_f}")
        print(f"forced_Zveloc : {self.forced_Zveloc}")

        if self.version == '1D':
            
            # Action space: 
            # - action in [-1,1], transfomed in PWM signal in [0, 100%] to drive 
            #   - the 2 CW electrical turbines when action <0, 
            #   - the 2 CCW electrical turbines when action > 0.
            self.action_space = spaces.Box(low=self.min_action,
                                           high=self.max_action,
                                           shape=(1,))
            
            # Observation space:
            # - mini-APTERROS roll angle in ]-inf, +inf[ 
            # - mini-APTERROS dot_roll in ]-inf, +inf[
            # - roll_torque applied to the Mini-APTERROS, in ]-inf, +inf[
            inf = np.finfo(np.float32).max
            self.observation_space = spaces.Box(low=np.array([-inf, -inf, -inf], dtype=np.float32),  
                                                high=np.array([inf, inf, inf], dtype=np.float32), 
                                                dtype=np.float32)
                    
        self.initialized = True        
    
    
    def updateState(self, do_step=False):
        '''
        Get the robot state from the simulation, and update the 'state' attribute.  
        If 'step' is True, request PyBullet simulation to make a time step.
        '''
        
        if do_step:
            p.stepSimulation(physicsClientId=self.pc_id)
            self.curr_step += 1
        
        match self.version:
            case '1D':
                state = self.updateState_1D()
            case '3D':
                state = self.updateState_3D()
            
        self.state = state
        
        
    def updateState_1D(self):
        '''
        Get the robot 1D state from the simulation.
        '''        
        pos_orient = p.getBasePositionAndOrientation(self.apterros_id, physicsClientId=self.pc_id)
        roll = p.getEulerFromQuaternion(pos_orient[1])[2]
        
        base_rot_veloc = p.getBaseVelocity(self.apterros_id, physicsClientId=self.pc_id)[1]
        dot_roll = base_rot_veloc[2]
        
        state = [roll, dot_roll, self.roll_torque]
        return state
    
    def updateState_3D(self):
        '''
        Get the robot 3D state from the simulation.
        '''        
        
        #state = [x, y, z, x_dot, y_dot, z_dot, q]
        
        raise "not implemented"
        return None
    
    def step(self, action):
        """
        Apply the action to the vehicule and run one step of simulation.
        """
        
        try :
            assert self.action_space.contains(action), f"{action} (type(action) invalid"
        except :
            if self.clip_action: np.clip(action, self.min_action, self.max_action)
        
        
        # action given by the Neural Network is in [-1, 1]: we transform it in pwm 
        # in [-100%, 100%]:
        
        assert isinstance(action, np.ndarray)            
        action = action[0]
        self.action = action
        
        pwm = 100*abs(action)        
        self.prev_PWM_que.append(pwm)
        pwm = np.array(self.prev_PWM_que).mean()

        # Apply the thrust to the 4 turbines:
        vehicule   = self.apterros_id
        
        # link index of the central_hub : 25
        p.applyExternalTorque(self.apterros_id, 25, [0, 0, self.roll_torque], p.LINK_FRAME)
        
        if action > 0:
            thrust1_N = self.CCW_turbine1.ComputeThrust_N(pwm)
            thrust2_N = self.CCW_turbine2.ComputeThrust_N(pwm)
            p.applyExternalForce(vehicule, self.CW_turbine1_id, 
                                 [0,0,thrust1_N], [0,0,0], p.LINK_FRAME, physicsClientId=self.pc_id)
            p.applyExternalForce(vehicule, self.CW_turbine2_id, 
                                 [0,0,thrust2_N], [0,0,0], p.LINK_FRAME, physicsClientId=self.pc_id)
        elif action < 0:
            thrust1_N = self.CW_turbine1.ComputeThrust_N(pwm)
            thrust2_N = self.CW_turbine2.ComputeThrust_N(pwm)
            p.applyExternalForce(vehicule, self.CCW_turbine1_id, 
                                 [0,0,thrust1_N], [0,0,0], p.LINK_FRAME, physicsClientId=self.pc_id)
            p.applyExternalForce(vehicule, self.CCW_turbine2_id, 
                                 [0,0,thrust2_N], [0,0,0], p.LINK_FRAME, physicsClientId=self.pc_id)
        
        # computes the new state of the vehicule:        
        p.stepSimulation(physicsClientId=self.pc_id)
        
        if self.forced_Zveloc:
            p.resetBaseVelocity(vehicule, [0, 0, self.forced_Zveloc], physicsClientId=self.pc_id)
    
        #if self.cur_step == 0 : print(f" step: {self.cur_step}", end="")
        self.cur_step  += 1        
        self.cur_time  += self.tau
        
        self.updateState() # update the attribute self.state)

        #
        # Now, computes terminated, truncated and the reward:
        #
        terminated, truncated = False, False
        
        roll, dot_roll, roll_torque = self.state
        self.roll_abs_error = abs(self.roll_target - roll)
        self.roll_abs_error_que.append(self.roll_abs_error)
            
        if self.max_step is not None and self.cur_step == self.max_step:
            terminated = True
            truncated  = True
            reward     = 0
            print(f" step: {self.cur_step} terminated by max_step\n")
            
        elif len(self.roll_abs_error_que) == MiniAPTERROS_AntiRoll.ROLL_QUEUE_LEN:
            roll_abs_err_mean = np.array(self.roll_abs_error_que).mean()
            if self.locked == False and roll_abs_err_mean <= self.roll_err_epsi:
                self.locked = True
                print(f" step: {self.cur_step} first roll_abs_err_mean < roll_err_epsi\n")
            elif self.locked and roll_abs_err_mean >= self.roll_err_epsi:
                self.locked = False
                print(f" step: {self.cur_step} first roll_abs_err_mean > roll_err_epsi\n")
                terminated = True
                reward     = 0
        
        if not terminated:
            reward_instr = f"rewards.{self.reward_f}(self)"
            reward = eval(reward_instr) # eval() generates Python code
            
        elif self.steps_beyond_terminated is None:
            # Apterros terminated for the first time, but the exprerience is still going on
            self.steps_beyond_terminated = 0
        #ajouter elif pour si diff avec prev state plus que pi    
        else:
            if self.steps_beyond_terminated == 0:
                logger.warn(
                "You are calling 'step()' even though this environment has already\n"
                "returned terminated = True. You should always call 'reset()' once\n"
                "you receive 'terminated = True' -- any further steps are undefined behavior.")
            self.steps_beyond_terminated += 1
            reward = 0.0       
            
        self.prev_action = action
        self.prev_state  = self.state

        return np.array(self.state, dtype=np.float32), reward, terminated, truncated, \
            {'t':self.cur_time, 
             'roll_torque': roll_torque, 
             'roll':roll, 
             'dot_roll': dot_roll, 
             'action':action, 
             'pwm': pwm}

    def reset(self, seed:int=None, options:dict=None):
        '''
        To reset the simulation and spawn every object in its initial position.
        
        Parameters:
        - seed: if not None, its value is used to reset the RNG.
        - options: optionnal dictionary to give:
            -'dt': to change the self.dt time step of the PyBullet simulation
            -'roll_target': the roll angle to be controlled
            -'init_roll_torque': the initial value of the external roll torque applied
              to the vehicule.
            -'randomize': whether to add or not random perturbation to the vehicule roll
                          position and external roll torque.
            -'epsilon': to change the self.epsilon value.
            
        Return: the observation (the environement state) and possibly a dictionary
                of debug/info data.
        '''
        
        assert(self.initialized)      
        print(f"MiniAPTERROS_AntiRoll reset", end="")
        
        # reset the random generator (rng) with the fixed seed if needed:
        if seed != None: 
            self.seed_value = seed
            super().reset(seed=seed)
        
        self.roll_abs_error_que.clear()
        self.prev_PWM_que.clear()
        
        self.steps_beyond_terminated = None
        self.cur_step   = 0
        self.cur_time   = 0    
        self.locked     = False    
        
        self.tau         = MiniAPTERROS_AntiRoll.metadata['tau']
        self.roll_torque = MiniAPTERROS_AntiRoll.metadata['roll_torque_Nm']
        self.roll_target = radians(MiniAPTERROS_AntiRoll.metadata['roll_target_deg'])
        randomize = True
        
        if options != None:           
            self.tau         = options.get('dt', self.tau)
            self.roll_torque = options.get('init_roll_torque', self.roll_torque)
            self.roll_target = options.get('roll_target', self.roll_target)
            randomize        = options.get("randomize", randomize)
       
        if randomize: 
            val = self.roll_torque*self.torque_percent/100
            self.roll_torque += float(self.np_random.uniform(-val, val, (1,)))
            if self.roll_torque < 0 : self.roll_torque = 0.
            
        # After reset every parameter has to be set
        p.resetSimulation(physicsClientId=self.pc_id)
        p.setRealTimeSimulation(0)
        p.setTimeStep(self.tau, physicsClientId=self.pc_id)
        p.setGravity(0, 0, -9.81, physicsClientId=self.pc_id)
        p.resetDebugVisualizerCamera(cameraDistance=1.2, cameraYaw=45, cameraPitch=0, 
                                     cameraTargetPosition=[0, 0, 1.], physicsClientId=self.pc_id)
        
        # Load the ground:
        self.plane_id = p.loadURDF("plane.urdf", physicsClientId=self.pc_id)
        
        # Position & load the vehicule:
        self.apterros_id  = p.loadURDF(self.urdf_file, 
                                       [0, 0, self.z_target],
                                       p.getQuaternionFromEuler([0.0, 0, 0]),
                                       physicsClientId=self.pc_id)
        
        pos_orient = p.getBasePositionAndOrientation(self.apterros_id, physicsClientId=self.pc_id)
        orient = np.array(pos_orient[1])
        p.resetBasePositionAndOrientation(self.apterros_id,[0., 0., .5], orient)
        roll = p.getEulerFromQuaternion(pos_orient[1])[2]
        
        base_rot_veloc = p.getBaseVelocity(self.apterros_id, physicsClientId=self.pc_id)
        dot_roll = base_rot_veloc[1][2]
        
        self.state = [roll, dot_roll, self.roll_torque]
        self.prev_state = self.state
        
        print(f"Intial z-pos of apterros battery-plate: {pos_orient[0][2]}")
        print(self.state)
        return np.array(self.state, dtype=np.float32), {}
    

    def close(self):
        '''
        Staff to done when closing the environment
        '''
        if self.screen is not None:
            import pygame
            pygame.display.quit()
            pygame.quit()
            self.isopen = False
        
        # close the Pybullet process:
        p.disconnect(physicsClientId=self.pc_id)

if __name__ == '__main__':
    
    from gymnasium.utils.env_checker import check_env
    
    # scene path relative to the 'project root' directory:
    urdf_file = './apterros/pybullet/URDF_design/URDFf-creator/URDF__40.urdf'
    
    MiniAPTERROS_AntiRoll.metadata = {'render.modes': ['human'],
                                     'video.frames_per_second': 10,
                                     'version': '1D',
                                     'urdf': urdf_file,
                                     'seed': 1234,
                                     'total_mass': 8.5,
                                     'roll_target_deg': 0,
                                     'roll_err_epsi_deg': 0,
                                     'roll_torque_Nm': 0.01,
                                     'torque_percent': 20,
                                     'z_target_m': 1.,
                                     'tau': 20e-3,
                                     'max_step': 500,
                                     'reward_f': 'reward_antiroll_1',
                                     'forced_Zveloc': 0.25
                                     }
    
    def convert_pwm_to_action(pwm):
        # convert pwm in [0,100] to action in [0,1]
        return pwm/100
    
    env   = MiniAPTERROS_AntiRoll(headless=False, CW_turbine_id=(14,15), CCW_turbine_id=(12,13))
    
    # TODO_JLC: check_env(env, skip_render_check=True) 
    
    env.reset()

    z_target = env.z_target            
    nb_time_step = 500
    t1 = nb_time_step*env.tau
    
    T, roll_torque, roll, dot_roll = [], [], [], []
    for i in range(nb_time_step):
        
        # compute physical time:
        t = i*env.tau
        print(f'{t:.1f} s\r', end='')
        
        # no action for this simulation (anti-rool turbines off):
        action = convert_pwm_to_action(0)
        
        obs, reward, terminated, truncated, data = env.step(action)
        
        T.append(data['t'])
        roll_torque.append(data['roll_torque'])
        roll.append(data['roll'])
        dot_roll.append(data['dot_roll'])
                
        time.sleep(env.tau)

    env.close()

    import matplotlib.pyplot as plt
    
    # process roll to be [0, infinity] and not [-pi, pi]:
    roll = np.degrees(np.array(roll))
    r, offset = [], 0
    for r1, r2 in zip(roll[:-1], roll[1:]):
        if offset == 0 and abs(r1 - r2) > 150:
            offset = 360
        r.append(r2 + offset)
    
    fig, ax = plt.subplots(2, 1, figsize=(8,5))
    
    ax[0].set_title("Mini-APTERROS roll angle")
    ax[0].plot(T[:len(r)], r, '-b', label='roll')
    ax[0].set_ylabel("Roll angle [°]")
    ax[0].legend()
    ax[0].grid(which='major', color='xkcd:cool grey',  linestyle='-',  alpha=0.7)
    ax[0].grid(which='minor', color='xkcd:light grey', linestyle='--', alpha=0.5)
    
    ax[1].set_title("Mini-APTERROS roll speed")
    ax[1].plot(T, np.degrees(dot_roll), '-m', label='dot_speed')
    ax[1].set_xlabel("Time [s]")
    ax[1].set_ylabel("Roll speed [°/s]")
    ax[1].legend()
    ax[1].grid(which='major', color='xkcd:cool grey',  linestyle='-',  alpha=0.7)
    ax[1].grid(which='minor', color='xkcd:light grey', linestyle='--', alpha=0.5)
    
    plt.subplots_adjust(hspace=0.40)
    plt.savefig("Roll_and_roll_speed.png")
    plt.show()
    
    