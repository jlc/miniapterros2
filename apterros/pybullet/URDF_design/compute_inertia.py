from math import pi, radians, cos, sin

def grep_mass_in_urdf(urdf_file):
   with open(urdf_file, encoding="utf8") as F:
      content = F.readlines()
   total_mass = 0
   for line in  content:
      if "mass" in line:
         mass = float(line.split('"')[1])
         total_mass += mass
   print(f"total mass: {total_mass:.3f} kg")                      
         
def inertia_cyl(m, R, h):
   Ixx = m*(3*R**2 + h**2)/12
   Iyy = Ixx
   Izz = m*(R**2)/2
   return Ixx, Iyy, Izz

def inertia_tub(m, R, r, h):
   Ixx = mass*(3*(R**2 + r**2) + h**2)/12
   Iyy = Ixx
   Izz = m*(R**2+ r**2)/2
   return Ixx, Iyy, Izz

def inertia_box(m, a, b, c):
   # a is Lx, b is Ly, c is Lz
   Ixx = m*(b**2 + c**2)/12
   Iyy = m*(a**2 + c**2)/12
   Izz = m*(a**2 + b**2)/12
   return Ixx, Iyy, Izz
               
solids = {
   'IIDRE_receiver': {'mass':64e-3, 'box':(3e-2, 3e-2, 3e-2)},
   'IMU_base': {'mass':50e-3, 'box':(57e-3, 42e-3, 8.6e-3)},
   'IMU_case': {'mass':100e-3, 'box':(41.8e-3, 36.4e-3, 14.4e-3)},
   'IMU_plate': {'mass':87e-3, 'cyl':(90e-3, 8e-3)},
   'computer_wall': {'mass':185e-3, 'box':(76.5e-3, 8e-3, 150e-3)},
   'computer_plate': {'mass':84e-3, 'cyl':(80e-3, 8e-3)},
   'anti_roll_wall': {'mass':20e-3, 'box':(130e-3, 8e-3, 36e-3)},
   'anti_roll_plate': {'mass':100e-3, 'cyl':(90e-3, 6e-3)},
   'battery_upper_plate': {'mass':53e-3, 'cyl':(90e-3, 10e-3)},
   'battery_1': {'mass':1.100, 'box':(45e-3, 75e-3, 150e-3)},
   'battery_2': {'mass':1.100, 'box':(45e-3, 75e-3, 150e-3)},
   'battery_plate': {'mass':131e-3, 'cyl':(90e-3, 20e-3)},
   'tube': {'mass':86e-3, 'tub':(8e-3, 6e-3, 440e-3)},
   'foot': {'mass':62e-3, 'tub':(7e-3, 5e-3, 600e-3)},
   'foot': {'mass':62e-3, 'tub':(7e-3, 5e-3, 600e-3)},
   'gymbal_x_up': {'mass':30e-3, 'cyl':(5e-3, 20e-3)},
   'gymbal_x': {'mass':10e-3, 'cyl':(1e-3, 10e-3)},
   'gymbal_y': {'mass':10e-3, 'cyl':(1e-3, 10e-3)},
   'gymbal_y_down': {'mass':30e-3, 'cyl':(5e-3, 20e-3)},
   'central_hub': {'mass':50e-3, 'box':(40.e-3, 40e-3, 50e-3)},
   'prop_arm': {'mass':110e-3, 'box':(70.e-3, 30e-3, 40e-3)},
   'prop_up': {'mass':350e-3, 'cyl':(46.e-3, 74e-3)},
   'prop_down': {'mass':160e-3, 'cyl':(23.e-3, 74e-3)},
   }

'''tot_m = 0
for val in solids.values():
   tot_m += val['mass']

print(f'total mass: {tot_m} g')'''
   
for piece in [s for s in solids.keys()]:
   mass = solids[piece].get('mass')
   if dimensions := solids[piece].get('box'):
      Ixx, Iyy, Izz = inertia_box(mass, *dimensions)
   elif dimensions := solids[piece].get('cyl'):
      Ixx, Iyy, Izz = inertia_cyl(mass, *dimensions)
   elif dimensions := solids[piece].get('tub'):
      Ixx, Iyy, Izz = inertia_tub(mass, *dimensions)

   print(f'{piece:20s}:'
         f'mass: {mass:.3f} ' 
         f'<inertia ixx="{Ixx:.2e}" ixy="0" ixz="0" iyy="{Iyy:.2e}" iyz="0" izz="{Izz:.2e}" />')
   

grep_mass_in_urdf("apterros/pybullet/MiniAPTERROS-20.0.urdf")
