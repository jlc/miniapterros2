from Joint import Joint

class Link():
    
    def __init__(self, name):
        
        self.link_name  = name
        self.joint = None
        
    def AddJoint(self, type, parent_link, orig_xyz=None, orig_rpy=None, 
                 axis_xyz=None, limit=None):
      
      self.joint = Joint(type, parent_link, self.link_name, 
                         orig_xyz, orig_rpy, 
                         axis_xyz=axis_xyz, limit=limit)