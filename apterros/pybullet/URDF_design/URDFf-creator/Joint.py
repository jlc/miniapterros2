class Joint():
    
    def __init__(self, type, parent_link, child_link,
                 orig_xyz=None, orig_rpy=None, 
                 axis_xyz=None, limit=None):
        
        self.name = f"{parent_link}_to_{child_link}"
        self.type = type
        self.parent_link = parent_link
        self.child_link  = child_link
        self.orig_xyz    = orig_xyz
        self.orig_rpy    = orig_rpy
        self.axis_xyz    = axis_xyz
        self.limit_dict  = limit
        
    def build_URDF_bloc(self):
    
        urdf_bloc =f'''
  <joint name="{self.name}" type="{self.type}">
    <parent link="{self.parent_link}" />
    <child link="{self.child_link}" />
'''
      
        origin_bloc = ''
        if self.orig_xyz or self.orig_rpy:
            xyz = self.orig_xyz if self.orig_xyz else "0. 0. 0."
            rpy = self.orig_rpy if self.orig_rpy else "0. 0. 0."
            origin_bloc = f'    <origin xyz="{xyz}" rpy="{rpy}" />\n'
        
        axis_bloc = ''
        if self.axis_xyz:    
            axis_bloc = f'    <axis xyz="{self.axis_xyz}" />\n'
        
        limit_line = ''
        if self.limit_dict:    
            limit_line = "    <limit "
            for key in ("effort", "lower", "upper", "velocity"):
                if val := self.limit_dict.get(key, None):
                    limit_line += f' {key}="{val}"'
            limit_line += " />\n"
            
        urdf_bloc += axis_bloc            
        urdf_bloc += origin_bloc
        urdf_bloc += limit_line
        
        urdf_bloc += "  </joint>"

        return urdf_bloc
    