from Link import Link

class Box(Link):
    '''
    The model for all the MiniAPTERROS parts having a Box shape 
    like Battery, Walll...
    '''
    
    def __init__(self, link_name, 
                 mass, length_xyz,
                 orig_xyz="0. 0. 0.", orig_rpy="0. 0. 0.", 
                 material=None, collision=False, header_comment=None):       
        
        Link.__init__(self, link_name)

        self.M_kg = mass
        self.size_xyz_mm = length_xyz
        self.orig_xyz  = orig_xyz
        self.orig_rpy  = orig_rpy
        self.material = "blue" if not material else material
        self.collision = collision
        self.joint = None
        self.header_comment = header_comment if header_comment else link_name
       
    def build_URDF_bloc(self):
        
        urdf_bloc=f'''
  <!-- {50*'*'} -->
  <!-- **** {self.header_comment:40s} **** -->
  <!-- {50*'*'} -->
  <link name="{self.link_name}">
    <visual>
      <origin xyz="{self.orig_xyz}" rpy="{self.orig_rpy}" />
      <geometry>
        <box size="{self.size_xyz_mm}" />
      </geometry>
      <material name="{self.material}" />
    </visual>
    <inertial>
      <origin xyz="{self.orig_xyz}" rpy="{self.orig_rpy}" />
      <mass value="{self.M_kg}" />
      {self.URDF_inertia_line()}
    </inertial>'''
    
        collision_bloc = ""
        if self.collision:
          collision_bloc = f'''
    <collision>
      <origin xyz="{self.orig_xyz}" rpy="{self.orig_rpy}" />
      <geometry>
        <box size="{self.size_xyz_mm}" />
      </geometry>
    </collision>'''

        urdf_bloc += collision_bloc
        urdf_bloc += '\n  </link>'
        
        return urdf_bloc
       
       
    def URDF_inertia_line(self):
      # a is Lx, b is Ly, c is Lz
      a, b, c = tuple(map(float, self.size_xyz_mm.split()))
      m = float(self.M_kg)
      Ixx = m*(b**2 + c**2)/12
      Iyy = m*(a**2 + c**2)/12
      Izz = m*(a**2 + b**2)/12
      line = f'<inertia ixx="{Ixx:.2e}" ixy="0" ixz="0" iyy="{Iyy:.2e}" iyz="0" izz="{Izz:.2e}" />'
      return line
        
