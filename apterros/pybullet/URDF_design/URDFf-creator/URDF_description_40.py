#
# The object 'c' is an instance of the class Creator.
#
 
# The battery_plate is the base_link:
battery_plate = Cylinder("base_link", 
                         header_comment="The battery Plate is the base_link",
                         mass="197e-3", length="20e-3", radius="90e-3", collision=True)
c.AddLink(battery_plate)

# TUBES
directions = ("Est", "West", "North", "South")
XYZ = ("80e-3 0. 0.", "-80e-3 0. 0.", "0. 80e-3 0.", "0. -80e-3 0.")
RPY = ("0. 0. 0.", "0. 0. 3.1416", "0. 0. 1.5708", "0. 0. -1.5708") 

for (d, xyz, rpy) in zip(directions, XYZ, RPY):
  material = "white" if d != "North" else "black"
  tube = Tube(f"tube_{d}", orig_xyz="0. 0. 0.2",
              mass="76e-3", length="440e-3", ext_radius="8e-3", int_radius="6e-3",
              material=material)
  tube.AddJoint("fixed", "base_link", orig_xyz=xyz, orig_rpy=rpy) 
  c.AddLink(tube)
  
# FEET
XYZ = ("0.25 0. -0.25", "-0.25 0. -0.25", "0. 0.25 -0.25", "0. -0.25 -0.25")
RPY = ("0. -0.6 0.", "0 -0.6 3.1416", "0. -0.6  1.5708", "0. -0.6 -1.5708")

for (d, xyz, rpy) in zip(directions, XYZ, RPY):
  material = "white" if d != "North" else "black"
  tube = Tube(f"foot_{d}", mass="96e-3", length="600e-3", ext_radius="8e-3", int_radius="6e-3",
              material=material,  collision=True)    
  tube.AddJoint("fixed", "base_link", orig_xyz=xyz, orig_rpy=rpy) 
  c.AddLink(tube)

# BATTERIES
batt1 = Box("Battery1", mass="1.1", length_xyz="45e-3 75e-3 150e-3", material="grey",)
batt1.AddJoint("fixed", "base_link", orig_xyz="35e-3 0. 75e-3", orig_rpy="0. 0. 0.")
batt2 = Box("Battery2", mass="1.1", length_xyz="45e-3 75e-3 150e-3", material="grey",)
batt2.AddJoint("fixed", "base_link", orig_xyz="-35e-3 0. 75e-3", orig_rpy="0. 0. 0.")
c.AddLink(batt1)
c.AddLink(batt2)

# ANTI_ROLL
anti_roll_plate = Cylinder("anti_roll_plate", mass="0.082", length="6e-3", radius="90e-3", collision=True)
anti_roll_plate.AddJoint("fixed", "base_link", orig_xyz="0. 0. 0.2")
anti_roll_wall  = Box("anti_roll_wall", mass="0.020", length_xyz="130e-3 8e-3 36e-3", material="orange")
anti_roll_wall.AddJoint("fixed", anti_roll_plate.link_name, orig_xyz="0. 0. 0.025")
c.AddLink(anti_roll_plate)
c.AddLink(anti_roll_wall)

# ANTI_ROLL_TURBINES
from math import pi
pi_over_2 = pi/2
dist_to_center = "0.09"
directions = ("Est", "West", "North", "South")
orig_XYZ_roll = (f"{dist_to_center} 0 0", f"-{dist_to_center} 0 0", f"0 {dist_to_center} 0", f"0 -{dist_to_center} 0")
orig_RPY_roll = (f"-{pi_over_2} 0 0", f"{pi_over_2} 0 0", f"0 {pi_over_2} 0", f"0 -{pi_over_2} 0")
for (d, xyz_roll, rpy_roll) in zip(directions, orig_XYZ_roll, orig_RPY_roll):
  anti_roll_turbine = Cylinder(f"{d}_anti_roll_turbine", mass="0.005", length="5e-2",radius="15e-3",collision=True, material="black")
  anti_roll_turbine.AddJoint("fixed", anti_roll_plate.link_name, xyz_roll, rpy_roll)
  c.AddLink(anti_roll_turbine)

# COMPUTER
comp_plate = Cylinder("computer_plate", mass="0.082", length="6e-3", radius="90e-3", collision=True)
comp_plate.AddJoint("fixed", "base_link", orig_xyz="0. 0. 0.25")
comp_wall  = Box("computer_wall", mass="0.185", length_xyz="76.5e-3 8e-3 150e-3", material="orange")
comp_wall.AddJoint("fixed", comp_plate.link_name, orig_xyz="0. 0. 0.075")
c.AddLink(comp_plate)
c.AddLink(comp_wall)

# IMU
IMU_plate = Cylinder("IMU_plate", mass="0.087", length="8e-3", radius="90e-3", collision=True)
IMU_plate.AddJoint("fixed", "base_link", orig_xyz="0. 0. 0.4")
IMU_base  = Box("IMU_base", mass="0.050", length_xyz="57e-3 42e-3 8.6e-3", material="orange")
IMU_base.AddJoint("fixed", IMU_plate.link_name, orig_xyz="-12.5e-3 -2.45e-3 8.3e-3")
IMU_case  = Box("IMU_case", mass="0.100", length_xyz="41.8e-3 36.4e-3 14.4e-3", material="orange")
IMU_case.AddJoint("fixed", IMU_base.link_name, orig_xyz="0 0 0.0117")
c.AddLink(IMU_plate)
c.AddLink(IMU_base)
c.AddLink(IMU_case)

# GYMBAL_X
gx_up = Cylinder("gymbal_x_up", mass="0.030", length="20.e-3", radius="5e-3", material="black" )
gx_up.AddJoint("fixed", "base_link", orig_xyz="0. 0. -18e-3")
gx = Cylinder("gimbal_x", mass="0.010", length="10.e-3", radius="1e-3", material="white" )
gx.AddJoint("revolute", gx_up.link_name, orig_xyz="0. 0. -10e-3", orig_rpy="0. 1.5708 0.", 
             axis_xyz="0 0 1", limit={"effort":"100", "lower":"-0.5", "upper":"0.5", "velocity":"100"})
c.AddLink(gx_up)
c.AddLink(gx)

# GYMBAL_Y
gy = Cylinder("gimbal_y", mass="0.010", length="10.e-3", radius="1e-3", material="white" )
gy.AddJoint("revolute", gx.link_name, orig_rpy="-1.5708 0 0", 
             axis_xyz="0 0 1", limit={"effort":"100", "lower":"-0.5", "upper":"0.5", "velocity":"100"})
gy_down = Cylinder("gymbal_y_down", mass="0.030", length="20.e-3", radius="5e-3", material="black" )
gy_down.AddJoint("fixed", gy.link_name, orig_xyz="10e-3 0 0", orig_rpy="1.5708 0. -1.5708")
c.AddLink(gy)
c.AddLink(gy_down)

# CENTRAL HUB
hub = Box("central_hub", mass="0.050", length_xyz="40e-3 40e-3 50e-3", material="green")
hub.AddJoint("fixed", gy_down.link_name, orig_xyz="0. 0. -35e-3", orig_rpy="0. 0. 0.7854")
c.AddLink(hub)
    
# PROPELLERS
directions = ("NE", "SW", "NW", "SE")
orig_XYZ_arm = ("55e-3 0 -5e-3", "-55e-3 0 -5e-3", "0 55e-3 -5e-3", "0 -55e-3 -5e-3")
orig_RPY_arm = ("0. 0. 0.", "0 0 3.1406", "0 0 1.5708", "0 0 -1.5708") 
for (d, xyz_arm, rpy_arm) in zip(directions, orig_XYZ_arm, orig_RPY_arm):
  arm = Box(f"{d}_prop_arm", mass="0.112", length_xyz="70.e-3 30e-3 40e-3", material="green")
  arm.AddJoint("fixed", hub.link_name, orig_xyz=xyz_arm, orig_rpy=rpy_arm) 
  prop_up = Cylinder(f"{d}_propeller_up", mass="0.320", length="74.e-3", radius="46e-3", material="lightred" )
  prop_up.AddJoint("fixed", arm.link_name, orig_xyz="81e-3 0. 0.")
  prop_do = Cylinder(f"{d}_propeller_down", mass="0.160", length="74.e-3", radius="23e-3", material="lightred" )
  prop_do.AddJoint("fixed", prop_up.link_name, orig_xyz="0. 0. -74e-3")
  c.AddLink(arm)
  c.AddLink(prop_up)
  c.AddLink(prop_do)    
