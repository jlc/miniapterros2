from Link import Link 

class Tube(Link):
    '''
    The model for all the MiniAPTERROS parts having a tube shape
    '''
    
    def __init__(self, link_name,
                 mass, length, ext_radius, int_radius, 
                 orig_xyz="0. 0. 0.", orig_rpy="0. 0. 0.", 
                 material=None, collision=False, header_comment=None):       
        
        Link.__init__(self, link_name)
        
        self.M_kg = mass
        self.L_mm = length
        self.Rext_mm   = ext_radius
        self.Rint_mm   = int_radius
        self.orig_xyz  = orig_xyz
        self.orig_rpy  = orig_rpy
        self.material  = "white" if not material else material
        self.collision = collision
        self.header_comment = header_comment if header_comment else link_name
        
    def build_URDF_bloc(self):
        
        urdf_bloc=f'''
  <!-- {50*'*'} -->
  <!-- **** {self.header_comment:40s} **** -->
  <!-- {50*'*'} -->
  <link name="{self.link_name}">
    <visual>
      <origin xyz="{self.orig_xyz}" rpy="{self.orig_rpy}" />
      <geometry>
        <cylinder length="{self.L_mm}" radius="{self.Rext_mm}" />
      </geometry>
      <material name="{self.material}" />
    </visual>
    <inertial>
      <origin xyz="{self.orig_xyz}" rpy="{self.orig_rpy}" />
      <mass value="{self.M_kg}" />
      {self.URDF_inertia_line()}
    </inertial>'''
    
        collision_bloc = ""
        if self.collision:
            collision_bloc = f'''
    <collision>
      <origin xyz="{self.orig_xyz}" rpy="{self.orig_rpy}" />
      <geometry>
        <cylinder length="{self.L_mm}" radius="{self.Rext_mm}" />
      </geometry>
    </collision>'''

        urdf_bloc += collision_bloc
        urdf_bloc += '\n  </link>'
        
        return urdf_bloc
       
       
    def URDF_inertia_line(self):
        '''
        Computes inetria of a tube
        '''
        M, R, r, h = tuple(map(float, (self.M_kg, self.Rext_mm, self.Rint_mm, self.L_mm)), )
        Ixx = M*(3*(R**2 + r**2) + h**2)/12
        Iyy = Ixx
        Izz = M*(R**2+ r**2)/2
        line = f'<inertia ixx="{Ixx:.2e}" ixy="0" ixz="0" iyy="{Iyy:.2e}" iyz="0" izz="{Izz:.2e}" />'
        return line
        
        