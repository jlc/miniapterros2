from collections import OrderedDict

from Cylinder import Cylinder
from Tube import Tube
from Box import Box
import os

class Creator():  
  '''
  The class to build the UDF model of the MiniAPTERRS vessel, given the  
  description of its different parts.
  '''
    
  def __init__(self, file_name):       
      
    self.urdf_file_name = file_name
    self.links = []
      
  def WriteHeader(self):
      
    head='''<?xml version="1.0"?>
<robot name="MiniAPTERROS-1">

  <material name="blue">     <color rgba="0. 0. .8 1"/> </material>
  <material name="red">      <color rgba=".8 0. 0. 1"/> </material>
  <material name="lightred"> <color rgba=".8 .4 .4 1"/> </material>
  <material name="green">    <color rgba="0. .8 0. 1"/> </material>
  <material name="white">    <color rgba="1. 1. 1. 1"/> </material>
  <material name="grey">     <color rgba=".6 .6 .6 1"/> </material>
  <material name="orange">   <color rgba=".9 .2 .2 1"/> </material>
  <material name="black">    <color rgba=".1 .1 .1 1"/> </material>
  
'''
    with open(self.urdf_file_name, mode="w", encoding="utf8") as F:
      F.write(head)
            
  def AddLink(self, link):
    self.links.append(link)

  def WriteURDF(self):
      
    self.WriteHeader()
    
    for link in self.links:
      
      with open(self.urdf_file_name, mode="a", encoding="utf8") as F:
          F.write(link.build_URDF_bloc())
          if link.joint: F.write(link.joint.build_URDF_bloc())
          
    self.WriteFooter()
            
  def WriteFooter(self):
      
    with open(self.urdf_file_name, mode="a", encoding="utf8") as F:
      F.write("\n</robot>\n")
         
def grep_mass_in_urdf(urdf_file):
   with open(urdf_file, encoding="utf8") as F:
      content = F.readlines()
   total_mass = 0
   for line in  content:
      if "mass" in line:
         mass = float(line.split('"')[1])
         total_mass += mass
   print(f"total mass: {total_mass:.1f} kg")    
            
if __name__ == "__main__":
    
    path = "apterros/pybullet/URDF_design/URDFf-creator"
    
    list_URDF_description = [file for file in os.listdir(path) if file.startswith('URDF_description')]
    list_URDF_description.sort()
    
    for n, file in enumerate(list_URDF_description, 1):
      print(f"{n} -> {file}")
      
    num = input("Enter the num of the description file to use... ")
    num = int(num) - 1
    
    urdf_description_file = os.path.join(path, list_URDF_description[num])
    
    urdf_file_name = os.path.join(path, list_URDF_description[num].replace("description",'').replace('.py', '.urdf'))
    c = Creator(urdf_file_name)
    
    #urdf_description_file = "apterros/pybullet/URDF_design/URDFf-creator/URDF_description_20.py"
    with open(urdf_description_file, "r", encoding='utf-8') as F:
      description = F.read()
      
    exec(description)
    
    c.WriteURDF()
    
    grep_mass_in_urdf(urdf_file_name)
