import numpy as np

#
# Define the method "reward" that will replace the one defined in
# the "BasicApterrosEnv" class in file mini_apterros.py :
#
def reward_takeoff(self, action, state, t):  
    
    PWM = 100*action
    prevPWM = 100*self.prev_action
    z, z_dot = self.state
    prev_z, prev_z_dot = self.prev_state
    R = 0

   # penalize too high values of PWM:
    if PWM >= 80: 
        R -= 6
    else:
        R += 3

    # behavior when the target height is not yet reached
    if z <= 0.9*self.z_target:
        
        # reward small variation of PWM, penalize high PWM values:
        if 0 < abs(PWM - prevPWM) <= 5: 
            R += 3            
        else:
            R -= 2
            
        # reward a slow increase in height, penalize high decrease:
        if 0 < z - prev_z <= 0.1:
            R += 3
        else:
            R -= 2
    
    # behavior when the target height is reached:
    else:
        # reward keeping the vehicule at the target height, otherwize penalize
        if abs(self.z_target - z) <= 0.1: 
            R += 11  
        else:
            R -= 11     
    
    return R/14

def reward_takeoff_1(self, action, state, t):  
    
    PWM = 100*action
    prevPWM = 100*self.prev_action
    z, z_dot = self.state
    prev_z, prev_z_dot = self.prev_state
    R = 1
    
    if t < 0.5:
        if PWM <= 80: 
            R += 2
        else:
            R -= 2
        
    # behavior when the target height is not yet reached    
    elif z <= 0.9*self.z_target:
        
        # reward small variation of PWM, penalize high PWM values:
        if 0 < abs(PWM - prevPWM) <= 5: 
            R += 1            
            
        # reward a slow increase in height, penalize high decrease:
        if 0 < z - prev_z <= 0.1:
            R += 1
    
    # behavior when the target height is reached:
    else:
        # reward keeping the vehicule at the target height, otherwize penalize
        if abs(self.z_target - z) <= 0.1: 
            R += 3  
    
    return R/3

def reward_landing(self, action, z, t):  
 
    PWM = 100*action
    prevPWM = 100*self.prev_action
    z, z_dot = self.state
    prev_z, prev_z_dot = self.prev_state
    R = 0

    # reward values of PWM less than 85% :
    if PWM <= 80 : R += 4

    # reward small variation of PWM, penalize high PWM values:
    if abs(prevPWM - PWM) <= 5:
        R += 2
    else:
        R -= 2
    
    # reward a slow decrease in height, penalize high increase:
    if 0.01 < prev_z - z <= 0.05:
        R += 4
    else:
        R -= 4

    # almost on the ground reward turning off the turbines
    if z <= 0.01:
        if PWM <= 10 : R += 2

    return R/12

def reward_antiroll_1(self): 
    
    R = 0
    epsilon = self.roll_err_epsi
    
    PWM = 100*self.action
    prevPWM = 100*self.prev_action
    
    if len(self.roll_abs_error_que) == self.ROLL_QUEUE_LEN:
        roll_abs_err_mean = np.array(self.roll_abs_error_que).mean()
        if roll_abs_err_mean >= epsilon:
            R += epsilon / roll_abs_err_mean
        else:
            R += 1                             
    else:
        R += 0.1
    
    return R

def reward_antiroll_2(self): 
    
    R = 0
    epsilon = self.roll_err_epsi
    
    PWM = 100*self.action
    prevPWM = 100*self.prev_action
    
    if PWM <= 80: R += 1
    
    if len(self.roll_abs_error_que) == self.ROLL_QUEUE_LEN:
        roll_abs_err_mean = np.array(self.roll_abs_error_que).mean()
        if roll_abs_err_mean >= epsilon:
            R += (epsilon / roll_abs_err_mean)**2
        else:
            R += 1                             
    else:
        R += 0.1
    
    return R/2

def reward_antiroll_MD(self):
    """
    Fonction améliorée pour réduire les oscillations en stabilisant l’anti-roulis.
    """

    epsilon = self.roll_err_epsi  # Tolérance d'erreur en roulis
    
    roll_value, dot_roll_value = self.state[:2]  # Angle et vitesse de roulis
    PWM = 100 * self.action
    prevPWM = 100 * self.prev_action

    # Récompense de base
    R = 0
    ## 0. Gestion de error_roll
    if len(self.roll_abs_error_que) == self.ROLL_QUEUE_LEN:
        error_roll = np.array(self.roll_abs_error_que).mean()
    else:
        error_roll = self.roll_abs_error  # Erreur en roulis absolue
    ## 1. Récompense si le roulis est dans la tolérance
    if error_roll <= epsilon:
        R += 2.0  # Récompense modérée pour éviter un effet de "yo-yo"

        # Récompense douce pour faible vitesse angulaire
        R += max(0, 1.5 - (dot_roll_value ** 2) * 2)

        # Pénalité logarithmique pour les variations de PWM
        PWM_variation = abs(PWM - prevPWM)
        R -= float(np.log(1 + PWM_variation)) * .5 
      

    ## 2. Pénalités pour les erreurs de roulis et la vitesse angulaire
    else:
        R -= error_roll * 0.7  # Pénalité renforcée
        if dot_roll_value > 0.5:
            R -= (dot_roll_value - 0.5) ** 2 * 2  # Quadratique pour éviter des corrections violentes
         


    ## 4. Pénalité pour gros roulis
    if abs(roll_value) > epsilon:
        R -= abs(roll_value) * 0.7  
    

    # Limite pour éviter des valeurs extrêmes
    R = max(-5, min(R, 5))

    return R

def reward_antiroll_AJ(self):
    R = 0
    epsilon = self.roll_err_epsi
    roll_target = self.roll_target
    error_roll = self.roll_abs_error
    PWM = 100 * self.action
    prevPWM = 100 * self.prev_action
    
    
    if error_roll <= epsilon:
        R = R + 15  
    else:
        R += max(0, 5 - (error_roll / epsilon) * 5)  
    
    
    
    if PWM > 120:
        R -= 2  
    elif PWM <= 80:
        R += 3
    elif PWM <= 60:
        R+=5
    elif PWM <= 40:
        R+=7
    rr, rr_dot,torque = self.state
    r = rr * (180 / np.pi)
    r_dot = rr_dot * (180 / np.pi)
    r_t = roll_target * (180 / np.pi)
    amp = epsilon * (180 / np.pi)
    
    R += 1 / (abs(r - r_t) * 0.01 + 0.01)
    if abs(r - r_t) <= amp:
        R += (1 / (abs(r_dot) + 0.02)) * (R / 100)
    
    return R / 100