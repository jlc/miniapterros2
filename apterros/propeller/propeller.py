import numpy as np

class TakeOffPropeller():
    """
    TakeOffPropeller is the model of the electrical propeller turbine used for the 
    Mini-APTERROS takeing off.
    """
    
    # class attributes:
    Calibrated = False
    Calib_pwm  = None  # array of the PWM values [%] from calibration
    Thrust_N   = None  # array of the 4 thrusts from calibration file (in Newton)

    def __init__(self, number:int, mass:float, clockwise=True):
        
        self.id              = number - 1 # the number to identify the propeller position

        # The calibration file must have been loaded first of all:
        assert(TakeOffPropeller.Calibrated)      
        # Retrieve the thrust values [N] from the class attribute:
        self.calib_thrust_N  = TakeOffPropeller.Thrust_N[self.id]  
        self.calib_pwm       = TakeOffPropeller.Calib_pwm
             
        self.mass            = mass       # the mass of the propeller (kg)
        self.clockwise       = clockwise  # the direction of rotation of the propeller
        self.curr_thrust_N   = 0          # the thrust computed with the ComputeThrust method.
        
    def ComputeThrust_N(self, pwm:int):
        """
        To compute the propeller thrust in Newton, given the PWM command.
        Args:
            PWM:int: The value of the PWM command in range [0, 100]
        """
        assert (TakeOffPropeller.Calibrated)
        
        i2 = (pwm > self.calib_pwm).argmin()
        if self.calib_pwm[i2] == pwm:
            thrust = self.calib_thrust_N[i2]
        else:
            i1 = i2 - 1 
            assert(i1 >= 0)
            t1, t2 = self.calib_thrust_N[i1], self.calib_thrust_N[i2]
            p1, p2 = self.calib_pwm[i1], self.calib_pwm[i2]
            thrust = t1 + (t2 - t1)/(p2 - p1)*(pwm - p1)
            
        self.curr_thrust = thrust
        return thrust
        
    @staticmethod        
    def loadCalibrationData(file):
        '''
        To load the file *.npz that gives the calibration data (PWM, Thrust1...Thrust4)
        '''
        data = np.load(file)['four_turbines_array']
        TakeOffPropeller.Calib_pwm = data[0]
        TakeOffPropeller.Thrust_N    = data[1:]
        assert(TakeOffPropeller.Thrust_N.shape[0] == (4))
        
        TakeOffPropeller.Calibrated = True


class AntiRollPropeller():
    """
    AntiRollPropeller is the model of the esmall lectrical propeller turbine used to
    for the anti-roll actuator.
    """
    
    # class attributes:
    Calibrated = False
    Calib_pwm  = None  # array of the PWM values [%] from calibration
    Thrust_N   = None  # array of the 4 thrusts from calibration file (in Newton)

    def __init__(self, num:int, mass:float, clockwise=True, max_thrust_g=170):
        
        self.id              = num          # the number to identify the propeller position             
        self.mass            = mass         # the mass of the propeller (kg)
        self.max_thrust_g    = max_thrust_g # the thrust max in gram
        self.clockwise       = clockwise    # the direction of rotation of the propeller
        self.curr_thrust_N   = 0            # the thrust computed with the ComputeThrust method.
        self.name            = self.MakeName()
        
    def MakeName(self):
        name = 'AntiRoll-'
        if self.clockwise : name += "CW"
        else: name += "CCW"
        name += f'-{self.id}'
        return name

    def ComputeThrust_N(self, pwm:int):
        """
        To compute the propeller thrust in Newton, given the PWM command.
        Args:
            PWM:int: The value of the PWM command in  [0, 100]
        """
        
        # Compute the thrust in Newton :
        thrust = self.max_thrust_g * 1.e-3 * pwm /100  * 9.81           
        self.curr_thrust_N = thrust
        return thrust
                
if __name__ == "__main__":
    
    print("*****************************************")
    print("********* TakeOffPropeller **************")
    print("*****************************************")

    #
    # Load the .npz file to get directly the 4 thrust data
    #    
    file = "apterros/propeller/calib_4turbines.npz"
    TakeOffPropeller.loadCalibrationData(file)
    
    def test_turbine(turb):
        print(turb.calib_pwm)
        print(turb.calib_thrust_N)
        
        pwm_test = (7, 26, 35, 89)
        thrust_test = []
        for pwm in pwm_test:
            thrust = turb.ComputeThrust_N(pwm)
            thrust_test.append(thrust)
            print(f"Computed thrust for pwm={pwm:3d} : {thrust:8.3f} N")
        
        import matplotlib.pyplot as plt
        plt.figure()
        plt.plot(turb.calib_pwm, turb.calib_thrust_N, 'ob', markersize=3)
        plt.xlabel('PWM [%]')
        plt.ylabel('Thrust [N]')
        plt.grid()
        plt.plot(pwm_test, thrust_test, '+m', markersize=6)
        plt.show()
    
    prop1 = TakeOffPropeller(1, mass=.999, clockwise=True)
    prop2 = TakeOffPropeller(2, mass=.999, clockwise=True)
    prop3 = TakeOffPropeller(3, mass=.999, clockwise=True)
    prop4 = TakeOffPropeller(4, mass=.999, clockwise=True)
    
    test_turbine(prop1)
    test_turbine(prop2)
    test_turbine(prop3)
    test_turbine(prop4)
    
    print("*****************************************")
    print("********* AntiRollPropeller *************")
    print("*****************************************")

    prop1 = AntiRollPropeller(1, mass=0.080, clockwise=True)
    prop2 = AntiRollPropeller(2, mass=0.080, clockwise=True)
    prop3 = AntiRollPropeller(3, mass=0.080, clockwise=False)
    prop4 = AntiRollPropeller(4, mass=0.080, clockwise=False)
    
    np.set_printoptions(precision=2)
    pwm_array = np.linspace(0,100,11)
    
    print(f'{"pwm values":15s}', pwm_array)
    for prop in (prop1, prop2, prop3, prop4):
        thrust = []
        for pwm in pwm_array:
            thrust.append(prop.ComputeThrust_N(pwm))
        thrust = np.array(thrust)
        print(f'{prop.name:15s}', thrust)
    