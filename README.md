# Projet Mini-Apterros
  
Ce dépôt contient les ressources pour entraîner un réseau de neurones (_agent_) à contrôler plusieurs types de sytèmes mécatroniques (_environments_) tels que : _CartPole_, _Balancing robot_, Mini-Apterros... en utilisant l'apprentissage DRL (_Deep Reinforcement Learning_ : Apprentissage par Renforcement des réseaux Profonds).

- Pour mettre en place les algorithmes du __DRL__, nous utilisons les programmes et la documentation du site  _stable-baselines3_ <https://github.com/DLR-RM/stable-baselines3>.

- Pour la simulations des environnement contrôlés par DRL, nous utilisons au choix :

  - pour des _environements_ simples : __Gymnasium__ <https://gymnasium.farama.org/><br>
    (fork de _OpenAI/Gym_ (anciennement <https://www.gymlibrary.dev/> devenu <https://www.gymlibrary.dev/>) dont le développement est figé depuis 2022),
  - pour les _environements_ plus complexes : le simulateur robotique  _pybullet_ <https://pybullet.org/wordpress/> .

- Pour l'atelier de développement (_Integrated Developement Environment : IDE_), nous conseillons l'IDE __Visual Studio Code__ (_aka_ VSCode):

  - multi-plateforme : Windows, Mac & GNU/Linux,
  - code source libre distribué sous licence  MIT, disposant d'une communauté active de contributeurs,
  - faible empreinte mémoire et CPU,
  - compatible avec les _Environnements Virtuels Python_ et le système _git_ pour le stockage/versionnage du code source sur les plateformes Github ou GitLab,
  - debugger graphique intégré compatible Python,
  - terminal (console) intégré,
  - compatible avec les fichiers _notebook jupyter*, avec rendu graphique,
  - doté d'un mécanisme d'extensions puissant simple à utiliser.

  Il existe d'autres IDE Python qui proposent des fonctionnalités analogues  (pycharm, atom...).
Le choix de VSCode est motivé par sa relative simplicité d'utilisation et sa faible empreinte mémoire.

## Racine du projet

La racine du projet (_root_) désigne le répertoire contenant ce README.

C'est le répertoire de plus haut niveau du projet : normalement c'est le répertoire nommé `miniapterros2` obtenu par clonage du dépôt GitLab <https://gitlab.com/jlc/miniapterros2.git>.

## Créer l'environnement Python de travail

### 1) Environnement Virtuel Python (EVP) nommé __pyml__

- Télécharge __miniconda3__ pour ton OS : https://docs.conda.io/en/latest/miniconda.html<br>
- Installe __miniconda3__ : https://docs.anaconda.com/miniconda/miniconda-install/<br>
**Attention** le chemin complet du répertoire d'installation `miniconda3`  [ `C:\..chemin..\miniconda3` pour Wndows, ou `/..chemin../miniconda3` pour Mac & Linux ] doit être choisi de façon à ne comporter aucun caractère accentué ni espace !!!<br><br>
Une fois miniconda3 installé, lance un nouveau "terminal" (Linux ou MacOs) ou une nouvelle fenêtre "Anaconda Prompt" (Windows) et tape les  commandes:<br>
`conda config --set auto_activate_base false`<br>
`conda update -n base -c defaults conda`<br>

- Crée l'EVP __minia2__ associé à un interpréteur Python 3.10 en tapant dans le terminal (Mac/Linux) ou la fenêtre "Anaconda prompt" (Windows) :<br>
`conda create -n minia2 python=3.10`<br>
=> Attention à bien utiliser le nom __minia2__ : la suite du tutoriel, les fichiers de configurations et les exemples fournis, sont basés sur ce nom.

### 2) Compléter l'installation des modules Python

- Le plus simple est d'utiliser le fichier `PVE/minia2.yml`, qui liste les modules Python à charger, en tapant la commande :<br>
`conda env update -n minia2 --file PVE/minia2.yml`

### 3) Activer l'EVP __minia2__

Deux situations se rencontrent :

- Dans une console => on active l'EVP en tapant la commande :
  `conda activate minia2`

- Avec VSCode :
  
  Installe les extensions `Python` et `Jupyter` depuis la section `Extensions` du panneau à gauche dans la fenêtre VScode.
  
  => Utilise le raccourci clavier `SHIFT+CTRL+P` puis continue en tapant `Python: Select Interp` :

  ![bbbb](docs/Images/VSC_selectEVP_3.png)<br>
  puis sélectionner l'__interpréteur Python__ de l'EVP __minia2__<br>
  Idem pour `jupyter: select interpreter to start Jupyter serer`.<br><br>

### 4) Windows

- Définition de la variable __PYTHONPATH__ dans VSCode :<br>
Dans VSCode, rechercher ___terminal.integrated.env.windows___ dans les __settings__ (roue dentée en bas à gauche de la fenêtre VSCode ou raccourci clavier : "CTRL" + ",") $\leadsto$  cliquer sur <span style="color:green;font-family:arial; font-style:italic">Edit in Settings.json</soan> et ymettre le contenu : <br>

```json
   {
       "terminal.integrated.env.windows": {        
           "PYTHONPATH": "${workspaceFolder}"
       }
   }
```

- Activation du mode __développeur__: <br>
Dans la fenêtre _Paramètres > Système_, activer le ___Mode développeur___ :
![bbbb](docs/Images/W11_mode_dev.png)<br>

### 5) Post-installation du module _gymnasium_

Le but est d'installer dans l'arborescence du module __gymnasium__ des liens vers des fichiers Python du dossier `miniapterros` de façon à pouvoir utiliser les environnements _Continuous CartPole_ et _MiniApterros_ comme des environnements **natifs Gym**.

Avec le terminal intégré de VSCode, va dans le dosssier `<project_root>/scripts` et lance le programme Python `register_envs_gym.py` :

```sh
cd scripts
python register_envs_gym.py
```

### 6) Tester le bon fonctionnement

- Lance VSCode et ouvre le __dossier__ `miniapterros2`.
- Charge (dans l'odre) chacun des programmes du dossier `ai/src/simple_test` lance les avec `CTRL+F5` : un réseau PPO est entraîné à piloter plusieurs environnements (CartPole, LunarLandar...).<br>
**Windows** $\leadsto$ Si tu obtiens l'erreur ***RuntimeError: Calling torch.geqrf on a CPU tensor requires compiling PyTorch with LAPACK. Please use PyTorch built with LAPACK support.*** il faut installer e module Python **cpuonly** en tapnat la commande:<br>
`conda install cpuonly -c pytorch`
- Les vidéos des programmes de test du réseau entraîné sont sauvegardées dans les répertoire `ai/out/quick_tests/...`.

## Tester l'API PyBullet - Python

### Test élémentaire

Lance le programme Python `MiniApterros_GymEnv.py` qui est dans le dossier `apterros/pybullet:`

```bash
python apterros/pybullet/MiniAPTERROS_GymEnv.py
```

Le bloc `main` de ce programme teste la classe `MiniAPTERROS` définie dans le programme Python.<br>
La classe `MiniAPTERROS` définit un environement Gym pour le modèle simplifié du véhicule MiniAPTERROS : masse concentrée en son centre de gravité G et poussée appliquée au centre de poussée, différent de G.<br>
Les équations utilisées pour calculer la poussée sont détaillées dans le document `docs/Simu_MiniApterros_OpenAIGylm.pdf`

### Notebook

Ouvre le notebook `MiniAPTERROS_Pybullet.ipynb` du dossier `apterros\pybullet`. Il contient des cellules Python permettant de prendre en main :
- le lancement du simulateur PyBullet,
- le chargement du fichier URDF qui modélise le véhicule MiniAPTERROS,
- un exemple de suimulation de décollage du véhicule, avec le tracé des courbes Z(t) et PWM(t).

## Utilisation des scripts Python

- les scripts d'entraînement et tests doivent être executés à partir de la racine du projet :
  - VSCode : vérifie que le fichier `<racine_projet>/.vscode/launch.json` contient :
  
  ```json
  {
    // Use IntelliSense to learn about possible attributes.
    // Hover to view descriptions of existing attributes.
    // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
    "version": "0.2.0",
    "configurations": [
        {
            "name": "Python : Fichier actuel",
            "type": "python",
            "request": "launch",
            "program": "${file}",
            "console": "integratedTerminal",
            "env": {"PYTHONPATH": "${workspaceFolder}${pathSeparator}${env:PYTHONPATH}"},
            "justMyCode": false
        }
    ]
  }
  ```

## Utilisation des notebooks

Les notebooks Jupyter peuvent être utilisés de 2 façons :

- Avec l'EDI Jupyter :
  - si besoin, installe `jupyter` en tapant dans le terminal (ou console "Anaconda prompt") la commande : `conda install jupyter`.

  Pour lancer jupyte, tape `jupyter notebook` dans le terminal (ou console "Anaconda prompt"), __avec l'EVP `pyml` activé__.

- avec VSCode : l'utilisation des notebooks est intégré à l'atelier.

Nouveau notebook : partir d'une copie du notebook `Template.ipynb` qui permet :

- de faire exécuter le notebook à la racine du projet (évite les problèmes d'import relatif des modules)
- de prendre en compte automatiquement toute modification des environnements ou des sources python du projet sans avoir à re-importer les packages et relancer le kernel.

## Configuration de l'EDI Viual Studio Code

- `Features`
  - `terminal`
    - `Integrated: Inherit Env`: coché
- `Extensions`
  - `Jupyter`
    - `Always trust Notebook`: coché


