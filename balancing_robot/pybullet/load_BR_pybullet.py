import pybullet as p
import pybullet_data

import numpy as np
import os, time

pc = p.connect(p.GUI)
p.setAdditionalSearchPath(pybullet_data.getDataPath())  # used by loadURDF

p.resetSimulation()

p.setGravity(0,0,-10) # m/s^2
p.setTimeStep(0.01) # sec

planeId = p.loadURDF("plane.urdf")

startPos = [0, 0, 0.2]
startOrientation = p.getQuaternionFromEuler([0, 0, 0])
botId = p.loadURDF("./balancing_robot/pybullet/Balancing-robot-1.1.urdf", startPos, startOrientation)

while True:
    p.stepSimulation()
    
    keys = p.getKeyboardEvents(physicsClientId=pc)
    if ord('q') in keys and keys[ord('q')] & p.KEY_WAS_TRIGGERED:
        break
    
    time.sleep(1/240)

p.disconnect(physicsClientId=pc)