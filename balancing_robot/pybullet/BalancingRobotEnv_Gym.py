############################################################
# v1 06/06/2022 by JLC

import numpy as np
from math import pi, radians
import gymnasium as gym

import time

import pybullet as p
import pybullet_data

import balancing_robot.python.rewards as rewards

class BalancingRobotEnv_PyBullet(gym.Env):
    
    def __init__(self,
                 vers: str,
                 urdf: str,
                 wheel_diam_m: float,
                 dt: float,
                 theta_lim: int = 7,                 
                 x_lim: str = 0.1,
                 theta0_deg:float = None,
                 reward = "reward",
                 clip_action: bool = False,
                 veloc_mag: float = 1,
                 headless: bool = False,
                 verbose: int = 1):
       
        '''
        Parameters of the constructor of BalancingRobotEnv_PyBullet:
            vers:         "v0","v1" or "v2", to specify the BalancingRobot version
            urdf:         the URDF file that describes the robot architecture.
            wheel_diam_m: the diameter of the robot wheels, in meter.
            dt:           timestep in seconds for the CopSIM simulation
            theta_lim:    limit angle in degrees beyond which the training is over.
            x_lim:        limit displacement beyond which the training is over.
            reward        name of the reward function to use as a method in the class.
            clip_action:  wether to clip 'action' given by the neural network to keep its 
                          value in the 'action space' interval.
            veloc_mag:    linear velocity max in m/s for driving the BalancingRobot.
            headless:     wether to display 3D rendering or not.
                          For computing only headless=True (default value: True).
            verbose:      verbosity (displaying info), possible values:
                          0,1,2. Default value : 1.
        '''

        super().__init__()
        
        self.dt          = dt
        self.urdf_file   = urdf          
        self.wheel_diam  = wheel_diam_m  # wheel diameter in meter
        
        self.dist_orig   = 0             # distance from the original position at t=0
        
        # run the pybullet simulation process:
        mode = p.DIRECT if headless else p.GUI 
        self.pc_id = p.connect(mode)   # or p.DIRECT for non-graphical version
        p.setAdditionalSearchPath(pybullet_data.getDataPath())
        
         # Angle at which to fail the episode :
        self.theta_threshold_radians = radians(theta_lim)
        
        # The model will receive reward when abs(theta) is between 0 and theta_max_radians
        self.theta_max_radians = radians(theta_lim)

        # Position at which to fail the episode:
        self.x_threshold = x_lim        # [m]

        # The model will receive reward when abs(x) is between 0 and x_max
        self.x_max = x_lim              # [m]

        # Limit angle is set to 2*theta_threshold_radians so failing observation
        # is still within bounds:
        high = np.array([self.x_threshold*2, np.finfo(np.float32).max,
                         self.theta_threshold_radians*2, np.finfo(np.float32).max])
         
        self.min_action = -1.0
        self.max_action =  1.0
        
         # Reset angle:
        self.theta0_deg = theta0_deg

        self.action_space = gym.spaces.Box(low=self.min_action, high=self.max_action, shape=(1,))
        self.observation_space = gym.spaces.Box(-high, high, dtype=np.float32)
        
        self.nb_step = 0                # the total number of steps done
        self.steps_beyond_done = None
        self.state   = None             # the robot state vector (x, x_dot, theta, theta_dot)
        self.clip_action = clip_action

         # Set the max number of steps based on the environment version:
        self.vers    = "EnvSpec(BalancingRobot-"+vers.lower()+")"
        if self.vers == "EnvSpec(BalancingRobot-v0)" :
            self._max_episode_steps = 200
        elif self.vers == "EnvSpec(BalancingRobot-v1)" :
            self._max_episode_steps = 500
        elif self.vers == "EnvSpec(BalancingRobot-v2)" :
            self._max_episode_steps = 1000
        else:
            raise RuntimeError("spec must be 'v0', 'v1' or 'v2'")
        
        if verbose >= 0 :
            print(f'[BalancingRobot-{vers}] initialized, _max_episode_steps:{self._max_episode_steps}')
        
        
        # we define loading as a displacement because this will be the case
        # for our material BalancingRobot with the step motor driving the robot:

        self.veloc_mag = veloc_mag
        
        # The reward function tu use as the method reward:
        self.reward_func = reward
        
    def seed(self, s): self.seed = s
    
    def updateState(self, step=False):
        '''Get the balancing robot state [x, x', theta, theta'], and update the 'state' 
           attribute. If step is True, request PyBullet simulation to make a time step.'''

        if step:
            p.stepSimulation(physicsClientId=self.pc_id)
            self.nb_step += 1
        
        state = p.getJointStates(self.robot_id, jointIndices=[0, 1], physicsClientId=self.pc_id)
        wheel_angles     = np.array((state[0][0], state[1][0]))
        wheel_velocities = np.array((state[0][1], state[1][1]))
        #applied_torques  = np.array((state[0][3], state[1][3]))
        
        #print(f"wheel_angles: {wheel_angles}")
        #print(f"wheel_velocities: {wheel_velocities}")
        alpha     = wheel_angles.mean()
        alpha_dot = wheel_velocities.mean()
                
        R = self.wheel_diam/2
        x = R*alpha
        x_dot = R*alpha_dot
        
        robot_pos, robot_orn = p.getBasePositionAndOrientation(self.robot_id, physicsClientId=self.pc_id)
        robot_angle = p.getEulerFromQuaternion(robot_orn)[0]
        robot_dist  = np.sqrt(robot_pos[0]**2 + robot_pos[1]**2)  # distance only on XY plane
        
        linear_veloc, angular_veloc = p.getBaseVelocity(self.robot_id, physicsClientId=self.pc_id)    
        #print("EulerFromQuaternion:", p.getEulerFromQuaternion(robot_orn))
    
        #print(f"x        : {x}, robot_pos: {robot_pos}")
        #print(f"x_dot    : {x_dot}, linear_veloc[0]    : {linear_veloc[0]}")
        #print(f"angular   : {angular}")
        
        self.state = [x, x_dot, robot_angle, angular_veloc[1]]

        return np.array(self.state).astype('float32')
    
    def compute_done_reward(self, action):
            
        x, x_dot, theta, theta_dot = self.state

        #print("x, theta:", x, theta)
        
        done = abs(x) > self.x_threshold
        if self.theta0_deg is not None and self.theta0_deg < 45:
            # angle limit only if the initial position of the robot is vertical position up:
            done = done or abs(theta) > self.theta_threshold_radians 
        # limit the total number of steps:
        done = done or (self._max_episode_steps is not None and self.nb_step >= self._max_episode_steps)

        #print("self.nb_step:", self.nb_step)
        #JLC: improved reward computation:
        if not done:
            reward_instr = f"rewards.{self.reward_func}(self, action)"
            reward = eval(reward_instr) # eval() generates Python code
            #print(f"{reward_instr} -> {reward}")

            
        else:
            # pole has fell or cart is outside limits:
            if self.steps_beyond_done is None:
                # Pole just fell!
                self.steps_beyond_done = 0
                reward = 1.0
            else:
                if self.steps_beyond_done == 0:
                    print("You are calling 'step()' even though this environment "+\
                          "has already returned done = True. \nYou should always "+\
                          "call 'reset()' once you receive 'done = True' "+\
                          "-- any further steps are undefined behavior.")
                self.steps_beyond_done += 1
                reward = 0.0
                
        #print("done, reward", done, reward)
        
        return done, reward
    
    def step(self, action):
        """Apply control and run one step of simulation."""
            
        try :
            assert self.action_space.contains(action), \
                "%r (%s) invalid" % (action, type(action))
        except :
            if self.clip_action: np.clip(action, self.min_action, self.max_action)
               
        angular_veloc = self.veloc_mag*float(action)
        
        # step
        p.setJointMotorControlArray(bodyIndex=self.robot_id,
                                    jointIndices=[0,1],
                                    controlMode=p.VELOCITY_CONTROL,
                                    targetVelocities=[angular_veloc, angular_veloc],
                                    physicsClientId=self.pc_id)
        
        p.stepSimulation(physicsClientId=self.pc_id)
        self.nb_step += 1        
        
        self.updateState() # update the attribute self.state)
        done, reward = self.compute_done_reward(action)
        
        #print("state, action", self.state, action)
        return np.array(self.state), reward, done, {}

    def reset(self, dt=None, theta_deg=None):
        """Resets simulation and spawn every object in its initial position."""
        
        print(f"BalancingRobot reset - last #steps : {self.nb_step}")
        
        self.nb_step = 0
        self.steps_beyond_done = None
        
        if dt is not None : self.dt = dt
        if theta_deg is None:
            self.state = np.random.uniform(low=-0.05, high=0.05, size=(4,))
        else:
            self.state = [0, 0, np.radians(theta_deg), 0]
        
        x, x_dot, theta, theta_dot = self.state
        self.state = 0, 0, theta, 0
        
        # After reset every parameter has to be set
        p.resetSimulation(physicsClientId=self.pc_id)
        p.setTimeStep(self.dt, physicsClientId=self.pc_id)
        p.setGravity(0, 0, -9.81, physicsClientId=self.pc_id)
        
        self.plane_id = p.loadURDF("plane.urdf", physicsClientId=self.pc_id)
        
        start_pos = [0, 0, 0.128]
        start_orn = p.getQuaternionFromEuler((0, theta, 0), physicsClientId=self.pc_id)
        self.robot_id = p.loadURDF(self.urdf_file, start_pos, start_orn, physicsClientId=self.pc_id)
        p.resetDebugVisualizerCamera(cameraDistance=1., cameraYaw=45, 
                                     cameraPitch=10, 
                                     cameraTargetPosition=[0, 0, 0.5], physicsClientId=self.pc_id)
        return np.array(self.state)

    def reward(self):
        _, robot_orn = p.getBasePositionAndOrientation(self.robot_id, physicsClientId=self.pc_id)
        robot_euler = p.getEulerFromQuaternion(robot_orn)
        return (1 - abs(robot_euler[1]))*0.1 - abs(self.vt - self.vd) * 0.01
    
    
    def runVeloc(self, nb_step: int, angular_veloc: float, dt: float):
        '''To run the BalancingRobot with velocity imposed.'''

        self.dt = dt
        print(f"Ready to run {nb_step} steps of velocity with timestep={self.dt} s")
        States = []
        state = self.reset(self.dt, theta_deg=0)
        print("state:",state)

        for _ in range(nb_step):
            
            # step
            p.setJointMotorControlArray(bodyIndex=self.robot_id,
                                        jointIndices=[0,1],
                                        controlMode=p.VELOCITY_CONTROL,
                                        targetVelocities=[angular_veloc, angular_veloc],
                                        physicsClientId=self.pc_id)
            
            p.stepSimulation(physicsClientId=self.pc_id)
            self.nb_step += 1        
        
            # observe (sets the attribute self.state):
            state = self.updateState()       
            print(state)     
            States.append(state)
            time.sleep(0.01)
            rep=input("mess")
        return np.array(States)

    def close(self):
        p.disconnect(physicsClientId=self.pc_id)

if __name__ == '__main__':
    
    # scene path relative to the 'project root' directory:
    urdf = "./balancing_robot/pybullet/Balancing-robot-1.1.urdf"
    
    env   = BalancingRobotEnv_PyBullet(vers="v2",
                                       urdf=urdf,
                                       dt=0.01,
                                       wheel_diam_m=9e-2,
                                       veloc_mag=6,
                                       headless = None,
                                       theta_lim=10,
                                       x_lim=0.2)
    
    print(f"starting new simulation with <{urdf}>")

    steps = 500
    veloc = 1       # radian/sec
    dt    = 0.01    # seconde
    data = env.runVeloc(nb_step=steps, angular_veloc=veloc, dt=dt)
    title = f'{steps} steps of velocity run with velocity:{veloc:.2f} rad/sec and dt:{dt} ms'
    
    env.close()