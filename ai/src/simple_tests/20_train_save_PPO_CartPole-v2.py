# Quick test to see if you can can train a PPO network from "stable baseline3" 
# with a simple Gym environnement like "CartPole-v2"
#
# Nota: PPO can manage discrete or continuous actions. 
#       => For CartPole-v2 continuous action in [-1, 1] is used .
#
# Two Python programs are used:
#   20_train_save_PPO_CartPole-v2.py  to train the network and save its weights.
#   21_load_test_PPO_CartPole-v2.py   to load a trained model and test its bahaviour.
#

from time import time
import gymnasium as gym
from stable_baselines3 import PPO

# creates the custom cartpole (robot with lateral movement and a stick) environment

# WARNING : the syntaxe "gym.make('CartPole-v2')" requires the modification of your directory 
# ..somewhere../miniconda3/envs/minia2/lib/python3.10/site-packages/gymnasium/ (see <project_root>/README.md)
env = gym.make('CartPole-v2')

# create and train the AI using Deep Reinforcement Learning (DRL) and Proximal Policy Optimisation (PPO)
# see https://stable-baselines3.readthedocs.io/en/master/modules/ppo.html
model = PPO('MlpPolicy', 
            env, 
            verbose=1,
            tensorboard_log="ai/models/test_PPO_CartPole-v2")

t0 = time() # start chrono

# Train the agent:
model.learn(total_timesteps=100_000)

# compute elapsed time:
t = int(time()-t0)
h = int(t//3600)
m = int((t - h*3600)//60)
print(f"Training elapsed time : {h:02d}h {m:02d}m")

# save trained model
model.save("ai/models/test_PPO_CartPole-v2/model.zip")
