#
# Run this program to load and test a trained PPO network from "stable baseline3" 
# with a simple Gym environnement like "CartPole-v1"
#
# Two Python programs are used:
#   10_train_save_PPO_CartPole-v1.py  to train the network and save its weights.
#   11_load_test_PPO_CartPole-v1.py   to load a trained model and test its bahaviour.
#

import gymnasium as gym
from stable_baselines3 import PPO
from ai.src.visu.gym_render import model_render

# creates the cartpole (robot with lateral movement and a stick) environment
env = gym.make('CartPole-v1', render_mode="rgb_array")

# load a trained Reinforcment Learning and Proximal Policy Optimisation (PPO) model
model = PPO.load("ai/models/test_PPO_CartPole-v1/model.zip")

# display how the trained model performs and save it as a video in ./out
model_render(model, 
             env,  
             output_dir="ai/out/quick_tests/test_PPO_CartPole-v1", 
             step_nb=1000, 
             deterministic=False)
