#
# Run this program to load and test a trained PPO network from "stable baseline3" 
# with a simple Gym environnement like "LunarLanderContinuous-v2". 

# Two Python programs are used:
#   70_train_save_PPO_LunarLanderContinuous-v2.py  to train the network and save its weights.
#   71_load_test_PPO_LunarLanderContinuous-v2.py   to load a trained model and test its behaviour.
#

import gymnasium as gym
from stable_baselines3 import PPO
from ai.src.visu.gym_render import model_render

# Create environment:
env = gym.make('LunarLanderContinuous-v2', render_mode="rgb_array")

# Load the trained agent:
model = PPO.load("ai/models/test_PPO_LunarLanderContinuous-v2/model.zip")

# display how the trained model performs and save it as a video:
model_render(model, 
             env,  
             output_dir="ai/out/quick_tests/test_PPO_LunarLanderContinuous-v2", 
             step_nb=2000,
             deterministic=True)

