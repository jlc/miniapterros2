#
# Run this program to load and test a trained PPO network from "stable baseline3" 
# with a simple Gym environnement like the "continuous_cartpole"
#
# Two Python programs are used:
#   20_train_save_PPO_CartPole-v2.py  to train the network and save its weights.
#   21_load_test_PPO_CartPole-v2.py   to load a trained model and test its behaviour.
#

import gymnasium as gym
from stable_baselines3 import PPO
from ai.src.visu.gym_render import model_render

# creates the custom cartpole (robot with lateral movement and a stick) environment:

# WARNING : the syntaxe "gym.make('CartPole-v2')" requires the modification of your directory 
# /..path../miniconda3/envs/pyml/lib/python3.x/site-packages/gym/ (see <project_root>/README.md)

env = gym.make('CartPole-v2', render_mode="rgb_array")

# Load the trained agent:
model = PPO.load("ai/models/test_PPO_CartPole-v2/model.zip")

# display how the trained model performs and save it as a video in ./out
model_render(model,
             env,
             output_dir="ai/out/quick_tests/test_PPO_CartPole-v2",
             step_nb=1000,
             deterministic=False)
