#
# Quick test to see if you can can train a DQN network from "stable baseline3" 
# with a simple Gym environnement like "LunarLander-v2"
#
# Nota: DQN can give discrete or continuous actions. 
#       For LunarLander-v2 discrete action (left/right) is used.#
#
# Two Python programs are used:
#   30_train_save_DQN_LunarLander-v2.py  to train the network and save its weights.
#   31_load_test_DQN_LunarLander-v2.py   to load a trained model and test its bahaviour.
#

from time import time
import gymnasium as gym
from stable_baselines3 import DQN

# Create environment
env = gym.make('LunarLander-v2')

# Instantiate the agent
model = DQN('MlpPolicy',
            env,
            verbose=1,
            tensorboard_log="ai/models/test_DQN_LunarLander-v2")

t0 = time() # start chrono

# Train the agent:
model.learn(total_timesteps=500_000)

# compute elapsed time:
t = int(time()-t0)
h = int(t//3600)
m = int((t - h*3600)//60)
print(f"Training elapsed time : {h:2d}h {m:2d}m")

# save trained model
model.save("ai/models/test_DQN_LunarLander-v2/model.zip")
