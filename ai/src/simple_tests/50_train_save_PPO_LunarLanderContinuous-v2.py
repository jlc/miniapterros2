#
# Quick test to see if you can can train a PPO network from "stable baseline3" 
# with a simple Gym environnement like "LunarLanderContinuous-v2"
#
# Nota: PPO can gives continuous actions. 
#
# Two Python programs are used:
#   60_train_save_PPO_LunarLanderContinuous-v2.py  to train the network and save its weights.
#   61_load_test_PPO_LunarLanderContinuous-v2.py   to load a trained model and test its bahaviour.
#

from time import time
import gymnasium as gym
from stable_baselines3 import PPO
from stable_baselines3.common.evaluation import evaluate_policy

# Create environment:
env = gym.make('LunarLanderContinuous-v2')

# Instantiate the agent:
model = PPO('MlpPolicy',
            env,
            verbose=1,
            batch_size=32,
            n_epochs=15,
            seed=1234,
            tensorboard_log="ai/models/test_PPO_LunarLanderContinuous-v2")

t0 = time() # start chrono

# Train the agent:
model.learn(total_timesteps=500_000)

# Compute elapsed time:
t = int(time()-t0)
h = int(t//3600)
m = int((t - h*3600)//60)
print(f"Training elapsed time : {h:02d}h {m:02d}m")

# save trained model:
model.save("ai/models/test_PPO_LunarLanderContinuous-v2/model.zip")
