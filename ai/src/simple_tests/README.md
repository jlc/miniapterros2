# Content of directory `simple_test` in `<project_root_dir>/ai/src`

This directory contains Python programs that can be used to **quickly test** :

- the configuration of your _Visual Studio EDI_
- your installation of _gymnasium_ and _stable baseline3_
- your installation of _PyNullet_ and its _Python API_.

## 1/ Test PPO with CartPole-v1 (discrete action) in Gym simulator

- to train the PPO network and save its weights type in the VSCode terminal:<br>
`python ai/src/simple_tests/10_train_save_PPO_CartPole-v1.py`<br>
The trained model is saved as **model.zip** in **ai/models/test_PPO_CartPol-v1**.

- to load the trained PPO neural wights and test its behavior type in the VSCode terminal:<br>
`python ai/src/simple_tests/11_load_test_PPO_CartPole-v1.py`<br>
The programme loads the runs the trained PPO : the result can be seen as a mp4 video file in **ai/out/quick_tests/test_PPO_CartPole-v1**.

## 2/ Test PPO with CartPole-v2 (continuous action) in Gym simulator

- to train the PPO network and save its weights type in the VSCode terminal:<br>
`python ai/src/simple_tests/20_train_save_PPO_CartPole-v2.py`<br>
The trained model is saved as **model.zip** in **ai/models/test_PPO_CartPol-v2**.

- to load the trained PPO neural wights and test its behavior type in the VSCode terminal:<br>
`python ai/src/simple_tests/21_load_test_PPO_CartPole-v2.py`<br>
The programme loads the runs the trained PPO : the result can be seen as a mp4 video file in **ai/out/quick_tests/test_PPO_CartPole-v2**.

## 3/ Test DQN with LunarLander-v2 in Gym simulator

- to train the DQN network and save its weights type in the VSCode terminal:<br>
`python ai/src/simple_tests/30_train_save_DQN_LunarLander-v2.py`<br>
The trained model is saved as **model.zip** in **ai/models/test_DQN_LunarLander-v2**.

- to load the trained DQN neural wights and test its behavior type in the VSCode terminal:<br>
`python ai/src/simple_tests/31_load_test_DQN_LunarLander-v2.py`<br>
The programme loads the runs the trained DQN : the result can be seen as a mp4 video file in **ai/out/quick_tests/test_DQN_LunarLander-v2**.

## 4/ Test PPO with LunarLander-v2 in Gym simulator

- to train the PPO network and save its weights type in the VSCode terminal:<br>
`python ai/src/simple_tests/40_train_save_PPO_LunarLander-v2.py`<br>
The trained model is saved as **model.zip** in **ai/models/test_PPO_LunarLander-v2**.

- to load the trained DQN neural wights and test its behavior type in the VSCode terminal:<br>
`python ai/src/simple_tests/41_load_test_PPO_LunarLander-v2.py`<br>
The programme loads the runs the trained PPO : the result can be seen as a mp4 video file in **ai/out/quick_tests/test_PPO_LunarLander-v2**.

## 5/ Test PPO with LunarLanderContinuous-v2 in Gym simulator

- to train the PPO network and save its weights type in the VSCode terminal:<br>
`python ai/src/simple_tests/50_train_save_PPO_LunarLanderContinuous-v2.py`<br>
The trained model is saved as **model.zip** in **ai/models/test_PPO_LunarLander-v2**.

- to load the trained DQN neural wights and test its behavior type in the VSCode terminal:<br>
`python ai/src/simple_tests/51_load_test_PPO_LunarLanderContinuous-v2.py`<br>
The programme loads the runs the trained PPO : the result can be seen as a mp4 video file in **ai/out/quick_tests/test_PPO_LunarLander-v2**.
