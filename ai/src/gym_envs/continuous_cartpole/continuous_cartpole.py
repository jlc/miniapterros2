import math
import numpy as np

import gymnasium as gym
from gymnasium import logger, spaces

class ContinuousCartPoleEnv(gym.Env):
    
    metadata = {
        'render_modes': ['human', 'rgb_array'],
        'render_fps': 20,
        'seed': 1234,
        'cart_mass': 1.0,
        'pole_mass': 0.1,
        'pole_length': 0.5,
        'force_mag': 10,
        'tau': 0.02,
        'max_step': None,
    }

    def __init__(self, render_mode = None, verbose=0):

        super().__init__()
        
        # attributes to be set using metadata mechanism:
        self.masscart   = None
        self.masspole   = None
        self.length     = None  # actually half the pole's length
        self.force_mag  = None
        self.tau        = None  # seconds between state updates
        self.total_mass = None
        self.polemass_length = None
        
        # other attributes:
        self.gravity    =  9.8
        self.min_action = -1.0
        self.max_action =  1.0

        # Angle at which to fail the episode
        self.theta_threshold_radians = 12 * 2 * math.pi / 360
        self.x_threshold = 2.4

        # Angle limit set to 2 * theta_threshold_radians so failing observation
        # is still within bounds

        high = np.array([2*self.x_threshold, np.finfo(np.float32).max,
                         2*self.theta_threshold_radians, np.finfo(np.float32).max],
                        dtype=np.float32)

        self.action_space = spaces.Box(low=self.min_action,
                                       high=self.max_action,
                                       shape=(1,), dtype=np.float32)
        self.observation_space = spaces.Box(-high, high, dtype=np.float32)

        self.render_mode = render_mode
        self.verbose = verbose
        self.viewer  = None
        self.state   = None
        
        self.screen_width = 600
        self.screen_height = 400
        self.screen = None
        self.clock = None
        self.isopen = True
        
        self.steps_beyond_terminated = None
        self.initialized = False
        self.setAttributesFromMetadata()


    def stepPhysics(self, force):
        '''
        Computes the physics for the current step (explicit time integration)
        '''
        x, x_dot, theta, theta_dot = self.state
        costheta = math.cos(theta)
        sintheta = math.sin(theta)
        temp = (force + self.polemass_length * theta_dot**2 * sintheta) / self.total_mass
        thetaacc = (self.gravity*sintheta - costheta*temp)/(self.length*(4.0/3.0 - self.masspole*costheta**2 / self.total_mass))
        xacc = temp - self.polemass_length*thetaacc*costheta/self.total_mass
        
        x = x + self.tau*x_dot
        x_dot = x_dot + self.tau*xacc
        theta = theta + self.tau*theta_dot
        theta_dot = theta_dot + self.tau*thetaacc
        
        return (x, x_dot, theta, theta_dot)
    
    def setAttributesFromMetadata(self): #inutile car on
        self.seed_value  = ContinuousCartPoleEnv.metadata['seed']
        self.masscart    = ContinuousCartPoleEnv.metadata['cart_mass']
        self.masspole    = ContinuousCartPoleEnv.metadata['pole_mass']
        self.length      = ContinuousCartPoleEnv.metadata['pole_length']
        self.tau         = ContinuousCartPoleEnv.metadata['tau']
        self.max_step    = ContinuousCartPoleEnv.metadata['max_step']
        self.force_mag   = ContinuousCartPoleEnv.metadata['force_mag']
        
        self.polemass_length = (self.masspole * self.length)
        self.total_mass      = (self.masspole + self.masscart)

        if self.verbose >= 1:
            print(f"seed_value : {self.seed_value}")
            print(f"cart_mass  : {self.masscart}")
            print(f"pole_mass  : {self.masspole}")
            print(f"pole_length: {self.length}")
            print(f"tau        : {self.tau}")
            print(f"max_step   : {self.max_step}")
        
        self.initialized = True
        
    def step(self, action):
        '''
        Run one timestep of the environment’s dynamics using the agent actions.
        When the end of an episode is reached (terminated or truncated), it is necessary to call reset() to reset 
        this environment’s state for the next episode.
        
        The episode ends if any one of the following occurs:

        1. Termination: Pole Angle is greater than ±12°
        2. Termination: Cart Position is greater than ±2.4 (center of the cart reaches the edge of the display)
        3. Truncation: Episode length is greater than 500 (200 for v0)
        '''
        if not self.initialized : self.setAttributesFromMetadata()

        try :
            assert self.action_space.contains(action), f"{action!r} ({type(action)}) invalid"
        except :
            np.clip(action,-1,1)
            #print("action range exception , ignored, clipped")

        # Cast action to float to strip np trappings
        force = self.force_mag*action[0]
        
        # compute the new state uner the given action
        self.state = self.stepPhysics(force)
        x, x_dot, theta, theta_dot = self.state
        
        terminated = bool(x < -self.x_threshold 
                       or x > self.x_threshold 
                       or theta < -self.theta_threshold_radians 
                       or theta > self.theta_threshold_radians)

        if not terminated:
            reward = 1.0
        elif self.steps_beyond_terminated is None:
            # Pole just fell!
            self.steps_beyond_terminated = 0
            reward = 1.0
        else:
            if self.steps_beyond_terminated == 0:
                logger.warn(
                    "You are calling 'step()' even though this "
                    "environment has already returned terminated = True. You "
                    "should always call 'reset()' once you receive 'terminated = "
                    "True' -- any further steps are undefined behavior."
                )
            self.steps_beyond_terminated += 1
            reward = 0.0
        
        return np.array(self.state, dtype=np.float32), reward, terminated, False, {}

    def reset(self, seed=None, options=None):
        '''
        reset is run at each beginning of episode.
        '''
        if seed == None: seed = self.seed_value
        super().reset(seed=seed)
        
        if not self.initialized : self.setAttributesFromMetadata()
        
        self.state = self.np_random.uniform(low=-0.05, high=0.05, size=(4,))
        self.steps_beyond_terminated = None
        return np.array(self.state, dtype=np.float32), {}

    def render(self):
        if self.render_mode is None:
            assert self.spec is not None
            gym.logger.warn(
                "You are calling render method without specifying any render mode. "
                "You can specify the render_mode at initialization, "
                f'e.g. gym.make("{self.spec.id}", render_mode="rgb_array")'
            )
            return

        try:
            import pygame
            from pygame import gfxdraw
        except ImportError as e:
            raise DependencyNotInstalled(
                "pygame is not installed, run `pip install gymnasium[classic-control]`"
            ) from e

        if self.screen is None:
            pygame.init()
            if self.render_mode == "human":
                pygame.display.init()
                self.screen = pygame.display.set_mode(
                    (self.screen_width, self.screen_height)
                )
            else:  # mode == "rgb_array"
                self.screen = pygame.Surface((self.screen_width, self.screen_height))
        if self.clock is None:
            self.clock = pygame.time.Clock()

        world_width = self.x_threshold * 2
        scale = self.screen_width / world_width
        polewidth = 10.0
        polelen = scale * (2 * self.length)
        cartwidth = 50.0
        cartheight = 30.0

        if self.state is None:
            return None

        x = self.state

        self.surf = pygame.Surface((self.screen_width, self.screen_height))
        self.surf.fill((255, 255, 255))

        l, r, t, b = -cartwidth / 2, cartwidth / 2, cartheight / 2, -cartheight / 2
        axleoffset = cartheight / 4.0
        cartx = x[0] * scale + self.screen_width / 2.0  # MIDDLE OF CART
        carty = 100  # TOP OF CART
        cart_coords = [(l, b), (l, t), (r, t), (r, b)]
        cart_coords = [(c[0] + cartx, c[1] + carty) for c in cart_coords]
        gfxdraw.aapolygon(self.surf, cart_coords, (0, 0, 0))
        gfxdraw.filled_polygon(self.surf, cart_coords, (0, 0, 0))

        l, r, t, b = (
            -polewidth / 2,
            polewidth / 2,
            polelen - polewidth / 2,
            -polewidth / 2,
        )

        pole_coords = []
        for coord in [(l, b), (l, t), (r, t), (r, b)]:
            coord = pygame.math.Vector2(coord).rotate_rad(-x[2])
            coord = (coord[0] + cartx, coord[1] + carty + axleoffset)
            pole_coords.append(coord)
        gfxdraw.aapolygon(self.surf, pole_coords, (202, 152, 101))
        gfxdraw.filled_polygon(self.surf, pole_coords, (202, 152, 101))

        gfxdraw.aacircle(
            self.surf,
            int(cartx),
            int(carty + axleoffset),
            int(polewidth / 2),
            (129, 132, 203),
        )
        gfxdraw.filled_circle(
            self.surf,
            int(cartx),
            int(carty + axleoffset),
            int(polewidth / 2),
            (129, 132, 203),
        )

        gfxdraw.hline(self.surf, 0, self.screen_width, carty, (0, 0, 0))

        self.surf = pygame.transform.flip(self.surf, False, True)
        self.screen.blit(self.surf, (0, 0))
        if self.render_mode == "human":
            pygame.event.pump()
            self.clock.tick(self.metadata["render_fps"])
            pygame.display.flip()

        elif self.render_mode == "rgb_array":
            return np.transpose(
                np.array(pygame.surfarray.pixels3d(self.screen)), axes=(1, 0, 2)
            )


    def close(self):
        if self.screen is not None:
            import pygame

            pygame.display.quit()
            pygame.quit()
            self.isopen = False

if __name__ == "__main__":
    
    from gymnasium.utils.env_checker import check_env
    
    env = ContinuousCartPoleEnv(verbose=1)
    print(f"env.observation_space:{env.observation_space}")
    
    check_env(env, skip_render_check=True)
