#
# Define the method "reward" that will replace the one defined in
# the "BasicApterrosEnv" class in file mini_apterros.py :
#
import numpy as np

def reward_takeoff_2(self):  
    
    t = self.cur_time
    action_n      = self.action_n
    z_target      = self.z_target
    prev_action_n = self.prev_action_n
    z, z_dot      = self.state
    prev_z, prev_z_dot = self.prev_state
    z_mean        = np.mean(self.z_que)
    
    #R = 1 - abs(z_mean - z_target)**3 - 0.2*(prev_action_n - action_n)**2 - 0.2*(prev_z - z)**2 - 0.2*(z_dot - prev_z_dot)**2 
    #R = 1 - abs(z_mean - z_target)**3 - (prev_action_n - action_n)**2 - (prev_z - z)**2 - (z_dot - prev_z_dot)**2 
    R  = 1 - abs(z_mean - z_target) - (abs(z_dot) > .8) - (t < 0.2)*(action_n > .9)*0.6
    #R = 1 - 2*abs(z - z_target)**2 - 2*(prev_z - z)**2  - 0.5*(action_n > .95)

    return R

def reward_landing_2(self):  
    
    t = self.cur_time
    action_n      = self.action_n
    z_target      = self.z_target
    prev_action_n = self.prev_action_n
    z, z_dot    = self.state
    prev_z, prev_z_dot = self.prev_state
    z_que_mean = np.mean(self.z_que)
    
    R = 1 - 2*z_que_mean**3 - (prev_action_n - action_n)**2 - (prev_z - z)**2 - (abs(z_dot) > 0.4)
    #R = 1 - abs(z_que_mean)*0.1 - (z > (prev_z - 0.005))*2 - (100*abs(action_n - prev_action_n) > 5)*2 - (action_n > .9)*0.6

    return R

def reward_landing_1(self, ):  
 
    t = self.cur_time
    action_n = self.action_n
    
    PWM = 100*action_n
    prevPWM  = 100*self.prev_action_n
    z, z_dot = self.state
    prev_z, prev_z_dot = self.prev_state
    R = 0

    # reward values of PWM less than 85% :
    if PWM <= 85 : R += 4

    # reward small variation of PWM, penalize high PWM values:
    if abs(prevPWM - PWM) <= 5:
        R += 2
    else:
        R -= 2

    # reward a slow decrease in height, penalize high increase:
    if 0.005 < prev_z - z <= 0.02:
        R += 4
    else:
        R -= 4

    # almost on the ground reward turning off the turbines
    if z <= 0.01:
        if PWM <= 10 : R += 2

    return R/12