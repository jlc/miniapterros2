###########################################################################
# Copyright 2022 Jean-Luc CHARLES
# Created: 2022-07-29
# version: 1.1 - 14 Dec 2023 
# License: GNU GPL-3.0-or-later
###########################################################################

import numpy as np
from collections import deque

import gymnasium as gym
from gymnasium import logger, spaces

import ai.src.gym_envs.mini_apterros.rewards as rewards

def reward(self, action, state, t):
    raise RuntimeError("This method must be defined by yourself and set as a metadata")

class BasicMiniApterrosEnv(gym.Env):
    ''' 
    This class defines the Gym Environment for the miniapterros simulations. For the 'basic' 
    MiniAPTERROS only the thrust power is controlled, not the thrust direction.
    
    '''
    #
    # metada are set by the script ai/src/run/test_model_gym.py
    #
    # metadata = {'render_modes': ['human', 'rgb_array'],
    #             'render_fps': None ,
    #             'seed': None,
    #             'total_mass': None,
    #             'tau': None,
    #             'z_target': None,
    #             'reward_f': None,
    #             'max_step': None,
    #             'static_frict': None,
    #             'stage': None
    #             }

    Z_QUE_LEN = 20  # length of the queue on Z    
    
    def __init__(self, verbose=1):
        
        super().__init__()

        # attributes to be set using metadata mechanism:
        self.seed_value   = None
        self.total_mass   = None    # kg, to be confirmed
        self.tau          = None    # time step [s]
        self.z_target     = None    # target altitude [m]
        self.stage        = None    # the stage ('takeoff', 'station', 'landing')
        self.max_step     = None    # max number of steps to run
        self.static_frict = None    # static friction force [N]

        # other attributes:
        self.state        = None    # state = [z, z_dot, M]
        self.prev_state   = None    # pre_state = [prev_z, prev_z_dot, M]
        self.gravity      = 9.81
        self.initialized  = False
        self.stateArray   = []
        self.action_n     = 0    # The normalized PPO action n range [0., 1.]
        self.prev_action_n= 0
        self.verbose      = verbose
        self.cur_step     = 0
        self.cur_time     = 0       # current physical time [s]
        self.z_que        = deque([], self.Z_QUE_LEN)    # moving window of 20 successive values of z
        self.viewer       = None
        self.screen       = None
        self.steps_beyond_terminated = None
        self.initialized  = False
        
        # Action space:
        self.min_action   = -1.0  # 'action' comming from PPO is in range [-1, 1] which is
        self.max_action   =  1.0  # scaled later to [0, 1] by taking (action+1)/2
        self.action_space = spaces.Box(low=self.min_action, high=self.max_action, shape=(1,))
        
        # Observation space : (M, z, z_dot, z_target):
        #   z       : current altitude in [0, +inf[
        #   z_dot   : current velocity in ]-inf, +inf[
        #   z_target: target altitude 
        #   M       : mass of the vehicule in [5, 10] kg
        inf = np.finfo(np.float32).max
        self.observation_space = spaces.Box(low = np.array([ 0, -inf, 0]), 
                                            high = np.array([inf, inf, inf]),
                                            dtype = np.float32)
        
        # Initialize the attributes from metadata:
        self.setAttributesFromMetadata()
        
        self._max_episode_steps = self.max_step
        if self.verbose >= 1 :
            print(f'[BasicMiniApterrosEnv] initialized, _max_episode_steps:{self._max_episode_steps}')

    def thrust(self, action) :
        """ 
        To convert the action (input in  [0,1]) into the thrust force in Newton
        """
        PWM = 100*action        # convert the PPO action in PWM to command the thrust
        res = 4*(0.32*PWM - 2)  # from calibration
        
        #prop_input = action
        #res = 4*self.gravity*3.71*prop_input #caracterisation 2021

        #res = 4*9.81*(3.09*prop_input) # D'après JLC
        #res = 4*9.81*(3.25e-2*prop_input*100 - 3.09e-1) # D'après étalonnage  de novembre 2019
        
        if res < 0 : res = 0
        #print(f"action:{action:6.2f}, thrust:{res:6.2f} N")
        return res

    def stepPhysics(self, thrust):
        
        def Cauchy_F(Z, t, T=thrust, m=self.total_mass, g=self.gravity, friction=self.static_frict):
            '''Cauchy Function:
               Z is the vector [z, z_dot], the Cauchy function f'(Z,t) returns the 
               vector Z_dot = (z_dot, z_dot_dot).
               In our problem, the 2nd order diff. equation is: z_dot_dot = (T-Friction)/m -g.
            '''
            return np.array([Z[1], (T-friction)/m - g])       

        t, dt = self.cur_time, self.tau

        # Z is the vector [z, z_dot]:
        Z  = np.array(self.state[:2])
        K1 = Cauchy_F(Z, t)
        K2 = Cauchy_F(Z+K1*dt/2, t)
        K3 = Cauchy_F(Z+K2*dt/2, t)
        K4 = Cauchy_F(Z+K3*dt, t)
        Z += (K1 + 2*K2 + 2*K3 + K4)*dt/6

        if Z[0] < 0:   #Z must be positive
            Z[0] = 0
            Z[1] = - 0.5* Z[1]

        return Z

    def setAttributesFromMetadata(self): 
        '''
        To set the actual attributes with values coming from the metadata dict.
        '''
        self.seed_value   = BasicMiniApterrosEnv.metadata['seed']
        self.total_mass   = BasicMiniApterrosEnv.metadata['total_mass']
        self.z_target     = BasicMiniApterrosEnv.metadata['z_target']
        self.tau          = BasicMiniApterrosEnv.metadata['tau']
        self.stage        = BasicMiniApterrosEnv.metadata['stage']
        self.max_step     = BasicMiniApterrosEnv.metadata['max_step']
        self.static_frict = BasicMiniApterrosEnv.metadata['static_frict']
        self.reward_f     = BasicMiniApterrosEnv.metadata['reward_f']
        
        if self.verbose >= 1:
            print(f"seed        : {self.seed_value}")
            print(f"total_mass  : {self.total_mass}")
            print(f"z_target    : {self.z_target}")
            print(f"tau         : {self.tau}")
            print(f"stage       : {self.stage}")
            print(f"max_step    : {self.max_step}")
            print(f"static_frict: {self.static_frict}")
            print(f"reward_f    : {self.reward_f}")

        self.initialized = True

    def step(self, action:float):
        '''
        To run one timestep of the environment’s dynamics using the agent actions.
        When the end of an episode is reached (terminated or truncated), it is necessary to call reset() to reset 
        this environment’s state for the next episode.
        
        The episode ends if any one of the following occurs:
        1. Termination: the rocket has reached the Z target if taking off, or reached
           the ground if landing.
        2. Truncation: Episode length is greater than self.max_step .
        '''
        
        if self.initialized is False: raise ValueError(f"Object not initialised for metaData")
    
        # normalized action: transform 'action' from range [-1,1] to range[0,1]:
        self.action_n = (action[0]+1)/2

        # get thrust in Newton from action_n:
        thrust = self.thrust(self.action_n)

        # compute self.state = [z, z_dot, M]
        #print(f"\tstate: {self.state}", end="")
        self.state = self.stepPhysics(thrust).tolist() + [self.total_mass]
        z = self.state[0]
        self.z_que.append(z)
        z_mean = np.mean(self.z_que)
        #print(f"\tafter step: {self.state}")
       
        if self.cur_step == 0 : print(f" step: {self.cur_step}", end="")
        self.cur_step  += 1        
        self.cur_time  += self.tau
        
        terminated, truncated = False, False

        if self.stage == "takeoff" and abs(z_mean - self.z_target) <= 0.01*self.z_target: 
            terminated = True
            reward = 1
            print(f" step: {self.cur_step} [{self.stage}] terminated (z_mean ~ z_target)")
            
        elif self.stage == "landing" and z_mean <= 0.005:
            terminated = True
            reward = 1
            print(f" step: {self.cur_step} [{self.stage}] terminated (z_mean <= epsilon)")            
            
        elif self.max_step is not None and self.cur_step == self.max_step:
            truncated  = True
            reward = -10
            print(f" step: {self.cur_step} truncated by max_step, z_que_mean: "
                  f"{np.array(self.z_que).mean():.3f}\n")     
        
        if terminated == False and truncated == False:
            reward_instr = f"rewards.{self.reward_f}(self)"
            reward = eval(reward_instr) # eval() generates Python code
            
        elif self.steps_beyond_terminated is None:
            # Apterros terminated for the first time, but the exprerience is still
            # going on
            self.steps_beyond_terminated = 0
            
        else:
            if self.steps_beyond_terminated == 0:
                logger.warn(
                    "You are calling 'step()' even though this "
                    "environment has already returned terminated = True. You "
                    "should always call 'reset()' once you receive 'terminated = "
                    "True' -- any further steps are undefined behavior."
                )
            self.steps_beyond_terminated += 1
            reward = 0.0
            
        self.prev_action_n = self.action_n
        self.prev_state    = self.state

        info = {'t':self.cur_time, 'z':z, 'action_n': self.action_n}
        return np.array(self.state, dtype=np.float32), reward, terminated, truncated, info
    
    def reset(self, seed=None, options=None):
        '''
        reset is run at each beginning of episode.
        '''
        
        assert(self.initialized) 
        
        print("reset ", end="")
        if seed == None: seed = self.seed_value
        super().reset(seed=seed)

        random_state = self.np_random.uniform(low=0.005, high=0.01, size=(2,))
        
        if self.stage == "landing":
            # Landing from altitude z_target, with a null initial velocity:
            self.state = (self.z_target + random_state[0], 0., self.total_mass) 
        elif self.stage == "takeoff":
            # Taking off from the ground , with a null initial velocity:
            self.state = (0., 0., self.total_mass)
        
        self.steps_beyond_terminated = None
        self.prev_state = self.state
        self.prev_action_n = 0
        self.cur_step = 0
        self.cur_time = 0
        self.z_que.clear()
        
        return np.array(self.state, dtype=np.float32), {}

    def render(self, mode='human'): #P as de rendu mais la méthode doit exister
        pass
        return None
        
    def close(self):
        if self.verbose >= 1: print("\nclosing")
        if self.screen is not None:
            import pygame

            pygame.display.quit()
            pygame.quit()
            self.isopen = False
            
if __name__ == "__main__":
    
    from gymnasium.utils.env_checker import check_env
    
    BasicMiniApterrosEnv.metadata = {'render.modes': ['human'],
                                     'video.frames_per_second': 10,
                                     'seed': 1234,
                                     'total_mass': 8,
                                     'tau': 10e-3,
                                     'z_target': 1,
                                     'reward_f': 'reward_takeoff_2',
                                     'max_step': 500,
                                     'static_frict': 10,
                                     'stage': 'takeoff'
                                     }
    
    env = BasicMiniApterrosEnv()

    print(f"env.action_space     :{env.action_space}")
    print(f"env.observation_space:{env.observation_space}")
    
    print("\t type(env.action_space)     :", type(env.action_space))
    print("\t env.action_space           :", env.action_space)
    print("\t env.action_space.high      :", env.action_space.high)
    print("\t env.action_space.low       :", env.action_space.low)

    print("\t type(env.observation_space):", type(env.observation_space))
    print("\t env.observation_space      :", env.observation_space) 
    print("\t env.observation_space.high :", env.observation_space.high)
    print("\t env.observation_space.low  :", env.observation_space.low)

    
    check_env(env, skip_render_check=True)    
    
    env.close()
