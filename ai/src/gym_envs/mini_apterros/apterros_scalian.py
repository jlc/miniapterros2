import math
import gym
from gym import spaces, logger
from gym.utils import seeding
import numpy as np
from ai.src.gym_env.apterros.linear_thrust import compute_thrust


def reward(obj, action, z):
	raise RuntimeError("This method must be defined by yourself")


class BasicApterrosEnv(gym.Env):
    metadata = {'render.modes': ['human', 'rgb_array'],
                'video.frames_per_second': 10 ,
                'tau': 0.10,
                'total_mass': 8,
                'z_target': 1.0,
                'reward_f': reward,
                'noise_norm_z': 0.1,
                'noise_norm_zdot': 0.02,
                'responseTime': -3,
                'stage': 'takeoff',
                "duration":10
                }

    def __init__(self, initProp_input=0):
        self.gravity     = 9.81
        self.min_action  = -1.0  # 'action' comming from DDPG is in range [-1, 1] which is
        self.max_action  =  1.0  # scaled to [0, 1] taking (action+1)/2
        self.total_mass  = None  # kg, to be confirmed
        self.tau         = None  # time step in seconds
        self.z_target    = None  # target altitude in meter
        self.stage       = None  # the stage ('takeoff', 'station', 'landing')
        self.initialized = False

        self.state           = None  # current z and dot_z
        self.stateArray  = []
        self.previous_action = initProp_input
        self.previous_z      = 0
        self.cur_step        = 0
        self.cur_time        = 0       # current physical time, secondes
        self.z_threshold     = 0.05    # 5 cm
        self.noise_norm_z    = 0.1
        self.noise_norm_zdot = 0.02
        self.responseTime    = -3      # 30ms 3 * 10

        high = np.array([self.z_threshold*2, np.finfo(np.float32).max])
        self.action_space = spaces.Box(low=self.min_action,
                                       high=self.max_action,
                                       shape=(1,))
        self.observation_space = spaces.Box(np.array([0, -np.finfo(np.float32).max]),
                                            np.array([np.finfo(np.float32).max, np.finfo(np.float32).max]))
        self.seed()
        self.viewer = None
        self.steps_beyond_done = None

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]
        
    
    def stepPhysics(self, thrust):

        def Cauchy_F(Z, t, T=thrust, m=self.total_mass, g=self.gravity):
            '''Cauchy Function:
               Z is vector [z, z_dot], the Cauchy function is f'(Z,t)
               and returns the vector Z_dot = (z_dot, z_dot_dot).
               In our problem, the 2nd order diff. equation is: z_dot_dot = T/m -g.
            '''
            return np.array([Z[1], T/m-g])       

        t, dt = self.cur_time, self.tau

        # Z is the vector [z, z_dot]:
        Z  = np.array(self.state)
        K1 = Cauchy_F(Z, t)
        K2 = Cauchy_F(Z+K1*dt/2, t)
        K3 = Cauchy_F(Z+K2*dt/2, t)
        K4 = Cauchy_F(Z+K3*dt, t)
        Z += (K1 + 2*K2 + 2*K3 + K4)*dt/6

        if Z[0] < 0:   #Z must be positive
            Z[0] = 0
            Z[1] = - 0.5* Z[1]

        return Z

    def setAttributesFromMetadata(self):
        #self.reward_f    = BasicApterrosEnv.metadata['reward_f']
        self.total_mass  = BasicApterrosEnv.metadata['total_mass']
        self.z_target    = BasicApterrosEnv.metadata['z_target']
        self.tau         = BasicApterrosEnv.metadata['tau']
        self.stage       = BasicApterrosEnv.metadata['stage']
        self.initialized = True

    def step(self, action):
        if self.initialized is False: self.setAttributesFromMetadata()
        # transform 'action' from range [-1,1] to range[0,1]:
        action = (float(action)+1)/2
        # get thrust in Newton from action in range [0,1]:
        thrust = compute_thrust(action)
        # compute self.state = [z, z_dot]
        self.state = self.stepPhysics(thrust)
        z, z_dot = self.state
        self.cur_step  += 1
        self.cur_time  += self.tau
        
        #done ?
        done = False
        if z < 0:
            done = True
        elif z > 1.2*self.z_target:
            done = True
        
        #reward
        if not done:
            reward = self.reward_f(action, z, self.cur_time)
            self.previous_action = action
        elif self.steps_beyond_done is None:
            # Apterros crashed 
            self.steps_beyond_done = 0
            reward = 1
        else:
            if self.steps_beyond_done == 0:
                logger.warn("""
You are calling 'step()' even though this environment has already returned
done = True. You should always call 'reset()' once you receive 'done = True'
Any further steps are undefined behavior.
                """)
            self.steps_beyond_done += 1
            reward = 0.0
            
        self.previous_z = z

        return np.array(self.state), reward, done, {'t':self.cur_time,'z':z,'z_target':self.z_target}

    def reset(self):
        if self.initialized is False: self.setAttributesFromMetadata()
        self.state = self.np_random.uniform(low=0.005, high=0.01, size=(2,))
        if self.stage is None: self.stage = self.metadata['stage']
        if self.stage == "landing":
            self.state[0] += self.z_target # Départ à une altitude de z_target
        """elif self.stage == "takeoff":
            self.state = (0.,0.)"""
        self.steps_beyond_done = None
        self.previous_z = 0
        self.cur_step   = 0
        self.cur_time   = 0
        return np.array(self.state)

    def render(self, mode='human'):
        screen_width = 600
        screen_height = 400

        world_width = self.z_threshold * 2
        scale = screen_width /world_width
        carty = 100  # TOP OF CART
        apterros_width = 10.0
        Apterros_height = scale * 1.0
        cartwidth = 50.0
        cartheight = 30.0

        if self.viewer is None:
            from gym.envs.classic_control import rendering
            self.viewer = rendering.Viewer(screen_width, screen_height)
            l, r, t, b = -cartwidth / 2, cartwidth / 2, cartheight / 2, -cartheight / 2
            axleoffset = cartheight / 4.0
            cart = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            self.carttrans = rendering.Transform()
            cart.add_attr(self.carttrans)
            self.viewer.add_geom(cart)
            l, r, t, b = -apterros_width / 2, apterros_width / 2, Apterros_height-apterros_width / 2, -apterros_width / 2
            pole = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
            pole.set_color(.8, .6, .4)
            self.poletrans = rendering.Transform(translation=(0, axleoffset))
            pole.add_attr(self.poletrans)
            pole.add_attr(self.carttrans)
            self.viewer.add_geom(pole)
            self.axle = rendering.make_circle(apterros_width / 2)
            self.axle.add_attr(self.poletrans)
            self.axle.add_attr(self.carttrans)
            self.axle.set_color(.5, .5, .8)
            self.viewer.add_geom(self.axle)
            self.track = rendering.Line((0, carty), (screen_width, carty))
            self.track.set_color(0, 0, 0)
            self.viewer.add_geom(self.track)

        if self.state is None:
            return None

        x = self.state
        cartx = x[0] * scale + screen_width / 2.0  # MIDDLE OF CART
        self.carttrans.set_translation(cartx, carty)
        # self.poletrans.set_rotation(-x[1])

        return self.viewer.render(return_rgb_array=(mode == 'rgb_array'))

    def close(self):
        if self.viewer:
            self.viewer.close()

    def reward_f(self, action, z, t):
        t_station = 10  # stationnary pahe after 4 secondes...
        if self.stage is None: self.stage = self.metadata['stage']
        R = 1
        if self.stage == "takeoff":
            if abs(action - self.previous_action) <= 0.3: R += 1
            if t <= t_station:
                if 0.001 < z - self.previous_z <= 0.02:
                    R += 2
                else:
                    R -= 1
            else:
                if 0.9 * self.z_target <= z <= 1.1 * self.z_target:
                    R += 2
                else:
                    R -= 1
        return R / 4