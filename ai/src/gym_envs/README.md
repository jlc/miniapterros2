# Content

This directory contains Python programs to define some Gymnasium Environments:

- file `mini_apterros/mini_apterros.py`: <br>
  The `BasicApterrosEnv` class derived from the `Env` Gym class: gives a very simplified 1D model of the vehicule.
- file `continuous_cartpole/continuous_cartpole.py`:<br>
  The `ContinuousCartPoleEnv` class derived from the `Env` Gym class: a simplified model of the continuous CartPole.
