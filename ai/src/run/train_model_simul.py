#
# To train a neural network to control a vehicule (ContinuousCartPoleEnv, BalancingRobotEnv_PyBullet,
# BasicMiniApterrosEnv...) 
# within a given simulator (gym, pybullet...) for dynamic simulations and rendering.
#
# After the training computation, the program prints the elapsed training time 
# and the path of the saved model (useful for the test stage). 
#
# Run this programm interactively in the project root directory, from the Python interpretor, so that 
# the "main bloc" at the end is run:
### python ai/src/run/train_model_gym.py -h
### usage: train_model_gym.py [-h] --config CONFIG

import time, os, shutil, yaml
from pathlib import Path
from ai.src.run.constants import EXPERIMENT_CONFIG_FILENAME, ENVIRONMENT_CONFIG_FILENAME

import gymnasium as gym
from stable_baselines3.common.callbacks import CheckpointCallback # to save a checkpoint every N steps

from balancing_robot.pybullet.BalancingRobotEnv_Gym import BalancingRobotEnv_PyBullet
from ai.src.gym_envs.mini_apterros.mini_apterros import BasicMiniApterrosEnv
from apterros.pybullet.MiniAPTERROS_GymEnv_antiroll import MiniAPTERROS_AntiRoll


def train_model(cfg_file:str, training_dir:str=None, headless:bool=True):
    """
    Train a PPO network with DRL to control a given environment as described in cfg_file,
    and save the results and config data for reproductibility

    Parameters:
      cfg_file    : Path of the yaml configuration file for the agent and the environment.
      training_dir: If None, a training directory will be created, else the parameter is used
                    as the training directory location.
    
    Return: None
    """
    # Reads parameters from the config file:
    with open(cfg_file, 'r') as f:
        cfg = yaml.safe_load(f.read())

    # Required parameters:
    try:
        environment  = cfg['env']['environment']
        cfg_file_env = cfg['env']['cfg_env']

        out_path     = cfg['train']['output_model_path']
        agent_type   = cfg['train']['agent_type']
        policy       = cfg['train']['policy']
        tot_steps    = cfg['train']['total_timesteps']
        save_freq    = cfg['train']['save_freq']
        nb_steps     = cfg['train']['n_steps']
        b_size       = cfg['train']['batch_size']
        nb_epochs    = cfg['train']['n_epochs']
        seed         = cfg['train']['seed']
    except:
        raise RuntimeError(f"Parameters missing in file <{cfg_file}>")
    
    # Read the cfg_file_env file:
    with open(cfg_file_env, 'r') as f:
        cfg_env = yaml.safe_load(f.read())

    # import agent
    if agent_type == 'PPO':
        from stable_baselines3 import PPO as agent
    else:
        raise Exception("Agent <{agent_type}> not implemented")

    # Define the training dir if needed:
    if training_dir is None:
        experiment_time = time.localtime()
        # prepare directory for output
        experiment_id = "_".join([environment, agent_type,
                                 time.strftime("%y-%m-%d_%H-%M-%S", experiment_time)])
        training_dir = Path(out_path) / experiment_id
    
    training_dir = Path(training_dir)
    training_dir.mkdir(parents=True, exist_ok=True)

    # Create env for training
    print(f"found <{environment}>")
    match environment:
        
        case 'BalancingRobotEnv_PyBullet':
            
            urdf_file = cfg['env']['urdf']
            headless  = cfg['train']['headless'] 
            
            version   = cfg_env['version']
            veloc     = cfg_env['velocity']
            dt        = cfg_env['dt']
            theta_lim = cfg_env['theta_lim']
            x_lim     = cfg_env['x_lim']   
            reward    = cfg_env['reward']   
            diameter  = cfg_env['wheel_diam']   
                    
            theta0_deg = None
            try:
                theta0_deg = cfg_env['theta0_deg'] 
            except: pass
            finally: print(f"found parameter 'theta0_deg':{theta0_deg}") 

            env = BalancingRobotEnv_PyBullet(version, urdf_file, diameter, dt,
                                             theta_lim=theta_lim,
                                             x_lim=x_lim,
                                             theta0_deg=theta0_deg,                                 
                                             reward=reward,
                                             veloc_mag=veloc, 
                                             headless=headless, 
                                             verbose=0)

            shutil.copyfile('balancing_robot/python/rewards.py', training_dir/'rewards.py')

        case 'BasicMiniApterrosEnv':
            
            param_list = ('total_mass', 'tau', 'z_target', 'stage', 'static_frict', 'max_step', 'reward_f')
            for param in param_list:
                BasicMiniApterrosEnv.metadata[param] = cfg_env[param]   
            BasicMiniApterrosEnv.metadata['seed']    = seed
            
            env = gym.make('MiniApterros-v0')
    
            print(env.metadata.keys())
            print(env.metadata.values())
            shutil.copyfile('ai/src/gym_envs/mini_apterros/rewards.py', training_dir/'rewards.py')
            shutil.copyfile('ai/src/gym_envs/mini_apterros/mini_apterros.py', training_dir/'mini_apterros.py')
        
        case 'ContinuousCartPoleEnv': 
                    
            env = gym.make("CartPole-v2")
            
            param_list = ('cart_mass', 'pole_mass', 'pole_length', 'max_step', 'tau', 'max_step')
            for param in param_list:
                env.metadata[param]   = cfg_env[param]
        
        case 'MiniApterros_AntiRoll':
            
            param_list = ('version', 'urdf', 'total_mass',
                          'roll_target_deg', 'roll_err_epsi_deg',
                          'roll_torque_Nm', 'torque_percent', 'z_target_m',
                          'max_step', 'tau', 'reward_f' )
            
            for param in param_list:
                MiniAPTERROS_AntiRoll.metadata[param]   = cfg_env[param]
            MiniAPTERROS_AntiRoll.metadata['seed']    = seed
            MiniAPTERROS_AntiRoll.metadata['forced_Zveloc'] = 0.25
             
            env   = MiniAPTERROS_AntiRoll(headless=True, CW_turbine_id=(12,13), CCW_turbine_id=(15,16))
            
            print(env.metadata.keys())
            print(env.metadata.values())
            shutil.copyfile('apterros/python/rewards.py', training_dir/'rewards.py')
            shutil.copyfile('apterros/pybullet/MiniAPTERROS_GymEnv_antiroll.py', training_dir/'MiniAPTERROS_GymEnv_antiroll.py')
            
        case _:
            raise Exception("Not implemented environment: <{environment}>")

    print(f"Using the environnement <{environment}>")
    print("\t type(env.action_space)     :", type(env.action_space))
    print("\t env.action_space           :", env.action_space)
    print("\t env.action_space.high      :", env.action_space.high)
    print("\t env.action_space.low       :", env.action_space.low)

    print("\t type(env.observation_space):", type(env.observation_space))
    print("\t env.observation_space      :", env.observation_space) 
    print("\t env.observation_space.high :", env.observation_space.high)
    print("\t env.observation_space.low  :", env.observation_space.low)

    # Copy precious files in experiment_dir:
    shutil.copyfile(cfg_file, training_dir/EXPERIMENT_CONFIG_FILENAME)
    shutil.copyfile(cfg_file_env, training_dir/ENVIRONMENT_CONFIG_FILENAME)
    
    # Prepare agent for training:
    model = agent(policy, 
                  env, 
                  n_epochs=nb_epochs,
                  n_steps=nb_steps,
                  batch_size=b_size,
                  seed=seed,
                  tensorboard_log=training_dir,
                  verbose=1)

    checkpoint_callback = CheckpointCallback(save_freq=save_freq, save_path=training_dir/'ZIP')

    #
    # train the agent:
    #
    t0 = time.time()

    model.learn(total_timesteps=tot_steps, callback=checkpoint_callback)
    t = int(time.time()-t0)
    h = int(t//3600)
    m = int((t - h*3600)//60)
    print(f"Training elapsed time: {h:02d}h {m:02d}m")
  
    # save trained agent
    target_zip = training_dir/'ZIP'/'model.zip'
    print(f"saving trained model in <{target_zip}>")
    model.save(target_zip)
    env.close()

    os.system(f"cd {out_path} && ln -s -f {os.path.basename(training_dir)} last")

    return training_dir
    

if __name__ == "__main__":

    # Main bloc

    import argparse, sys
    parser = argparse.ArgumentParser()
    
    parser.add_argument('--configfile', action="store", dest='config_file', 
                         help="relative path name of the config file '..._ppo_...yaml'")

    parser.add_argument('--traindir', action="store", dest='traindir', 
                         help="Optional, the relative pathname of the training directory")
    
    args = parser.parse_args()
    config_path = args.config_file
    traindir = None if not args.traindir else args.traindir

    print(f"running: train_model('{config_path}', training_dir='{traindir}')")

    train_model(config_path, training_dir=traindir)
