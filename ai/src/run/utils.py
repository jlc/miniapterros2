import numpy as np
import matplotlib.pyplot as plt

def plot_action_reward(perf:dict, real_dir, test_dir, display_plots:bool):
    '''
    To plot the actions end the reward evaluated for a trained PPO driving an environment
    '''
    
    actions  = perf["actions"]
    rewards  = perf['rewards']
    nb_steps = perf['last_step_count']
    
    plt.figure(str(real_dir), figsize=(10,6))
    plt.subplots_adjust(hspace=.4)
    plt.subplot(2,1,1)
    plt.suptitle(f"Perf from <{real_dir.name}>")
    plt.title("Reward")
    plt.plot(range(nb_steps), rewards, 'b', linewidth=0.4)
    plt.ylabel("reward", color='b')
    #plt.ylim(0,2.)
    plt.grid()
    plt.twinx()
    
    cum, reward_cum = 0, []
    for r in rewards:
        cum = cum + r
        reward_cum.append(cum)
    plt.plot(range(nb_steps), reward_cum, 'm')
    plt.ylabel("cumulated reward", color='m')
    #plt.ylim(0, perf['last_step_count'])
    
    plt.subplot(2,1,2)
    plt.title("Actions")
    plt.plot(range(nb_steps), actions, 'g', marker='.', markersize=3, linewidth=0.4)
    plt.xlabel("steps")
    plt.ylabel("network action in [-1.;1.]")
    plt.ylim(-1, 1)
    plt.grid()

    plt.savefig(test_dir/"rewards.png")

    if display_plots: plt.show()
    plt.close()
    
def plot_data_miniapterros(title, perf, test_dir):        
    '''
    To plot the data reccorded when testing a PPO to drive the miniAPTERROS
    '''
    time   = perf['value_to_plot']['Time']
    PWM    = perf['value_to_plot']['PWM']
    reward = perf['rewards']
    z      = perf['value_to_plot']['altitude']
    z_dot  = perf['value_to_plot']['altitude_dot']
        
    plt.figure("PPO performances",[18,6])
    plt.suptitle(title)
    plt.subplots_adjust(hspace=0.4)
    plt.subplot(2,2,1)
    plt.plot(time, PWM, '.-' )
    plt.title("PWM(t)")
    plt.ylim(0,100)
    plt.xlabel("time [s]")
    plt.ylabel("percent")
    plt.grid(True)

    plt.subplot(2,2,2)
    plt.plot(time, reward, '.-' )
    plt.title("Reward(t)")
    plt.xlabel("time [s]")
    plt.ylabel("reward")
    plt.ylim(-1,1)
    plt.grid(True)

    plt.subplot(2,2,3)
    plt.plot(time, z, '.-' )
    plt.title("z(t)")

    plt.xlabel("time [s]")
    plt.ylabel("distance to ground [m]")
    plt.ylim(0,1.3)
    plt.grid(True)

    plt.subplot(2,2,4)
    plt.plot(time, z_dot, '.-' )
    plt.title("z_dot(t)")
    plt.xlabel("time [s]")
    plt.ylim(-1,1)
    plt.ylabel("verticale velocity [m/s]")
    plt.grid(True)

    plt.savefig(test_dir / "eval.png")
    plt.show()


def plot_data_miniapterros_antiroll(title, perf, test_dir):        
    '''
    To plot the data reccorded when testing a PPO to drive the miniAPTERROS anti-roll
    '''
    time        = perf['value_to_plot']['time']
    roll        = perf['value_to_plot']['roll']
    dot_roll    = perf['value_to_plot']['dot_roll']
    action      = perf['value_to_plot']['action']
    pwm         = perf['value_to_plot']['pwm']
    
    # process roll to be [0, infinity] and not [-pi, pi]:
    roll = np.degrees(np.array(roll))
    r, offset = [], 0
    for r1, r2 in zip(roll[:-1], roll[1:]):
        if offset == 0 and abs(r1 - r2) > 150:
            offset = 360
        r.append(r2 + offset)

    title, params = title.split('\n')        
    fig, axes = plt.subplots(4, 1, figsize=(10,8), sharex=True, gridspec_kw={'height_ratios': [1, 1, 1, 2]})
    fig.suptitle(title, size=13)
    fig.text(0.5, .93, params, size=10, color="gray", horizontalalignment='center')
    
    ax = axes[0]
    ax.set_title("Mini-APTERROS roll")
    ax.plot(time[:len(r)], r, '-b', label='dot angle')
    ax.set_ylabel("roll [°]")
    ax.legend()
    ax.grid(which='major', color='xkcd:cool grey',  linestyle='-',  alpha=0.7)
    ax.grid(which='minor', color='xkcd:light grey', linestyle='--', alpha=0.5)

    ax = axes[1]
    ax.set_title("Mini-APTERROS roll speed")
    ax.plot(time, np.degrees(dot_roll), '-m', label='dot_speed')
    ax.set_ylabel("Roll speed [°/s]")
    ax.legend()
    ax.grid(which='major', color='xkcd:cool grey',  linestyle='-',  alpha=0.7)
    ax.grid(which='minor', color='xkcd:light grey', linestyle='--', alpha=0.5)

    ax = axes[2]
    ax.set_title("Action given by the Neural Network")
    ax.plot(time, action, '-g', linewidth=0.5, label='action')
    ax.set_ylabel("Arbitrary unit")
    ax.legend()
    ax.grid(which='major', color='xkcd:cool grey',  linestyle='-',  alpha=0.7)
    ax.grid(which='minor', color='xkcd:light grey', linestyle='--', alpha=0.5)
    
    # process action to get CW and CCW PWM:   
    action = np.array(action)
    pwm = np.array(pwm)
    CW_pwm  = (action > 0)*pwm
    CCW_pwm = (action < 0)*pwm
    ax = axes[3]
    ax.set_title("Average PWM given by the Neural Network")
    ax.plot(time, CW_pwm, 'o-m', markersize=1., linewidth=0.5, label='CW_pwm (mov. av.)')
    ax.plot(time, CCW_pwm, 'o-g', markersize=1., linewidth=0.5, label='CCW_pwm (mov. av.)')
    ax.set_ylabel("percent")
    ax.set_xlabel('Time [s]')
    ax.legend()
    ax.grid(which='major', color='xkcd:cool grey',  linestyle='-',  alpha=0.7)
    ax.grid(which='minor', color='xkcd:light grey', linestyle='--', alpha=0.5)

    plt.subplots_adjust(top=.88, hspace=0.30)
    plt.savefig(test_dir / "eval.png")
    plt.show()
