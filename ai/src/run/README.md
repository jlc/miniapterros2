# Content

This directory contains Python programs aim to train/test PPO networks to drive various environments (CartPole, BalancingRobot, MiniApterros...).

- `train_model_gym.py` : used to train a neural network to control a vehicule (ContinuousCartPoleEnv, BasicMiniApterrosEnv...) within the native gym simulator (and rendering).<br>
After computation, the program prints the elapsed training time and the path of the saved model (useful for the test stage). 

```bash
python ai/src/run/train_model_gym.py -h
usage: train_model_gym.py [-h] --config CONFIG

options:
  -h, --help       show this help message and exit
  --config CONFIG  relative path name of the config file <..._ppo_....yaml>
```

- `train_model_simul.py` : used to train a neural network to drive a vehicule (cartpole, balancingrobot...) within a simulator given with the `--simulator` option (gym, copsim, pybullet...)

```bash
python ai/src/run/train_model_simul.py -h
usage: train_model_simul.py [-h] [--configfile CONFIG_FILE] [--traindir TRAINDIR]

options:
  -h, --help            show this help message and exit
  --configfile CONFIG_FILE
                        relative path name of the config file '..._ppo_...yaml'
  --traindir TRAINDIR   Optional, the relative pathname of the training directory
```
Examples:

__to train meural networks:__

- train a PPO network with the MiniAPTERROS vehicule simulated under gym, using the configutaion file `apterros_ppo_gym.yaml` :

```Bash
python ai/src/run/train_model_gym.py --config ai/config/apterros_ppo_gym.yaml 
```

__to test trained neural networks__

To train a neural network to drive a vehicule (cartpole, balancingrobot...) within a simulator given with 
the --simulator option (gym, pybullet...)

Run this programm interactively in the project root directory, from the Python interpretor, so that 
the "main bloc" at the end is run:

```bash
python ai/src/run/test_model_gym.py -h

usage: test_model_gym.py [-h] --configdir CONFIG_DIR [--step STEP_SAVED] [--quiet] [--noisy] [--forcetest]

options:
  -h, --help            show this help message and exit
  --configdir CONFIG_DIR
                        directory (relative path) where the config file <..._ppo_....yaml> lives
  --step STEP_SAVED     step of the saved NN to test
  --quiet               wether to print informations or not
  --noisy               wether to test with deterministic False or True
  --forcetest           wether to force test computation or not
```