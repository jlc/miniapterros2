#
# To train a neural network to control a vehicule (ContinuousCartPoleEnv, BasicMiniApterrosEnv...) 
# within the gym workbench for simulations and rendering.
#
# After the training computation, the program prints the elapsed training time 
# and the path of the saved model (useful for the test stage). 
#
# Run this programm interactively in the project root directory, from the Python interpretor, so that 
# the "main bloc" at the end is run:
### python ai/src/run/train_model_gym.py -h
### usage: train_model_gym.py [-h] --config CONFIG

import time, os, shutil, yaml
from pathlib import Path
from ai.src.run.constants import EXPERIMENT_CONFIG_FILENAME, ENVIRONMENT_CONFIG_FILENAME

import gymnasium as gym
from stable_baselines3.common.callbacks import CheckpointCallback # to save a checkpoint every N steps
from ai.src.gym_envs.mini_apterros.mini_apterros import BasicMiniApterrosEnv

def train_model(cfg_file: str):
    """
    Train a PPO network with DRL to control a given environment as described in cfg_file,
    and save the results and config data for reproductibility

    Parameters:
      cfg_file : Path of the yaml configuration file for the agent and the environment.
    
    Return: None
    """

    # Reads parameters from config file:
    with open(cfg_file, 'r') as f:
        cfg = yaml.safe_load(f.read())

    # Required parameters:
    try:
        environment  = cfg['env']['environment']
        cfg_file_env = cfg['env']['cfg_env']

        out_path     = cfg['train']['output_model_path']
        agent_type   = cfg['train']['agent_type']
        policy       = cfg['train']['policy']
        tot_steps    = cfg['train']['total_timesteps']
        save_freq    = cfg['train']['save_freq']
        nb_steps     = cfg['train']['n_steps']
        b_size       = cfg['train']['batch_size']
        nb_epochs    = cfg['train']['n_epochs']
        seed         = cfg['train']['seed']
    except:
        raise RuntimeError(f"Parameters missing in file <{cfg_file}>")
    
    # Reads the cfg_file_env file:
    with open(cfg_file_env, 'r') as f:
        cfg_env = yaml.safe_load(f.read())

    # import agent
    if agent_type == 'PPO':
        from stable_baselines3 import PPO as agent
    else:
        raise Exception("Agent <{agent_type}> not implemented")

    # Define the training directory
    experiment_time = time.localtime()
    experiment_id = "_".join([environment, agent_type,
                              time.strftime("%y-%m-%d_%H-%M-%S", experiment_time)])
    training_dir = Path(out_path) / experiment_id
    training_dir.mkdir(parents=True, exist_ok=True)

    # Create env for training    
    if environment == 'BasicMiniApterrosEnv':


        param_list = ('total_mass', 'tau', 'z_target', 'stage', 'static_frict', 'max_step', 'reward_f')
        for param in param_list:
            BasicMiniApterrosEnv.metadata[param] = cfg_env[param] 
                      
        BasicMiniApterrosEnv.metadata['seed']    = seed

        env = gym.make('MiniApterros-v0')
        shutil.copyfile('ai/src/gym_envs/mini_apterros/rewards.py', training_dir/'rewards.py')

    elif environment == 'ContinuousCartPoleEnv': 
        
        env = gym.make("CartPole-v2")
        
        param_list = ('cart_mass', 'pole_mass', 'pole_length', 'max_step', 'tau', 'max_step')
        for param in param_list:
            env.metadata[param]   = cfg_env[param]
                                  
        nb_steps   = cfg['train']['n_steps']
        tot_steps  = cfg['train']['total_timesteps']
        save_freq  = cfg['train']['save_freq']
        
    else:
        raise Exception("Not implemented environment :"+cfg['env']['environment'])

    print(f"Using the environnement <{environment}>")
    print("\t type(env.action_space)     :", type(env.action_space))
    print("\t env.action_space           :", env.action_space)
    print("\t env.action_space.high      :", env.action_space.high)
    print("\t env.action_space.low       :", env.action_space.low)

    print("\t type(env.observation_space):", type(env.observation_space))
    print("\t env.observation_space      :", env.observation_space) 
    print("\t env.observation_space.high :", env.observation_space.high)
    print("\t env.observation_space.low  :", env.observation_space.low)

    # Copy precious files in experiment_dir:
    shutil.copyfile(cfg_file, training_dir/EXPERIMENT_CONFIG_FILENAME)
    shutil.copyfile(cfg_file_env, training_dir/ENVIRONMENT_CONFIG_FILENAME)
    
    # Prepare agent for training:
    model = agent(policy, 
                  env, 
                  n_epochs=nb_epochs,
                  n_steps=nb_steps,
                  batch_size=b_size,
                  seed=seed,
                  tensorboard_log=training_dir,
                  verbose=1)

    checkpoint_callback = CheckpointCallback(save_freq=save_freq, save_path=training_dir/'ZIP')

    #
    # train the agent:
    #
    
    t0 = time.time()

    model.learn(total_timesteps=tot_steps, callback=checkpoint_callback)

    t = int(time.time()-t0)
    h = int(t//3600)
    m = int((t - h*3600)//60)
    print(f"Training elapsed time: {h:02d}h {m:02d}m")
  
    # save trained agent
    target_zip = training_dir/'ZIP'/'model.zip'
    print(f"saving trained model in <{target_zip}>")
    model.save(target_zip)
    env.close()

    #os.system(f"cd {out_path} && ln -s -f {training_dir.name} last")

    return training_dir

if __name__ == "__main__":
    
    # Main bloc
    import argparse, sys

    parser = argparse.ArgumentParser()
    parser.add_argument('--config', action="store", dest='config', required=True,
                         help="relative path name of the config file <..._ppo_....yaml>")

    args = parser.parse_args()
    config_path = args.config

    print(f"Running: train_model({config_path})")
    train_model(config_path)
