#
# To test the perfomances of a PPO neural network trained with the script 'train_model_gym.py'.
#
# In this file we define two functions:
#   - test_model    : to run an experiment described in config files, save the results and config files.
#   - evaluate_model: to display and save how a model performs to drive a given environment.
#
# Run this programm interactively in the project root directory, from the Python interpretor, so that 
# the "main bloc" at the end is run:
#
### python ai/src/run/test_model_gym.py -h
### usage: test_model_gym.py [-h] --configdir CONFIG_DIR [--stepsaved STEP_SAVED] [--quiet] [--noisy] [--alwaystest]
### options:
### -h, --help show this help message and exit
### --configdir CONFIG_DIR      directory (relative path) where the config file <..._ppo_....yaml> lives
### --stepsaved STEP_SAVED      step of the saved NN to test
### --quiet                     wether to print informations or not
### --noisy                     wether to test swith deterministic False or True
### --alwaystest                wether to force test computation or not

import numpy as np
import os, sys, time, yaml, pathlib
from os.path import realpath

import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

import gymnasium as gym
from gymnasium.wrappers import RecordVideo
from stable_baselines3.common.base_class import BaseAlgorithm
from ai.src.run.constants import TEST_DIR, EXPERIMENT_CONFIG_FILENAME, ENVIRONMENT_CONFIG_FILENAME
from ai.src.run.constants import PERFORMANCE_REPORT_FILENAME

from ai.src.gym_envs.mini_apterros.mini_apterros import BasicMiniApterrosEnv

def evaluate_model(model: BaseAlgorithm, 
                   env: gym.Env, 
                   out_dir:str=None,
                   deterministic: bool = True, 
                   steps_nb: int = 1000,
                   verbose=False) -> dict:
    
    """
    Computes how a model performs on a given environment.

    Parameters:
      model:    Instance of a stable_baselines3 model (neural network).
      env:      Instance of a gym environment which is operated by the model.
      out_dir:  The directory path where to write the ouput of gym Monitor.
      deterministic: Wether a random noise is applied to the model during evaluation or not.
      steps_nb: # of simulation steps for evaluation.
      verbose:  Wether to print some debug informations or not.
      
    returns: 
      A dict of {metrics_name: metrics_values}
    """
    
    # initialize some objects before the loop:
    env._max_episode_steps = steps_nb
    obs,  info = env.reset()
    terminated,  step_count, rewards, actions = False, 0, [], []
    env_name = env.unwrapped.spec.id
    plot_value = None
    
    if verbose: print(f"total_timesteps: {steps_nb}")
    
    if env_name == "MiniApterros-v0":
        list_PWM, list_z, list_z_dot, list_t = [], [], [], [] # for plot after the testing
    elif env_name == "Cartpole-v2":
        pass
    
    # at each step, will make the agent predict the next action and update the environment consequently
    while step_count < steps_nb:
        action, _states = model.predict(obs, deterministic=deterministic)
        obs, reward, terminated, truncated, info = env.step(action)
        rewards.append(reward)
        actions.append(action)
        step_count += 1
        
        if verbose and (step_count % 10 == 0) : print("\r", step_count, end="")
        
        if env_name == "MiniApterros-v0":
            t, action, z = info['t'], info['action'], info['z']
            z_dot = obs[1]
            PWM = action*100
            print(f"t: {t:4.2f}, a: {action:6.3f}, PWM: {PWM:04.1f}, z: {z:5.3f}", flush=True)
            list_PWM.append(PWM) 
            list_z.append(z)
            list_z_dot.append(z_dot)
            list_t.append(t)
        #if verbose :
        #    print(f"{step_count:6d}     {action}    {terminated}")
        #if terminated: break

    
    if env_name == "MiniApterros-v0":
        plot_value = {"PWM" : list_PWM,
                      "altitude": list_z,
                      "altitude_dot": list_z_dot,
                      "Time": list_t}
    else:
        plot_value = {}
                    
    return {"actions": actions,
            "rewards" : rewards,
            "rewards_mean": float(np.mean(rewards)),
            "rewards_std": float(np.std(rewards)),
            "last_step_count": step_count,
            "percent_completion": step_count/steps_nb,
            "value_to_plot" : plot_value}


def test_model(cfg_loc: str,
               step_saved=0,
               force_test=False, 
               noisy=None,
               verbose=True):
    """
    Run an experiment described in cfg_file, and save the results and config for reproducibility

    Parameters:
      cfg_loc:      Path of the directory that contains the yaml config files.
      step_saved:   step of the saved NN to test
      force_test:   If True, the test will be terminated even if the test directory already exists,
                    else the test is skipped.
      noisy:        wether to make a deterministic or noisy evaluation
      verbose:      Wether to print some debug informations or not.

    Return: None
    """

    print(f"Running test_model with cfg_loc: <{cfg_loc}>")

    # The config files must be searched in the directory of the 'cfg_loc'
    assert os.path.isdir(cfg_loc)
    real_dir  = realpath(cfg_loc)
    cfg_file = os.path.join(real_dir, EXPERIMENT_CONFIG_FILENAME)

    with open(cfg_file, 'r') as f:
        cfg = yaml.safe_load(f.read())
    
    try:
        environment     = cfg['env']['environment']
        total_timesteps = cfg['eval']['total_timesteps']
        deterministic   = cfg['eval']['deterministic']
        model_name      = cfg['eval']['model_name']

        agent_type      = cfg['train']['agent_type']
        seed            = cfg['train']['seed']

    except:
        raise RuntimeError(f"Parametres missing in file <{cfg_file}>")
        
    # Build the absolute pathname of the file 'model.zip':
    if step_saved == 0:
        model_name = os.path.join('ZIP', model_name)
    else:
        model_name = os.path.join(f'ZIP/rl_model_{step_saved}_steps.zip')
        
    full_model_name = os.path.join(real_dir, model_name)
        
    if verbose: print(f"Looking for model <{model_name}>... ", end="")
    
    if not os.path.exists(full_model_name):
        print(" not found: ", end="")
        zip_dir = os.path.join(real_dir, 'ZIP')
        # the training stopped before the the last epoch: no link 'model.zip'
        zip_file_list = [(f, os.path.getmtime(os.path.join(zip_dir,f))) for f in os.listdir(zip_dir)]
        zip_file_list.sort(key=lambda elem: elem[1])
        # we take the last zip file for the trained model:
        model_name = os.path.join('ZIP', zip_file_list[-1][0])
        full_model_name = os.path.join(real_dir, model_name)
        print(f"using <{model_name}>")
    else:
        print(" found.")

    # load the saved environment config file:
    if verbose: print(f"env. parameters loaded from <{os.path.basename(real_dir)}/{ENVIRONMENT_CONFIG_FILENAME }>")
    cfg_file_env = real_dir/ENVIRONMENT_CONFIG_FILENAME         # cfg['env']['cfg_env']
    with open(cfg_file_env, 'r') as f:
        cfg_env = yaml.safe_load(f.read())

    # create the test directory if needed:
    test_dir = real_dir/TEST_DIR
    if step_saved:
        test_dir = test_dir/f"rl_model_{step_saved}_steps"

    if force_test or not test_dir.exists() or (test_dir.exists() and not os.listdir(test_dir)): 
        # import agent
        if  agent_type == 'PPO':
            from stable_baselines3 import PPO as agent
        else:
            raise Exception("Not implemented agent : <{agent_type}>")

        # Create env for evaluation
        # WARNING : the syntaxe gym.make('CartPole-v2') or gym.make('MiniApterros-v0') requires the 
        # modification of the directory /.../miniconda3/envs/pyml/lib/python3.x/site-packages/gym/ 
        # according to ai/docs/update_gym_env.pdf.
        
        if environment in ('CartPole-v2', 'ContinuousCartPoleEnv'):
            
            eval_env = gym.make('CartPole-v2', render_mode="rgb_array")
            # wrapper to monitor and save in a movie what is happening
            eval_env = RecordVideo(eval_env, test_dir)
            
            param_list = ('cart_mass', 
                      'pole_mass',
                      'pole_length',
                      'max_step',
                      'tau',
                      'max_step')
            for param in param_list:
                eval_env.metadata[param]   = cfg_env[param]

        elif environment == 'BasicMiniApterrosEnv':

            param_list = ('total_mass', 'tau', 'z_target', 'stage', 'static_frict', 'max_step', 'reward_f')
            for param in param_list:
                BasicMiniApterrosEnv.metadata[param] = cfg_env[param]   
            BasicMiniApterrosEnv.metadata['seed']    = seed

            eval_env = gym.make('MiniApterros-v0')
        
        else:
            raise Exception("Not implemented environment: <{environment}>")
    
        model = agent.load(full_model_name)

        if not test_dir.exists(): test_dir.mkdir(parents=True)

        # Evaluate the trained agent    
        perf = evaluate_model(model, 
                              eval_env,
                              out_dir=test_dir,
                              steps_nb=total_timesteps, 
                              deterministic=deterministic,
                              verbose=verbose)        
        perf["deterministic"] = deterministic
        
        with open(test_dir/PERFORMANCE_REPORT_FILENAME, 'w') as outfile:
            yaml.dump(perf, outfile, default_flow_style=True)

        eval_env.close()

    else:
        if verbose: print(f"TEST directory already exists: reading file <{os.path.basename(real_dir)}/TEST/{PERFORMANCE_REPORT_FILENAME}>")
        with open(test_dir/PERFORMANCE_REPORT_FILENAME, 'r') as f:
            perf = yaml.unsafe_load(f.read())

    if environment == 'BasicMiniApterrosEnv':
        
        plt.figure("Performances du  RN",[18,6])
        title = f"Training from {os.path.basename(cfg_loc)}/ZIP/{os.path.basename(realpath(full_model_name))}"
        title += f" [deterministic={deterministic}]"
        plt.suptitle(title)
        plt.subplots_adjust(hspace=0.4)
        plt.subplot(2,2,1)
        plt.plot(perf['value_to_plot']['Time'], perf['value_to_plot']['PWM'], '.-' )
        plt.title("PWM(t)")
        plt.ylim(0,100)
        plt.xlabel("time [s]")
        plt.ylabel("percent")
        plt.grid(True)

        plt.subplot(2,2,2)
        plt.plot(perf['value_to_plot']['Time'], perf['rewards'], '.-' )
        plt.title("Reward(t)")
        plt.xlabel("time [s]")
        plt.ylabel("reward")
        plt.ylim(-1,1)
        plt.grid(True)

        plt.subplot(2,2,3)
        plt.plot(perf['value_to_plot']['Time'], perf['value_to_plot']['altitude'], '.-' )
        plt.title("z(t)")

        plt.xlabel("time [s]")
        plt.ylabel("distance to ground [m]")
        plt.ylim(0,1.3)
        plt.grid(True)

        plt.subplot(2,2,4)
        plt.plot(perf['value_to_plot']['Time'], perf['value_to_plot']['altitude_dot'], '.-' )
        plt.title("z_dot(t)")
        plt.xlabel("time [s]")
        plt.ylim(-1,1)
        plt.ylabel("verticale velocity [m/s]")
        plt.grid(True)

        plt.savefig(os.path.join(test_dir, "eval.png"))
        plt.show()


if __name__ == "__main__":
    # main bloc

    import argparse, sys

    parser = argparse.ArgumentParser()
    parser.add_argument('--configdir', action="store", dest='config_dir', required=True,
                         help="directory (relative path) where the config file <..._ppo_....yaml> lives")
    
    parser.add_argument('--step', action="store", dest='step_saved', type=int, default=0,
                         help="step of the saved NN to test")

    parser.add_argument('--quiet', action="store_true", dest='quiet', default=False,
                         help="wether to print informations or not")        
    
    parser.add_argument('--noisy', action="store_true", dest='noisy', default=False,
                         help="wether to test with deterministic False or True")              
    
    parser.add_argument('--forcetest', action="store_true", dest='forcetest', default=False, 
                         help="wether to force test computation or not ")                         

    args = parser.parse_args()
    config_path = args.config_dir
    verbose = not args.quiet
    force_test = args.forcetest
    step_saved = args.step_saved
    noisy = args.noisy
    
    print(f"Running: test_model({config_path}, step_saved={step_saved}, force_test={force_test}, noisy={noisy}, verbose={verbose}")

    test_model(config_path, step_saved=step_saved, force_test=force_test, noisy=noisy, verbose=verbose)
