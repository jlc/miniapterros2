#
# To test the perfomances of a PPO neural network trained with the script 'train_model_simul.py'.
#
# In this file we define two functions:
#   - test_model    : to run an experiment described in config files, save the results and config files.
#   - evaluate_model: to display and save how a model performs to drive a given environment.
#
# Run this programm interactively in the project root directory, from the Python interpretor, so that 
# the "main bloc" at the end is run:
#
### python ai/src/run/test_model_simul.py -h
### usage: test_model_simul.py [-h] --configdir CONFIG_DIR [--stepsaved STEP_SAVED] [--port PORT] [--displayplot] [--quiet] [--noisy] [--forcetest] [--simulator SIMULATOR]
### options:
###   -h, --help             show this help message and exit
###   --configdir CONFIG_DIR directory (relative path) where the config file <..._ppo_....yaml> lives
###   --stepsaved STEP_SAVED step of the saved NN to test
###   --displayplot          wether to display graphs or not
###   --verbose              level of verbosity
###   --noisy                wether to test swith deterministic False or True
###   --forcetest            wether to force test computation or not
###   --simulator SIMULATOR  Which simulator use in (gym, copsim, pybullet)

import numpy as np
import os, sys, time, yaml, pickle
from os.path import realpath
from pathlib import Path

#import matplotlib
#matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt

import gymnasium as gym
from gymnasium.wrappers import RecordVideo
from stable_baselines3.common.base_class import BaseAlgorithm
from ai.src.run.constants import TEST_DIR, EXPERIMENT_CONFIG_FILENAME, ENVIRONMENT_CONFIG_FILENAME
from ai.src.run.constants import PERFORMANCE_REPORT_FILENAME

from balancing_robot.pybullet.BalancingRobotEnv_Gym import BalancingRobotEnv_PyBullet
from ai.src.gym_envs.mini_apterros.mini_apterros import BasicMiniApterrosEnv
from apterros.pybullet.MiniAPTERROS_GymEnv_antiroll import MiniAPTERROS_AntiRoll

from utils import plot_action_reward, plot_data_miniapterros, plot_data_miniapterros_antiroll

def evaluate_model(environment: str,
                   model: BaseAlgorithm, 
                   env: gym.Env, 
                   out_dir:str=None,
                   deterministic: bool = True, 
                   steps_nb: int = 1000,
                   termbreak:bool = False,
                   verbose=0) -> dict:
    """
    Computes how a model performs on a given environment.

    Parameters:
      model:    Instance of a stable_baselines3 model (neural network).
      env:      Instance of a gym environment which is operated by the model.
      out_dir:  The directory path where to write the ouput of gym Monitor.
      deterministic: Wether a random noise is applied to the model during evaluation or not.
      steps_nb: # of simulation steps for evaluation.
      break_on_terminate: wether to break when terminated becomes True.
      verbose:  Wether to print some debug informations or not.
      
    returns: 
      A dict of {metrics_name: metrics_values}
    """
    
    # initialize some objects before the loop:
    env._max_episode_steps = steps_nb
    obs,  info = env.reset()
    terminated,  step_count, rewards, actions = False, 0, [], []
    try:
        env_name = env.unwrapped.spec.id
    except:
        env_name = environment
    plot_value = None
    
    if verbose >= 1: print(f"total_timesteps: {steps_nb}")
    
    L_t = [] # list of time values
    if env_name == "MiniApterros-v0":
        L_PWM, L_z, L_z_dot = [], [], []    # for plot after the testing
    elif env_name == "MiniApterros_AntiRoll":
        L_roll, L_dot_roll, L_action, L_pwm = [], [], [], [] # for plot after the testing'''
    
    # at each step the agent predicts the next action and updates the environment consequently
    while step_count < steps_nb:
        action, _states = model.predict(obs, deterministic=deterministic)
        obs, reward, terminated, truncated, info = env.step(action)
        rewards.append(reward)
        actions.append(action)
        step_count += 1
        
        if verbose and (step_count % 10 == 0) : print("\r", step_count, end="")
        
        if env_name == "MiniApterros-v0":
            t, action_n, z = info['t'], info['action_n'], info['z']
            z_dot = obs[1]
            PWM = action_n*100
            print(f"t: {t:4.2f}, a: {action_n:6.3f}, PWM: {PWM:04.1f}, z: {z:5.3f}", flush=True)
            L_PWM.append(PWM) 
            L_z.append(z)
            L_z_dot.append(z_dot)
            L_t.append(t)
            
        elif env_name == "MiniApterros_AntiRoll":
            t, roll, dot_roll, action, pwm = info['t'], info['roll'], info['dot_roll'], info['action'], info['pwm']
            L_t.append(t)
            L_roll.append(roll)
            L_dot_roll.append(dot_roll)
            L_action.append(action) 
            L_pwm.append(pwm)

        if env.headless == False: time.sleep(env.tau)
        if termbreak and terminated: break

    plot_value = {}
    if env_name == "MiniApterros-v0":
        plot_value = {"PWM" : list_PWM,
                      "altitude": list_z,
                      "altitude_dot": list_z_dot,
                      "Time": list_t}
        
    elif env_name == "MiniApterros_AntiRoll":   
        plot_value = {"time": L_t, 
                      "roll": L_roll, 
                      "dot_roll": L_dot_roll,
                      "pwm": L_pwm,
                      "action": L_action}
                
    if verbose >= 1: print(" ...done!")
    if step_count == steps_nb+1: step_count -= 1
    
    return {"actions": actions,
            "mean_abs_actions": float(np.mean(np.abs(actions))),
            "rewards" : rewards,
            "reward_cum": sum(rewards),
            "rewards_mean": float(np.mean(rewards)),
            "rewards_std": float(np.std(rewards)),
            "last_step_count": step_count,
            "percent_completion": step_count/steps_nb,
            "value_to_plot" : plot_value}


def test_model(cfg_loc:str     = None, 
               step:int        = 0,
               plots:bool      = False, 
               forcetest:bool  = False, 
               noisy:bool      = None,
               headless:bool   = None,
               termbreak:bool  = None,
               verbose:int     = 1):
    """
    Run an experiment described in cfg_file, and save the results and config for reproducibility

    Parameters:
      cfg_loc      : Path of the directory that contains the yaml config files.
      step         : step of the saved NN to test
      forcetest    : If True, runs the test even if the test directory already exists, else skips.
      plots        : Whether to display the plots or not.
      noisy        : whether to make a deterministic or noisy evaluation. 
                     If given None the 'deterministic' param from the yaml config file is used, 
                     else it is used to define 'deterministic = not noisy'.
      headless     : whether to display the simulator GUI or not. 
                     If given None the 'headless' param from the yaml config file is used, 
                     else its value overwrite the parameter's one.
      verbose      : level of verosity in (0,1,2)
    
    Returns:        None
    """
    
    # The config files must be searched in the directory of the 'cfg_loc'
    assert os.path.isdir(cfg_loc)
    real_dir  = Path(realpath(cfg_loc))
    cfg_file  = real_dir / EXPERIMENT_CONFIG_FILENAME

    with open(cfg_file, 'r') as f:
        cfg = yaml.safe_load(f.read())
 
    try:
        environment     = cfg['env']['environment']
        
        agent_type      = cfg['train']['agent_type']
        seed            = cfg['train']['seed']
        
        total_timesteps = cfg['eval']['total_timesteps']
        headless        = cfg['eval'].get('headless', False) if headless is None else headless
        deterministic   = cfg['eval']['deterministic'] if noisy is None else not noisy
        model_name      = cfg['eval']['model_name']
    except:
        raise RuntimeError(f"Parametres missing in file <{cfg_file}>")
    
    print(f'Found environment: <{environment}>')

    # Build the absolute pathname of the file 'model.zip':
    if step == 0:
        model_name = Path('ZIP') / model_name 
    else:
        model_name = Path('ZIP') / f'rl_model_{step}_steps.zip'
        
    full_model_name = real_dir / model_name
    
    if verbose >= 1: print(f"Looking for model <{model_name}>... ", end="")
    
    if full_model_name.exists():
        if verbose >= 1: print("found")
        
        """if verbose >= 1: print(" not found: ", end="")
        # the training stopped before the the last epoch: no link 'model.zip'
        zip_dir = real_dir / 'ZIP'
        zip_file_list = [(f, os.path.getmtime(zip_dir / f)) for f in os.listdir(zip_dir)]
        zip_file_list.sort(key=lambda elem: elem[1])
        if verbose >= 2: print(f"{zip_file_list=}")
        # we take the last zip file for the trained model:
        model_name = Path('ZIP') / zip_file_list[-1][0]"""
        
        full_model_name = real_dir / model_name
        if verbose >= 1: print(f"using <{model_name}>")
    else:
        raise ValueError(f'rl_model_{step}_steps.zip: file not found')
            
    # load the saved environment config file:
    if verbose >=1 : 
        print(f"env. parameters loaded from <{real_dir.name}/{ENVIRONMENT_CONFIG_FILENAME }>")
    
    cfg_file_env = real_dir / ENVIRONMENT_CONFIG_FILENAME         # cfg['env']['cfg_env']
    with open(cfg_file_env, 'r') as f:
        cfg_env = yaml.safe_load(f.read())

    # create the test directory if needed:
    test_dir = real_dir/TEST_DIR
    if step >0 : test_dir = test_dir / f"rl_model_{step}_steps"

    if not test_dir.exists() or forcetest:
        
        # import agent
        if  agent_type == 'PPO':
            from stable_baselines3 import PPO as agent
        else:
            raise Exception("Not implemented agent : <{agent_type}>")
        
        # Create env for evaluation
        # WARNING : the syntaxe gym.make('CartPole-v2') or gym.make('MiniApterros-v0') requires the 
        # modification of the directory /.../miniconda3/envs/minia2/lib/python3.x/site-packages/gymnasium/ 
        # according to ai/docs/update_gym_env.pdf.
        
        match environment:
            
            case 'BalancingRobotEnv_PyBullet':

                urdf_file = cfg['env']['urdf']
                veloc      = cfg_env['velocity']
                dt         = cfg_env['dt']
                version    = cfg_env['version']
                theta_lim  = cfg_env['theta_lim']
                x_lim      = cfg_env['x_lim']   
                diameter   = cfg_env['wheel_diam']   
                theta0_deg = cfg_env.get('theta0_deg', default=None) 
                reward     = cfg_env['reward']
                if verbose >=1 : print(f"found parameter 'theta0_deg':{theta0_deg}") 
                
                eval_env = BalancingRobotEnv_PyBullet(version, urdf_file, diameter, dt,
                                                      theta_lim,
                                                      x_lim,
                                                      theta0_deg=theta0_deg,                                        
                                                      reward=reward,                                                  
                                                      veloc_mag=veloc, 
                                                      headless=headless,
                                                      verbose=0)

            case 'BasicMiniApterrosEnv':

                param_list = ('total_mass', 'tau', 'z_target', 'stage', 'static_frict', 'max_step', 'reward_f')
                for param in param_list:
                    BasicMiniApterrosEnv.metadata[param] = cfg_env[param] 
                      
                BasicMiniApterrosEnv.metadata['seed']    = seed

                eval_env = gym.make('MiniApterros-v0')
                eval_env.verbose = verbose
            
            
            case 'ContinuousCartPoleEnv':
            
                eval_env = gym.make('CartPole-v2', render_mode="rgb_array", verbose=verbose)
                
                # wrapper to monitor and save in a movie what is happening
                eval_env = RecordVideo(eval_env, test_dir)
                
                param_list = ('cart_mass', 'pole_mass', 'pole_length', 'max_step', 'tau', 'max_step')
                for param in param_list:
                    eval_env.metadata[param]   = cfg_env[param]            

            case 'MiniApterros_AntiRoll':
                
                param_list = ('version', 'urdf', 'total_mass',
                              'roll_target_deg', 'roll_err_epsi_deg',
                              'roll_torque_Nm', 'torque_percent', 'z_target_m',
                              'max_step', 'tau', 'reward_f' )
                
                for param in param_list:
                    MiniAPTERROS_AntiRoll.metadata[param]   = cfg_env[param]
                    
                MiniAPTERROS_AntiRoll.metadata['seed']    = seed
                MiniAPTERROS_AntiRoll.metadata['forced_Zveloc'] = 0.25
                 
                eval_env = MiniAPTERROS_AntiRoll(headless=False, CW_turbine_id=(12,13), CCW_turbine_id=(15,16))
                eval_env.verbose = verbose
            
            case _:
                raise Exception("Not implemented environment: <{environment}>")
            
        
        model = agent.load(full_model_name)

        if not test_dir.exists(): test_dir.mkdir(parents=True)
    
        # Evaluate the trained agent    
        perf = evaluate_model(environment,
                              model, 
                              eval_env,
                              out_dir=test_dir,
                              steps_nb=total_timesteps, 
                              deterministic=deterministic,
                              termbreak=termbreak,
                              verbose=verbose)        
        perf["deterministic"] = deterministic
        
        with open(test_dir / PERFORMANCE_REPORT_FILENAME, 'wb') as outfile:
            pickle.dump(perf, outfile)

        eval_env.close()

    else:
        if verbose >= 1: print(f"TEST directory already exists: reading file <{real_dir.name}/TEST/{PERFORMANCE_REPORT_FILENAME}>")
        with open(test_dir / PERFORMANCE_REPORT_FILENAME, 'rb') as f:
            perf = pickle.load(f)

    for key, fmt in zip(('deterministic', 'mean_abs_actions', 'reward_cum', 'rewards_mean', 'last_step_count', 'percent_completion'),
                        ('', '.2f','.2f', '.2f', 'd', '.1f')):
        if verbose >= 1 : print(f"{key:20s}: {perf[key]:{fmt}}")
    
    # plot rewards = f(time_step)
    plot_action_reward(perf, real_dir, test_dir, plots)
    
    if environment == 'BasicMiniApterrosEnv':
        title = f"DRL training from {os.path.basename(cfg_loc)}/ZIP/{os.path.basename(realpath(full_model_name))}"
        title += f" [deterministic={deterministic}]"
        plot_data_miniapterros(title, perf, test_dir)
        
    elif environment == "MiniApterros_AntiRoll":   
        if cfg_loc[-1] == '/': cfg_loc = cfg_loc[:-1]
        title = f"Training data (from {os.path.basename(cfg_loc)})"
        target = MiniAPTERROS_AntiRoll.metadata['roll_target_deg']
        torque = MiniAPTERROS_AntiRoll.metadata['roll_torque_Nm']
        reward = MiniAPTERROS_AntiRoll.metadata['reward_f']
        title += f"\n[Reward: {reward}, roll_target={target:.1f} °, roll_torque={torque:.2f} Nm, deterministic={deterministic}]"
        plot_data_miniapterros_antiroll(title, perf, test_dir)
        

if __name__ == "__main__":
    # main bloc
    import argparse, sys

    parser = argparse.ArgumentParser()
    
    parser.add_argument('--configdir', action="store", dest='config_dir', required=True,
                         help="directory (relative path) where the config file <..._ppo_....yaml> lives")
    
    parser.add_argument('--step', action="store", dest='step', type=int, default=0,
                         help="step of the saved NN to test")

    parser.add_argument('--plots', action="store_true", dest='plots', default=False, 
                         help="to display the graphs")

    parser.add_argument('--noisy', action="store_true", dest='noisy', default=False,
                         help="adds noise on network actions")        
    
    parser.add_argument('--forcetest', action="store_true", dest='forcetest', default=False, 
                         help="wether to force test computation or not ")  
    
    parser.add_argument('--headless', action="store_true", dest='headless', default=None, 
                         help="to turn off the simulator GUI")                        

    parser.add_argument('--termbreak', action="store_true", dest='termbreak', default=None, 
                         help="break on terminate")                        

    parser.add_argument('--verbose', action="store", dest='verbose', type=int, default=1,
                         help="Level of verbosity in (0,1,2)")
    
    args = parser.parse_args()
    
    config_path = args.config_dir
    
    arg_dict = {}
    for key in 'step', 'plots', 'noisy', 'forcetest', 'headless', 'termbreak', 'verbose':
        arg_dict[key] =  eval(f'args.{key}')    
    
    print(f"Running: test_model({config_path}, {arg_dict}")
    
    test_model(config_path, **arg_dict)
    
    
