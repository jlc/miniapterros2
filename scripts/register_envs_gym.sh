#! /bin/bash
#
# JLC 2021-04-07 v1.0	shell script to modifify __init__.py files and create links to *.py file 
#                     in the gym/envs, gym/envs/classic_control and gym/envs/box2d folders.
#
# JLC 2023-10-17 v1.1	Version for gymanasium.
#

# For Mac platform install gsed (sed does'nt work properly) :
# brew install gnu-sed
#

# TODO gsed...

PYTHON_VERSION=python3.10

PROJECT_ROOT=$(dirname $(pwd))

target=$CONDA_PREFIX/lib/$PYTHON_VERSION/site-packages/gymnasium/envs
test -d $target
if [ $? = "0" ]; then
 cd $target
else
  target=~/opt/miniconda3/envs/minia2/lib/$PYTHON_VERSION/site-packages/gymnasium/envs
  test -d $target
  if [ $? = "0" ]; then
    cd $target
  fi
fi

echo "PROJECT_ROOT is: $PROJECT_ROOT"
echo "working in     : $(pwd)"

echo "==================="
echo "continuous CartPole"
echo "==================="
echo -n "1/ Updating gymnasium/envs/__init__.py..."

grep -q CartPole-v2 __init__.py

if [ $? != "0" ]; then
  n=$(sed __init__.py -n -e "/# Classic/=")
  n=$(($n+2))
  sed __init__.py -i -e "$n a\register(\n   id='CartPole-v2',\n   entry_point='gymnasium.envs.classic_control:ContinuousCartPoleEnv',\n   max_episode_steps=500,\n   reward_threshold=475.0,\n)\n"
  echo "Done"
else
  echo " no need."
fi

target=continuous_cartpole.py
folder=continuous_cartpole
echo "2/ Looking for link $target in gymnasium/envs/classic_control"
test -f classic_control/$target
if [ $? = "0" ]; then
  echo -n "    file exists"
  test -L classic_control/$target
  if [ $? = "1" ]; then
    echo " and is not a link : renaming with .orig suffixe"
    mv classic_control/$target classic_control/$target.orig
    echo "    linking $PROJECT_ROOT/ai/src/gym_envs/$folder/$target as classic_control/$target."
    ln -s -f $PROJECT_ROOT/ai/src/gym_envs/$folder/$target classic_control/$target
  else
    echo " and is already a link : nothing to to."
  fi
else
  echo "    linking $PROJECT_ROOT/ai/src/gym_envs/$folder/$target as classic_control/$target."
  ln -s -f $PROJECT_ROOT/ai/src/gym_envs/$folder/$target classic_control/$target
fi

module=continuous_cartpole
target=ContinuousCartPoleEnv
echo "3/ Looking for $target in gymnasium/envs/classic_control/__init__.py:"
grep -q $target classic_control/__init__.py
if [ $? != "0" ]; then
  echo "    Adding 'from ...$module import $target' in gymnasium/envs/classic_control/__init__.py"
  sed classic_control/__init__.py -i -e "$ a\from gymnasium.envs.classic_control.$folder import $target"
else
  echo "    Already done."
fi

echo "==================="
echo "   mini apterros"
echo "==================="
echo -n "1/ Updating gymnasium/envs/__init__.py..."

grep -q MiniApterros-v0 __init__.py

if [ $? != "0" ]; then
  n=$(sed __init__.py -n -e "/# Box2d/=")
  n=$(($n+2))
  sed __init__.py -i -e "$n a\register(\n   id='MiniApterros-v0',\n   entry_point='gymnasium.envs.box2d:BasicMiniApterrosEnv',\n   max_episode_steps=1000,\n   reward_threshold=500,\n)\n"
  echo "Done"
else
  echo " no need."
fi

target=mini_apterros.py
folder=mini_apterros
echo "2/ Looking for link $target in gymnasium/envs/box2d"
test -f box2d/$target
if [ $? = "0" ]; then
  echo -n "    file exists"
  test -L box2d/$target
  if [ $? = "1" ]; then
    echo " and is not a link : renaming with .orig suffixe"
    mv box2d/$target box2d/$target.orig
    echo "    linking $PROJECT_ROOT/ai/src/gym_envs/$folder/$target as box2d/$target."
    ln -s -f $PROJECT_ROOT/ai/src/gym_envs/$folder/$target box2d/$target
  else
    echo " and is a link : nothing to to."
  fi
else
  echo "    linking $PROJECT_ROOT/ai/src/gym_envs/$folder/$target as box2d/$target."
  ln -s -f $PROJECT_ROOT/ai/src/gym_envs/$folder/$target box2d/$target
fi

module=mini_apterros
target=BasicMiniApterrosEnv
echo "3/ Looking for $target in gymnasium/envs/box2d/__init__.py:"
grep -q $target box2d/__init__.py
if [ $? != "0" ]; then
  echo "    Adding 'from ...$module import $target' in gymnasium/envs/box2d/__init__.py"
  sed box2d/__init__.py -i -e "$ a\from gymnasium.envs.box2d.$module import $target"
else
  echo "    Already done."
fi
