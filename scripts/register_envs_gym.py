#
# JLC 2024-10-23 v1.0	Python pgrogram to modifify __init__.py files and create links to *.py file 
#                       in the gym/envs, gym/envs/classic_control and gym/envs/box2d folders.
#

import os
import gymnasium
from pathlib import Path
from shutil import copyfile

PROJECT_ROOT = os.path.dirname(os.getcwd())

conda_pref = os.environ['CONDA_PREFIX']
gym_location = os.path.dirname(gymnasium.__file__) 
gym_envs = os.path.join(gym_location, 'envs')

if os.path.isdir(gym_envs):
   os.chdir(gym_envs)
else:
   print("cannot guess the path of gymnasium module... Tchao! ")

print(f"PROJECT_ROOT:   <{PROJECT_ROOT}>")
print(f"Working in dir: <{gym_envs}>'")

print("=============================")
print(" Stage 1: continuous CartPole")
print("=============================")

print("a/ Updating gymnasium/envs/__init__.py: ", end='')

file = os.path.join(gym_envs, '__init__.py')
with open(file, "r", encoding="utf8") as F_in:
   content = F_in.read()
   F_in.seek(0)
   lines = F_in.readlines()

new_lines = []
if 'CartPole-v2' in content:
   print(" no need.")
else:
   for i, line in enumerate(lines):
      if "# Classic" in line:
         new_lines = lines[:i+3]
         break
   assert(new_lines)
   new_lines += ['register(\n', 
                 '    id="CartPole-v2",\n', 
                 '    entry_point="gymnasium.envs.classic_control:ContinuousCartPoleEnv",\n'
                 '    max_episode_steps=500,\n',
                 '    reward_threshold=475.0,\n)\n']
   new_lines += lines[i+3:]

   with open(file, "w", encoding="utf8") as F_out:
      F_out.write(''.join(new_lines))
      print(" done.")


target = 'continuous_cartpole.py'
print(f"b/ Looking for link <{target}> in ./classic_control: ", end='')

folder = 'continuous_cartpole'
target_path = os.path.join('classic_control', target)
src_path = os.path.join(PROJECT_ROOT, 'ai/src/gym_envs/', folder, target)

if os.path.exists(target_path) or os.path.lexists(target_path):
   print(' file exists', end='')
   if not os.path.islink(target_path):
      print(" and is not a link -> renaming with .orig suffixe")
      os.rename(target_path, target_path + '.orig')
      print(f" linking <{src_path}> as <{target_path}>.")
      os.symlink(src_path, target_path)
   else:
      print(" and is already a link -> nothing to to.")
else:
   print(f" linking <{src_path}> as <{target_path}>.")
   os.symlink(src_path, target_path)

module = 'continuous_cartpole'
target = 'ContinuousCartPoleEnv'
print(f"c/ Looking for <{target}> in gym/envs/classic_control/__init__.py:", end='')

file = os.path.join(gym_envs, 'classic_control', '__init__.py')
with open(file, "r", encoding="utf8") as F:
   content = F.read()
   
if target in content:
   print(" already done.")
else:
   content += f"\nfrom gymnasium.envs.classic_control.{module} import {target}\n"
   with open(file, "w", encoding="utf8") as F:
      F.write(content)
   print(" done.")


print("=============================")
print(" Stage 2: MiniAPTERROS"       )
print("=============================")

print("a/ Updating gymnasium/envs/__init__.py...", end='')

file = os.path.join(gym_envs, '__init__.py')
with open(file, "r", encoding="utf8") as F_in:
   content = F_in.read()
   F_in.seek(0)
   lines = F_in.readlines()

new_lines = []
if 'Apterros-v0' in content:
   print(" no need.")
else:
   for i, line in enumerate(lines):
      if "# Box2d" in line:
         new_lines = lines[:i+3]
         break
   assert(new_lines)
   new_lines += ['register(\n', 
                 '    id="MiniApterros-v0",\n', 
                 '    entry_point="gymnasium.envs.box2d:BasicMiniApterrosEnv",\n'
                 '    max_episode_steps=1000,\n',
                 '    reward_threshold=500,\n)\n']
   new_lines += lines[i+3:]

   with open(file, "w", encoding="utf8") as F_out:
      F_out.write(''.join(new_lines))
      print(" done.")


target = 'mini_apterros.py'
print(f"b/ Looking for link <{target}> in ./box2d: ", end='')

folder = 'mini_apterros'
target_path = os.path.join('box2d', target)
src_path = os.path.join(PROJECT_ROOT, 'ai/src/gym_envs/', folder, target)

if os.path.exists(target_path) or os.path.lexists(target_path):
   print(' file exists', end='')
   if not os.path.islink(target_path):
      print(" and is not a link -> renaming with .orig suffixe")
      os.rename(target_path, target_path + '.orig')
      print(f" linking <{src_path}> as <{target_path}>.")
      os.symlink(src_path, target_path)
   else:
      print(" and is already a link -> nothing to to.")
else:
   print(f" linking <{src_path}> as <{target_path}>.")
   os.symlink(src_path, target_path)


module = 'mini_apterros'
target = 'BasicMiniApterrosEnv'
print(f"c/ Looking for <{target}> in gym/envs/box2d/__init__.py:", end='')

file = os.path.join(gym_envs, 'box2d', '__init__.py')
with open(file, "r", encoding="utf8") as F:
   content = F.read()
   
if target in content:
   print(" already done.")
else:
   content += f"\nfrom gymnasium.envs.box2d.{module} import {target}"
   with open(file, "w", encoding="utf8") as F:
      F.write(content)
   print(" done.")
   