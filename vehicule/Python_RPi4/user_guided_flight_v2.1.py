#
# v1.0 2019/11    JLC initial version
#
# v1.1 2019/11/06 JLC set baudrate to 57600 for RPi.
#                     Improve Emergency STOP processing.
# v1.2 2021/04/28 Guillaume added creation of a file "user_guided_flight.txt"
#                     which register datas (Time [s], Z [m], PWM)
#
# v2.0 2023/06/13 JLC: new architecture with ROS
#
 
import sys, os
import numpy as np
from time import sleep, time
from serial import Serial
from datetime import datetime
from Utils import get_pwmLimit, get_pwmStep, get_KeyPressed
import rospy

#
# This program must be run from terminal to work !!!
#

exit_required = False

################################
# Connexion to serial port :
################################
# Name of the serial port  :
#   Windows  : "COM3" or "COM4"....
#   PC-Linux : "/dev/ttyACM0" 
#   RPi      : same as PC-Linux
#   Mac OS X : see the title  bar of the wmonitor window.
#
# Parameters : Nb data bits  -> 8
#              Nb STOP bits  -> 1
#              Parity        -> Sans (None)
#              baud rate     -> 9600, 14400, 19200, 28800, 38400,
#                               57600, 115200, 250000

# JLC 2023/06/13: IIDRE is on /dev/ttyACM0

listUSBports = ["/dev/ttyACM1"]

for port in listUSBports:
    try:
        print(f"Trying {port}")
        serialPort = Serial(port, baudrate=115200, timeout=None)
        break
    except:
        continue
print(serialPort)    

# Open serial serial if needed:
sleep(0.5)
if not serialPort.is_open :
    serialPort.open()

# wait for Arduino ready:
print("Waiting for ARDUINO ... ")
data = b""
while "Arduino OK" not in data.decode().strip() :
    data = serialPort.readline()
    print(data.decode().strip())
    if "ERROR" in data.decode().strip():
        sys.exit()
print("Found <Arduino OK> : good !",flush=True)

pulseWidthMIN = 1000;   # micro-sec ; stops the turbine
pulseWidthMAX = 2000;   # micro-sec ; turbine rotates at max speed
pulseStepMIN  = 10;     # micro-sec ; smallest pulse width step allowed
pulseStepMAX  = 100;    # micro-sec ; biggest pulse width step allowed
pwmMIN        = 0.
pwmMAX        = 100.
pwmStepMIN    = 0.1
pwmStepMAX    = 10.

sleep(1)

ok = False
while not ok:
    pwmLimit = get_pwmLimit(pwmMIN, pwmMAX)
    pwmStep  = get_pwmStep(pwmStepMIN, pwmStepMAX)
    rep = input("pwmLim: {} %, pwmStep: {} %, confirm [y/n] ? "\
          .format(pwmLimit, pwmStep))
    if rep == 'y': ok = True    

now = datetime.now() # current date and time
fileName = 'user_guided_flight-'+now.strftime("%Y_%m_%d_%H_%M")+'.txt'
fileOut  = open(fileName, "w", encoding="utf8")
    
header        = "#time[s]\t PWM[%]\t Z[mm]\t yaw[rad]\t pitch[rad]\t roll[rad]\n"
data_to_write = ""
fileOut.write(header)
fileOut.flush()

keys = []
# data exchange loop...
delayLoop = 0.5 # secondes

pwm = pwmMIN
emergencyStopRequested = False
t0 = time()

while True:    
    K = get_KeyPressed()
   
    print(f"K: <{K}>")

    ch = hex(ord(K))
    ################################
    # useful ASCII code values:
    ################################
    # \n    -> 0xd    --> ENTER
    # ESC   -> 0x1b
    # SPACE -> 0x20
    # +     -> 0x2b
    # -     -> 0x2d
    # Q     -> 0x51
    # q     -> 0x71

    # if the user hits 'q', 'Q', SPACE or 'ENTER' -> quit the program:
    if ch in ('0x51', '0x71', '0x20', '0xd'):
        emergencyStopRequested = True
        req = "PWM_TU:{:02d}%PWM_LA:{:02d}%PWM_RA:{:02d}%"
        PWM_TU = 0
        PWM_LA, PWM_RA = 0, 0
        req = str(req.format(PWM_TU,PWM_LA, PWM_RA))
        print('RPi write : <{}>'.format(req))
        data_w = bytes(req, encoding="ascii")
        serialPort.write(data_w)
        serialPort.flush()
        break
    elif ch == '0x2b':
        pwm += pwmStep
    elif ch == '0x2d':
        pwm -= pwmStep
    
    if pwm > pwmLimit :
        pwm = pwmLimit
    elif pwm < pwmMIN :
        pwm = pwmMIN
    print("pwm:",pwm)
    
    PWM_TU = round(pwm)
    PWM_LA, PWM_RA = 0, 0
    # send PWM_T... to Arduino
    req = "PWM_TU:{:02d}%PWM_LA:{:02d}%PWM_RA:{:02d}%"
    req = str(req.format(PWM_TU,PWM_LA, PWM_RA))
    print('RPi write : <{}>'.format(req))
    data_w = bytes(req, encoding="ascii")
    serialPort.write(data_w)
    serialPort.flush()

    # 2 steps read : 1/ acknowledge, 2/ then measured data
    # 1/ read Arduino acknowledge:
    data_a = serialPort.readline()          # read data acknowledge
    data_a = data_a.decode().strip()        # clean data
    print("RPi read-1: <{}>".format(data_a))# for debug only
    if "EMERGENCY-STOP" in data_a:
        print("EMERGENCY-STOP resquested from ARDUINO, Tcho !")
        emergencyStopRequested = True
        req = "PWM_TU:{:02d}%PWM_LA:{:02d}%PWM_RA:{:02d}%"
        PWM_TU = 0
        PWM_LA, PWM_RA = 0, 0
        req = str(req.format(PWM_TU,PWM_LA, PWM_RA))
        print('RPi write : <{}>'.format(req))
        data_w = bytes(req, encoding="ascii")
        serialPort.write(data_w)
        serialPort.flush()
        break
    pass                                    # process data acknowledge

    # read Arduino measurement
    data_m = serialPort.readline()          # read data measurement
    data_m = data_m.decode().strip()        # clean data
    
    if data_m.startswith("ERROR") \
       or data_m.startswith("DEBUG") \
       or data_m.startswith("INFO"):
        pass
    elif "EMERGENCY-STOP" in data_m:
        print("EMERGENCY-STOP resquested from ARDUINO, Tcho !")
        emergencyStopRequested = True
        req = "PWM_TU:{:02d}%PWM_LA:{:02d}%PWM_RA:{:02d}%"
        PWM_TU = 0
        PWM_LA, PWM_RA = 0, 0
        req = str(req.format(PWM_TU,PWM_LA, PWM_RA))
        print('RPi write : <{}>'.format(req))
        data_w = bytes(req, encoding="ascii")
        serialPort.write(data_w)
        serialPort.flush()
        break
    else:        
        # LIdar data:
        lidar = rospy.get_param('MiniAPTERROS/LIDAR')
        # angle names for vehicule frame:
        yaw, pitch, roll = rospy.get_param('/MiniAPTERROS/MTI30')[2]
        t1 = time()-t0
        print(f"RPi read-2: <{lidar[1]:.1f} mm>")
        # data_format   = "{}\t{}\t{}\n"
        data_to_write = f"{t1:8.3f}\t{PWM_TU:02d}\t{lidar[1]:4.3f}\t"
        data_to_write += f"{yaw:0.3f}\t{pitch:0.3f}\t{roll:0.3f}\n"
        fileOut.write(data_to_write)  
        fileOut.flush()
    
t1 = time()-t0  
fileOut.write(data_to_write)  
fileOut.write("# Flight duration: {:.1f} s\n".format(round(t1,1)))
print("\n     >>> End of transmission")

# clean serial port before closing
serialPort.flushOutput()
sleep(0.5)

#close serial
serialPort.close()          

# close data file:
fileOut.close()
