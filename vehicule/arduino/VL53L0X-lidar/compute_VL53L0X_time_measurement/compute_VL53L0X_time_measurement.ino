/* This example shows how to get single-shot range
 measurements from the VL53L0X. The sensor can optionally be
 configured with different ranging profiles, as described in
 the VL53L0X API user manual, to get better performance for
 a certain application. This code is based on the four
 "SingleRanging" examples in the VL53L0X API.

 The range readings are in units of mm. */

#include <Wire.h>
#include <VL53L0X.h>

VL53L0X sensor;


// Uncomment this line to use long range mode. This
// increases the sensitivity of the sensor and extends its
// potential range, but increases the likelihood of getting
// an inaccurate reading because of reflections from objects
// other than the intended target. It works best in dark
// conditions.

//#define LONG_RANGE


// Uncomment ONE of these two lines to get
// - higher speed at the cost of lower accuracy OR
// - higher accuracy at the cost of lower speed

#define HIGH_SPEED
//#define HIGH_ACCURACY

void setup()
{
  Serial.begin(115200);
  Wire.begin();

  sensor.setTimeout(100);
  if (!sensor.init())
  {
    Serial.println("Failed to detect and initialize sensor!");
    while (1) {}
  }

#if defined LONG_RANGE
  // lower the return signal rate limit (default is 0.25 MCPS)
  sensor.setSignalRateLimit(0.1);
  // increase laser pulse periods (defaults are 14 and 10 PCLKs)
  sensor.setVcselPulsePeriod(VL53L0X::VcselPeriodPreRange, 18);
  sensor.setVcselPulsePeriod(VL53L0X::VcselPeriodFinalRange, 14);
#endif

#if defined HIGH_SPEED
  // reduce timing budget to 20 ms (default is about 33 ms)
  sensor.setMeasurementTimingBudget(20000);
#elif defined HIGH_ACCURACY
  // increase timing budget to 200 ms
  sensor.setMeasurementTimingBudget(200000);
#endif
}

bool done = false;

void loop()
{
  const int N = 200;
  double elapsed_time = 0;
  float z[N];
  float z_mean, z_std;
  unsigned int t0, t1, dt;
  
  if ( !done ) 
  {
    Serial.println("start...");
    done = true;
    for (int i=0; i<N; i++)
    {
       t0 = millis();
       z[i] = sensor.readRangeSingleMillimeters();
       t1 = millis();
       if (sensor.timeoutOccurred()) 
       {
          Serial.print(" TIMEOUT"); 
       }
       if ( i % 100 == 0) Serial.println(i);
       dt = t1 - t0;
       elapsed_time += dt;
    }
    Serial.print("Mean measurement time: ");
    Serial.print(elapsed_time/N, 2);
    Serial.print(" [ms]\n");

    z_mean = 0;
    for (int i=0; i<N; i++)
    {
      z_mean += z[i];
    }
    z_mean /= N;

    z_std = 0;
    for (int i=0; i<N; i++)
    {
      z_std += pow(z[i] - z_mean, 2)/float(N);
    }
    z_std = sqrt(z_std);

    
    Serial.print("z_mean: "); Serial.print(z_mean, 1);
    Serial.print(" mm, z_std: "); Serial.print(z_std, 1);
    Serial.println(" mm");

    for (int i=0; i<N; i++)
    {
      Serial.println(z[i],2);
    }
  }
}
