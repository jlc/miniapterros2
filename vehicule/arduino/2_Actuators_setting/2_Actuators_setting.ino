/////////////////////////////////////////////////////////////////////////////////////////
//      MiniApterros project -- File "2_Actuators_setting"
/////////////////////////////////////////////////////////////////////////////////////////
//
// This program drives the 2 electric actuators  using 2 potentiometers.
//
// Nota -- The 2 pulse-width limits for the actuatator ESC (Electronic Speed Control) are:
// - 1000 micro-sec: the actuator is fully retracted
// - 2000 micro-sec: the actuator is fully expanded.
//////////////////////////////////////////////////////////////////////////////////////////
//
// Essai_Turbines_v1.0.ino -- 2024/10/21 -- JLC
//

#define PWM_max 2000
#define PWM_min 1000

#define pot1 A0
#define pot2 A1
#define pinAct1 9
#define pinAct2 6

#define v1_min 0.05
#define v1_max 4.87
#define v2_min 0.05
#define v2_max 4.80

#include <Servo.h>      

#include <Wire.h>
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x20,16,2);  // objet lcd: 2 lignes x 16 caractères à l'adresse I2C 0x20

Servo actuator1;
Servo actuator2;

void setup() 
{
  pinMode(pinAct1, OUTPUT); 
  pinMode(pinAct2, OUTPUT);

  Serial.begin(9600);

  actuator1.attach(pinAct1, PWM_min, PWM_max);  
  actuator2.attach(pinAct2, PWM_min, PWM_max);  

  lcd.init();                      // initialize the lcd 
  lcd.backlight();
  
  lcd.setCursor(0, 0);
  lcd.print("Hello ");
  lcd.setCursor(0, 1);
  lcd.print("");
}

void loop() 
{
  float v1 = analogRead(pot1)*5/1024.;
  float v2 = analogRead(pot2)*5/1024.;

  float PWM1 = PWM_max - (v1 - v1_min)*(PWM_max - PWM_min)/(v1_max - v1_min);
  float PWM2 = PWM_max - (v2 - v2_min)*(PWM_max - PWM_min)/(v2_max - v2_min);
  
  lcd.setCursor(0, 0);
  lcd.print("pwm1: " + String(PWM1, 0));
  lcd.setCursor(0, 1);
  lcd.print("pwm2: " + String(PWM2, 0));

  actuator1.writeMicroseconds(PWM1);
  actuator2.writeMicroseconds(PWM2);

  delay(200);
}
