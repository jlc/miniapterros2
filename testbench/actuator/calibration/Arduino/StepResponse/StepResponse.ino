////////////////////////////////////////////////
//      MiniApterros project 
////////////////////////////////////////////////
// 
// Nota -- The 2 pulse-width limits for the actuatator ESC are:
// - 1000 micro-sec: the actuator is fully retracted
// - 2000 micro-sec: the actuator is fully expanded.
//
// v1.0 -- 2024/02/21 -- JLC
// v1.1 -- 2024/03/12 -- JLC -- Ajout beep

#include <Servo.h>      
Servo esc;              // create the esc PWM object to drive the actuator ESC

const int pulseWidthMIN = 1000+10;   // mu-sec, actuator fully retracted
const int pulseWidthMAX = 2000-10;   // mu-sec, actuator fully expanded.
const int midPulse = (pulseWidthMIN + pulseWidthMAX)/2; // 1500 mu-sec

#define pinActuator 9  // PWM output to drive the actuator
#define pinBeep     3  // Beeper with electronic 

int pulseStep;                      

void clearSerialBuffer()
{
  // read characters from the serial line until there is nothing to read:
  while(Serial.available() > 0) 
  {
    char t = Serial.read();
  }
}

void beep(int duration=500)
{
  digitalWrite(pinBeep, LOW);
  digitalWrite(pinBeep, HIGH);
  delay(duration);
  digitalWrite(pinBeep, LOW);
}

void beepBeep(int duration=80)
{
  beep(100);
  delay(duration);
  beep(100);  
}

void setup() 
{  
  pinMode(pinBeep, OUTPUT);
  digitalWrite(pinBeep, LOW);

  // link the linear actuator to pinActuator, with min and max pulse-width in microsecondes:
  esc.attach(pinActuator, pulseWidthMIN, pulseWidthMAX);  
  
  esc.writeMicroseconds(midPulse);     // initialize PWM to mid-course
  
  Serial.begin(9600);                           // open the USB connexion with computer

  Serial.print(">>> Enter the pulse-width step value in [0;500] micro-sec ?  "); 
  // read a pulse-step from keyboard until a valid value is given in the range [100;500]:
  pulseStep = -1;
  while(pulseStep < 0 || pulseStep > 500)
  {
    if (Serial.available() > 0) 
    {     
      pulseStep = Serial.parseInt(); 
    }
  }
  clearSerialBuffer();         
  Serial.println(String(pulseStep) + " mu-sec"); 
}

void loop() 
{

  esc.writeMicroseconds(midPulse);     // actuator at mid-course

  Serial.println(">>> Press ENTER when ready...");   
  while (Serial.available() <= 0) {;}  // wait until user press a key...    
  clearSerialBuffer();                 // clear the buffer

  Serial.println("\t >>> PWM at       " + String(midPulse) + " mu_sec");

  delay(1000);  
  beepBeep();
  delay(2000);

  beep(100);
  delay(1000);
  
  Serial.println("\t >>> PWM going to " + String(midPulse + pulseStep) + " mu_sec");
  esc.writeMicroseconds(midPulse + pulseStep);
  delay(3000);                         // wait a bit...
  
  Serial.println("\t >>> PWM going to " + String(midPulse) + " mu_sec");
  esc.writeMicroseconds(midPulse);     // actuator at mid-course
  delay(3000);
  
  Serial.println("\t >>> PWM going to " + String(midPulse - pulseStep) + " mu_sec");
  esc.writeMicroseconds(midPulse - pulseStep);
  delay(3000);                         // wait a bit...

  Serial.println("\t >>> PWM going to " + String(midPulse) + " mu_sec");
  esc.writeMicroseconds(midPulse);     // actuator at mid-course
  delay(3000);
  beepBeep();
}
