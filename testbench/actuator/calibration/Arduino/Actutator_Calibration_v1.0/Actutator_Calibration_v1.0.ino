/////////////////////////////////////////////////////////////////////////////////////////
//      MiniApterros project -- File "Actuator_Calibration"
/////////////////////////////////////////////////////////////////////////////////////////
// 
// * The program asks the user to choose an upper-limit for the pulse-width ramp. 
// * Then it launches an increasing pulse-width ramp until the pulse-width reaches the chosen value. 
// * The pulse-width ramp makes a step each time the user pushes the ENTER  key on the keyboard. 
//
// Nota -- The 2 pulse-width limits for the actuatator ESC (Electronic Speed Control) are:
// - 1000 micro-sec: the actuator is fully retracted
// - 2000 micro-sec: the actuator is fully expanded.
//////////////////////////////////////////////////////////////////////////////////////////
//
// Essai_Turbines_v1.0.ino -- 2024/10/21 -- JLC
//

#include <Servo.h>      
Servo esc;              // create the esc PWM object to drive the actuator ESC

const int pulseWidthMIN = 1000+10;   // micro-sec ; the actuator is fully retracted
const int pulseWidthMAX = 2000-10;   // micro-sec ; the actuator is fully expanded.

#define pinActuator 11 // PWM output to drive the actuator

void clearSerialBuffer()
{
  // read characters from the serial line until there is nothing to read:
  while(Serial.available() > 0) 
  {
    char t = Serial.read();
  }
}

void setup() 
{  
  // link the linear actuator to pinActuator, with min and max pulse-width in microsecondes:
  esc.attach(pinActuator, pulseWidthMIN, pulseWidthMAX);  
  
  esc.writeMicroseconds(pulseWidthMIN);         // initialize PWM command to the 'STOP' value
  
  Serial.begin(9600);                           // open the USB connexion with computer
}

void loop() 
{
  Serial.println("\n*******************************************************************");
  Serial.println("Actuator MIN and MAX PWM values are 10000 and 2000 micro-sec.");
  Serial.println(">>> Enter the max pulse width to reach in range [1000;2000] micro-sec ?"); 
  
  // read a pulse-width limit from keyboard until a valid value is given 
  // in the range [pulseWidthMIN, pulseWidthMAX]:
  int pulseWidthLimit = 0;                      
  while(pulseWidthLimit<= pulseWidthMIN || pulseWidthLimit > pulseWidthMAX)
  {
    pulseWidthLimit = Serial.parseInt(); 
  }

  Serial.print("    Limit pulse-width value for the ramp will be: ");
  Serial.print(pulseWidthLimit);
  Serial.println(" micro-sec");

  Serial.println(">>> Enter the step of the ramp between 10 and 100 micro-sec ?"); 
  
  // read a pulse-width limit from keyboard until a valid value is given 
  // in the range [10,100]:
  int pulseWidthStep = 0;                      
  while(pulseWidthStep < 10 || pulseWidthStep > 100)
  {
    pulseWidthStep = Serial.parseInt(); 
  }

  Serial.print("    pulse-width step for the ramp will be: ");
  Serial.print(pulseWidthStep);
  Serial.println(" micro-sec");  
  
  int pulseWidth = pulseWidthMIN; 
  while (pulseWidth <= pulseWidthLimit) 
  {
    Serial.print("    pulse width: ");
    Serial.print(pulseWidth);
    Serial.println(" micro-sec, press ENTER to step...");   
    esc.writeMicroseconds(pulseWidth);  

    while (Serial.available() <= 0) {;}  // wait until user press a key...    
    clearSerialBuffer();                 // clear the buffer
    
    pulseWidth += pulseWidthStep; 
  }
  
  delay(200);                             // wait a bit...
  Serial.println("now positioning the actuator at PWM value of 1010 micro-sec");
  esc.writeMicroseconds(pulseWidthMIN);   // actuator at mid-course
}
