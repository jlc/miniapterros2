/////////////////////////////////////////////////////////////////////////////////////////
//      MiniApterros project -- File "Turbine_Calibration"
/////////////////////////////////////////////////////////////////////////////////////////
// 
// >>> To run a calibration test of the ESC (Electronic Speed Control) of the electrical turbine
// >>> used for the MiniApterros vehicule. 
// >>> This program makes use of the calibration benchmark proposed by the turbine vendor that 
// >>> allows one to measure the thrust of the turbine in kg.
//
// The program asks the user to choose an upper-limit for the pulse-width ramp. 
// Then it launches an increasing pulse-width ramp until the pulse-width reaches the chosen value. 
// The pulse-width ramp makes a step each time the user pushes the ENTER  key on the keyboard. 
// At each step of the ramp one can write the data (current pulse-width, measured_turbine_thrust)
// in order to draw the line turbine_thrust = f(pulse-width). 
// It is then possible to approximate the data with a first order polynomial expression 
// which will be used to later to drive the turbines thrust.
//
// Nota -- The 2 pulse-width limits for the ESC (Electronic Speed Control) used are:
// - 1000 micro-sec for stopping the turbine
// - 2000 micro-sec for the maximum rotation speed of the turbine.
//////////////////////////////////////////////////////////////////////////////////////////
//
// Essai_Turbines_v1.0.ino -- 2019/10/21 -- T. HARRIET & M. LAROQUE
//      Initial revision
//
// Essai_Turbines_v1.1.ino -- 2019/10/25 -- JLC
//      Change variable names to explicit names... plus some cosmetic changes for better robustness
//
// Essai_Turbines_v1.2.ino -- 2019/10/29 -- ML + JLC
//      Add emergency stop.
//
// Essai_Turbines_v1.3.ino -- 2021/09/14 -- JLC
//      Modify emergency stop: normaly HIGH, fall down to LOW if button is pushed or cable destroyed ;-)
//
// Essai_Turbines_v1.4.ino -- 2023/05/23 -- JLC
//      Add battery voltage measurements
//

#include <Servo.h>      // include definition of the Servo class.

Servo esc;              // create the esc PWM object to drive the ESC  of the turbine

const int pulseWidthMIN = 1000;   // micro-sec ; stops the turbine
const int pulseWidthMAX = 2000;   // micro-sec ; turbine rotates at max speed

///////////////////////////////////////////////////
///////      Pins Layout          
///////////////////////////////////////////////////

///// Emergency STOP button //////
#define pinEmergencyStop 2

///// PWM output to drive the turbines //////
#define pinPWM_Turbine  9

volatile bool EmergencyStopRequired = false;  

void clearSerialBuffer()
{
  // read characters from the serial line until there is nothing to read:
  while(Serial.available() > 0) 
  {
    char t = Serial.read();
  }
}

void PrintBatteryVoltage()
{
  float V0 = analogRead(A0)/1024.*5*5;
  float V1 = analogRead(A1)/1024.*5*5;
  Serial.println("Battery_AO: " + String(V0,1) + " V - Battery_A1: " + String(V1,1) + " V");
}
void EmergencyStop() 
{
  int pulseWidth = pulseWidthMIN;
  esc.writeMicroseconds(pulseWidth);

  Serial.println("Emergency stop required");  
  EmergencyStopRequired = true;
}

void setup() 
{  
  // link the turbine ESC to pin9 (PWM capable) with min and max pulse width in microsecondes
  esc.attach(pinPWM_Turbine, pulseWidthMIN, pulseWidthMAX);  
  
  esc.writeMicroseconds(pulseWidthMIN);         // initialize PWM command to the 'STOP' value
  Serial.begin(9600);                           // open the USB connexion with computer

  // Since september 14, the pinEmergencyStop is pulled down to zero volt using an external resistor of 10 kOhm.
  // The Emergency stop button (normally close) applies the 5 V to the pinEmergencyStop: when the emergency button
  // is pressed, or when the cable is cut, the input level on pinEmergencyStop falls from 5 V to 0 V.
  // That's why the attachInterrupt() trigers the EmergencyStop function if the level of pinEmergencyStop goes "LOW"!!!
 
  pinMode(pinEmergencyStop, INPUT);
  while (digitalRead(pinEmergencyStop) == LOW)
  {
    Serial.println("EMERGENCY STOP ANOMALY : release Emergency Button, or check cables....");
    delay(1000);
  }

  // To attach the right interruption to the function  EmergencyStop:
  attachInterrupt(digitalPinToInterrupt(pinEmergencyStop), EmergencyStop, LOW);  

  PrintBatteryVoltage();
}

void loop() 
{
  if (EmergencyStopRequired) return;  // skip loop function if EmergencyStop eis required
  
  Serial.println("\n*******************************************************************");
  Serial.println("Turbine STOP and MAX speed PWM values are 10000 and 2000 micro-sec.");
  Serial.println(">>> Enter the pulse-width to reach in range [1000 ; 2000] micro-sec ?"); 
  
  // read a pulse-width limit from keyboard until a valid value is given 
  // in the range [pulseWidthMIN, pulseWidthMAX]:
  int pulseWidthLimit = 0;                      
  while(pulseWidthLimit<= pulseWidthMIN || pulseWidthLimit > pulseWidthMAX)
  {
    pulseWidthLimit = Serial.parseInt(); 
  }

  Serial.print("    Limit pulse-width value for the ramp will be: ");
  Serial.print(pulseWidthLimit);
  Serial.println(" micro-sec");

  Serial.println(">>> Enter the step of the ramp between 10 and 100 micro-sec ?"); 
  
  // read a pulse-width limit from keyboard until a valid value is given 
  // in the range [10,100]:
  int pulseWidthStep = 0;                      
  while(pulseWidthStep < 10 || pulseWidthStep > 100)
  {
    pulseWidthStep = Serial.parseInt(); 
  }

  Serial.print("    pulse-width step for the ramp will be: ");
  Serial.print(pulseWidthStep);
  Serial.println(" micro-sec");
  
  
  int pulseWidth = pulseWidthMIN; 
  while (pulseWidth <= pulseWidthLimit) 
  {
    if (EmergencyStopRequired) break;
    Serial.print("    pulse width: ");
    Serial.print(pulseWidth);
    Serial.println(" micro-sec, press ENTER to step...");   
    esc.writeMicroseconds(pulseWidth);  

    while (Serial.available() <= 0) {;}  // wait until user press a key...    
    clearSerialBuffer();                 // clear the buffer

    PrintBatteryVoltage();
    
    pulseWidth += pulseWidthStep; 
  }

  int i = 0;
  while (i++ <= 10)
  {
    PrintBatteryVoltage();
    delay(1000);
  }
  
  delay(200);                             // wait a bit...
  Serial.println("now stopping turbine with PWM value of 1000 micro-sec");
  esc.writeMicroseconds(pulseWidthMIN);   // Stops the turbine 
}
